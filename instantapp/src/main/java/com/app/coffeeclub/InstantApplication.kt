package com.app.coffeeclub

import android.app.Application
import android.util.Log
import androidx.annotation.NonNull
import com.app.base.BuildConfig
import com.app.coffeeclub.network.RetrofitHelper
import com.app.coffeeclub.utilities.helpers.SharedPreferencesHelper
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.jakewharton.threetenabp.AndroidThreeTen
import timber.log.Timber

class InstantApplication : Application() {

    init {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
    }

    /** A tree which logs important information for crash reporting.  */
    private class CrashReportingTree : Timber.Tree() {
        override fun log(priority: Int, tag: String?, @NonNull message: String, t: Throwable?) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return
            }

            val crashlytics = FirebaseCrashlytics.getInstance()
            crashlytics.log(message)

            if (t != null) {
                crashlytics.log(t.message.orEmpty())
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        SharedPreferencesHelper.init(applicationContext)
        RetrofitHelper.init(this)
        AndroidThreeTen.init(this)
    }
}