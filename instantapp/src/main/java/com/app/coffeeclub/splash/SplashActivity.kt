package com.app.coffeeclub.splash

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.coffeeclub.actions.Actions
import com.app.coffeeclub.utilities.helpers.SharedPreferencesHelper
import com.google.android.gms.common.wrappers.InstantApps

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startActivity(Actions.getHomeIntent(this))
    }
}
