package com.app.coffeeclub.utilities.helpers

import android.content.Context
import com.app.coffeeclub.utilities.R

object ValidationHelper {

    fun isEmailValid(email: String?): Boolean {
        email ?: return false
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun isPasswordValid(context: Context, password: String?): Boolean {
        password ?: return false
        val minPasswordLength = context.resources.getInteger(R.integer.password_min_length)
        return password.length >= minPasswordLength
    }

    fun isPostalCodeValid(context: Context, postalCode: String): Boolean {
        val validLength = context.resources.getInteger(R.integer.postal_code_length)
        return postalCode.length == validLength
    }

    fun isReferralCodeValid(context: Context, referralCode: String): Boolean {
        val validMaxLength = context.resources.getInteger(R.integer.referral_code_max_length)
        val validMinLength = context.resources.getInteger(R.integer.referral_code_min_length)
        return referralCode.length in validMinLength..validMaxLength || referralCode.isBlank()
    }
}