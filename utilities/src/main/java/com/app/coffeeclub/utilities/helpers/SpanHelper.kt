package com.app.coffeeclub.utilities.helpers

import android.text.Spannable
import android.text.SpannableString
import com.app.coffeeclub.utilities.classes.CustomTabSpan

object SpanHelper {

    fun addCustomTabToSpannable(spannableString: SpannableString, totalString: String, keyword : String, url: String) : SpannableString {
        val startIndex = totalString.indexOf(keyword)
        val endIndex = startIndex + keyword.length
        spannableString.setSpan(CustomTabSpan(url), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return spannableString
    }
}