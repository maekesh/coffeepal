package com.app.coffeeclub.utilities.helpers

import com.app.coffeeclub.utilities.OffsetDateTimeConverter
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.threeten.bp.OffsetDateTime

object GsonHelper {
    fun newInstance() : Gson{
        return GsonBuilder()
            .registerTypeAdapter(OffsetDateTime::class.java, OffsetDateTimeConverter())
            .create()
    }
}