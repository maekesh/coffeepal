package com.app.coffeeclub.utilities

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import org.threeten.bp.*
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.DateTimeParseException
import java.lang.reflect.Type

class OffsetDateTimeConverter : JsonDeserializer<OffsetDateTime> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): OffsetDateTime {
        if (typeOfT == OffsetDateTime::class.java) {

            try {
                return OffsetDateTime.parse(json?.asJsonPrimitive?.asString)
            } catch (ex: DateTimeParseException) {
                return OffsetDateTime.of(LocalDate.parse(json?.asJsonPrimitive?.asString, DateTimeFormatter.ofPattern("yyyy-MM-dd")), LocalTime.MIN, ZoneOffset.UTC)
            }
        }

        throw IllegalArgumentException("OffsetDateTimeConverter: unknown getSubscriptionType: $typeOfT")
    }
}