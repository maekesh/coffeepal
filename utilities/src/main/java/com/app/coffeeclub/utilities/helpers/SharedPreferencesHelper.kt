package com.app.coffeeclub.utilities.helpers

import android.content.Context
import android.content.SharedPreferences
import android.location.Location
import androidx.core.content.edit

object SharedPreferencesHelper {
    private val PREF_KEY = "PREF_KEY"

    private const val keyRefreshToken = "keyRefreshToken"
    private const val keyAccessToken = "keyAccessToken"
    private const val keyHasEnrolledNotifications = "keyHasEnrolledNotifications"
    private const val keyHasSkippedRegistration = "keykeyHasSeenWelcome"
    private const val keyHasSkippedLocationSettings = "keyHasSkippedLocationSettings"
    private const val keyShowCardAd = "keyshowCardAd"
    private const val keyUserLastLocationLatitude = "keyUserLastLocationLatitude"
    private const val keyUserLastLocationLongitude = "keyUserLastLocationLongitude"
    private const val keyHasSentPushToken = "keyHasSentPushToken"
    private const val LANGUAGE_DETAILS = "languageDetails"

    private lateinit var sharedPreferences: SharedPreferences

    fun init(context : Context){
        sharedPreferences =  context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
    }

    fun saveRefreshToken(accessToken : String){
        sharedPreferences.edit { putString(keyRefreshToken, accessToken)}
    }

    fun loadRefreshToken() : String?{
        return sharedPreferences.getString(keyRefreshToken, "")
    }

    fun saveAccessToken(accessToken : String){
        sharedPreferences.edit { putString(keyAccessToken, accessToken)}
        saveHasSkippedRegistration(false)
    }

    fun loadAccessToken() : String?{
        return sharedPreferences.getString(keyAccessToken, null)
    }

    fun loadHasEnrolledNotifications(): Boolean {
        return sharedPreferences.getBoolean(keyHasEnrolledNotifications, false)
    }

    fun saveHasEnrolledNotifications(hasEnrolled : Boolean){
        sharedPreferences.edit { putBoolean(keyHasEnrolledNotifications, hasEnrolled) }
    }

    fun loadHasSkippedRegistration(): Boolean {
        return sharedPreferences.getBoolean(keyHasSkippedRegistration, false)
    }

    fun saveHasSkippedRegistration(hasSkippedRegistration : Boolean){
        sharedPreferences.edit { putBoolean(keyHasSkippedRegistration, hasSkippedRegistration) }
    }

    fun loadShowCardSubscriptionAd(): Boolean {
        return sharedPreferences.getBoolean(keyShowCardAd, true)
    }

    fun saveShowCardSubscriptionAd(hasSeenWelcome : Boolean){
        sharedPreferences.edit { putBoolean(keyShowCardAd, hasSeenWelcome) }
    }

    fun loadHasSkippedLocationSettings(): Boolean {
        return sharedPreferences.getBoolean(keyHasSkippedLocationSettings, false)
    }

    fun saveHasSkippedLocationSettings(hasSkipped : Boolean){
        sharedPreferences.edit { putBoolean(keyHasSkippedLocationSettings, hasSkipped) }
    }

    fun loadUserLocation() : Location?{
        val location = Location("dummyProvider")
        val latitude = sharedPreferences.getFloat(keyUserLastLocationLatitude, 0f)
        val longitude = sharedPreferences.getFloat(keyUserLastLocationLongitude, 0f)

        if(latitude == 0f || longitude == 0f){
            return null
        }

        location.latitude = latitude.toDouble()
        location.longitude = longitude.toDouble()
        return location
    }

    fun saveUserLocation(location : Location){
        sharedPreferences.edit { putFloat(keyUserLastLocationLatitude, location.latitude.toFloat()) }
        sharedPreferences.edit { putFloat(keyUserLastLocationLongitude, location.longitude.toFloat()) }
    }

    fun loadHasSentPushToken(): Boolean {
        return sharedPreferences.getBoolean(keyHasSentPushToken, false)
    }

    fun saveHasSentPushToken(hasSentToken : Boolean){
        sharedPreferences.edit { putBoolean(keyHasSentPushToken, hasSentToken) }
    }

    fun clearAllData(){
        sharedPreferences.edit{clear()}
    }

    fun saveLanguageData(language: String){
        sharedPreferences.edit{ putString(LANGUAGE_DETAILS, language)}
    }

    fun getLanguageData(): String? {
        return sharedPreferences.getString(LANGUAGE_DETAILS, "")
    }
}