package com.app.coffeeclub.utilities.helpers

import android.content.Context
import com.app.coffeeclub.models.StripeSubscription
import com.app.coffeeclub.utilities.R
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.temporal.ChronoUnit
import java.util.*

object SubscriptionHelper {

    fun getSubscriptionTimeLine(context: Context, subscription: StripeSubscription) : String {
        val periodStart = LocalDateTime.ofInstant(Instant.ofEpochSecond(subscription.currentPeriodStart.toLong()), ZoneId.systemDefault())
        val periodEnd = LocalDateTime.ofInstant(Instant.ofEpochSecond(subscription.currentPeriodEnd.toLong()), ZoneId.systemDefault())
        val timeLineFormat = context.getString(R.string.profile_edit_subscription_payment_timeline_format)

        return String.format(timeLineFormat, DateHelper.formatDateShort(periodStart), DateHelper.formatDateShort(periodEnd))
    }

    fun getSubscriptionNextPayment(context: Context, subscription: StripeSubscription): String {
        val nextPayment = Date(subscription.currentPeriodEnd.toLong() * 1000)
        val nextPaymentFormat = context.getString(R.string.profile_edit_subscription_next_payment_format)
        return String.format(nextPaymentFormat, DateHelper.formatDate(nextPayment))
    }

    fun getFreeTrial(context: Context, subscription: StripeSubscription, hasCreditCard: Boolean): String {
        val periodEnd = LocalDateTime.ofInstant(Instant.ofEpochSecond(subscription.currentPeriodEnd.toLong()), ZoneId.systemDefault())

        val remainingDays = LocalDateTime.now().until(periodEnd, ChronoUnit.DAYS).plus(1)
        val freeTrialFormat = context.getString(if(hasCreditCard) {
            R.string.profile_edit_subscription_free_subscription_has_credit_card
        } else {
            R.string.profile_edit_subscription_free_subscription
        })

        return String.format(freeTrialFormat, remainingDays, DateHelper.formatDateShort(periodEnd))
    }
}