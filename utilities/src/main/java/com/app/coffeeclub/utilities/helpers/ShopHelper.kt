package com.app.coffeeclub.utilities.helpers

import android.location.Location
import com.app.coffeeclub.models.Shop
import com.app.coffeeclub.models.custom.ShopAdCard

object ShopHelper {
    fun sortListByDistance(userLocation: Location?, shops: MutableList<Shop>): List<Shop> {
        userLocation ?: return shops

        shops.sortBy { shop ->
            val shopLatitude = shop.latitude
            val shopLongitude = shop.longitude

            if (shopLatitude == null || shopLongitude == null) {
                Float.MAX_VALUE
            } else {
                LocationHelper.getDistanceToShop(shop, userLocation)
            }
        }

        //Place ad card at the right position
        if(shops.any { it is ShopAdCard }){
            val adCard = shops.single { it is ShopAdCard }
            shops.remove(adCard)
            shops.add(1, adCard)
        }

        return shops
    }
}