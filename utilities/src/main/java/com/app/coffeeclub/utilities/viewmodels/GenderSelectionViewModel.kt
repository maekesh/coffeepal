package com.app.coffeeclub.utilities.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.coffeeclub.models.UserDto

class GenderSelectionViewModel : ViewModel() {

    val selectedGender = MutableLiveData<UserDto.SexEnum>()
}