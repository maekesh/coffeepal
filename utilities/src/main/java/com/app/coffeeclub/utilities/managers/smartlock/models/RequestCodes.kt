package com.app.coffeeclub.utilities.managers.smartlock.models


class RequestCodes private constructor() {

    companion object {

        @JvmField
        val RC_CREDENTIALS_REQUEST = 1

        @JvmField
        val RC_HINT_REQUEST = 2

        @JvmField
        val RC_CREDENTIAL_SAVE = 3
    }

}

