package com.app.coffeeclub.utilities.helpers

import android.content.Context
import androidx.core.content.ContextCompat
import com.app.coffeeclub.resources.R
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import java.util.*

object DateOfBirthHelper {

    private const val MINIMUM_AGE = 15
    private const val DEFAULT_YEAR = 2000

    fun buildDatePicker(context: Context, selectedDate : Calendar? = null, onDateSelected: (date : Date) -> Unit): DatePickerDialog {
        val maxDate = Calendar.getInstance().apply {
            add(Calendar.YEAR, -MINIMUM_AGE) //Start calendar 15 years from now
        }
        val defaultDate = Calendar.getInstance().apply {
            set(DEFAULT_YEAR, 0, 1)
        }

        val datePickerDialog = DatePickerDialog.newInstance(
            { _, year, monthOfYear, dayOfMonth ->
                val date = Calendar.getInstance()
                date.set(Calendar.YEAR, year)
                date.set(Calendar.MONTH, monthOfYear)
                date.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                date.set(Calendar.HOUR, 0)
                date.set(Calendar.MINUTE, 0)

                onDateSelected.invoke(date.time)
            },
            selectedDate ?: defaultDate
        )

        datePickerDialog.showYearPickerFirst(true)
        datePickerDialog.isThemeDark = true
        datePickerDialog.maxDate = maxDate
        datePickerDialog.setCancelColor(ContextCompat.getColor(context, R.color.gray100))
        datePickerDialog.setOkColor(ContextCompat.getColor(context, R.color.gray100))
        datePickerDialog.accentColor = ContextCompat.getColor(context, R.color.colorAccent)

        return datePickerDialog
    }

}