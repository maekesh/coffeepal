package com.app.coffeeclub.utilities.helpers

import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.text.SimpleDateFormat
import java.util.*

object DateHelper {
    private val dateFormat = SimpleDateFormat("d. MMM yyyy", Locale.getDefault())
    private val dateFormatShort = DateTimeFormatter.ofPattern("d. MMM", Locale.getDefault())

    fun formatDate(date: Date) : String {
        return dateFormat.format(date)
    }

    fun formatDateShort(date: LocalDateTime) : String {
        return dateFormatShort.format(date)
    }


}