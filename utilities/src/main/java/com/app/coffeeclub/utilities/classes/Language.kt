package com.app.coffeeclub.utilities.classes

import com.app.coffeeclub.utilities.helpers.SharedPreferencesHelper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

object Language{

    fun getLanguageHashMap() : HashMap<*, *> {
        val resultHashMap = jacksonObjectMapper().readValue(SharedPreferencesHelper.getLanguageData(), HashMap::class.java)
        return resultHashMap
    }
}