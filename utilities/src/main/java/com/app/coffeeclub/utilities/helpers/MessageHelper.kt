package com.app.coffeeclub.utilities.helpers

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.ViewCompat
import com.app.coffeeclub.utilities.R
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import dk.idealdev.utilities.px

object MessageHelper {

    fun showErrorMessageSnackbar(anchor: View, message: CharSequence = anchor.context.getString(R.string.general_error_generic), topMargin: Int?) {
        val snackbar = Snackbar.make(anchor, message, Snackbar.LENGTH_SHORT)
        setSnackbarStyling(
            snackbar = snackbar,
            context = anchor.context,
            topMargin = topMargin ?: anchor.context.resources.getDimension(R.dimen.spacing_large).toInt())
        snackbar.show()
    }

    fun showSuccessMessageSnackbar(anchor: View, message: String, topMargin: Int?) {
        val snackbar = Snackbar.make(anchor, message, Snackbar.LENGTH_SHORT)
        setSnackbarStyling(
            snackbar = snackbar,
            context = anchor.context,
            topMargin = topMargin ?: anchor.context.resources.getDimension(R.dimen.spacing_large).toInt()
        )
        snackbar.show()
    }

    fun showSnackbarWithAction(
            anchor: View,
            message: String,
            actionText: String,
            actionListener: View.OnClickListener,
            dismissedListener: ((Int) -> Unit)? = null,
            topMargin: Int?
    ) {
        val snackbar = Snackbar.make(anchor, message, Snackbar.LENGTH_LONG)
        snackbar.setAction(actionText, actionListener)

        if (dismissedListener != null) {
            snackbar.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar?>() {
                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                    super.onDismissed(transientBottomBar, event)
                    dismissedListener.invoke(event)
                }
            })
        }

        setSnackbarStyling(
                snackbar = snackbar,
                context = anchor.context,
                topMargin = topMargin ?: anchor.context.resources.getDimension(R.dimen.spacing_large).toInt()
        )
        snackbar.show()
    }

    private fun setSnackbarStyling(snackbar: Snackbar, context: Context, topMargin: Int = 0) {
        snackbar.setCoffeeClubColors(context)
        snackbar.setCoffeeClubLayout(context, topMargin)
    }

    private fun Snackbar.setCoffeeClubColors(context: Context) {
        val tvMessage = view.findViewById<TextView>(R.id.snackbar_text)
        tvMessage.setTextColor(ContextCompat.getColor(context, R.color.gray800))
        tvMessage.typeface = ResourcesCompat.getFont(context, R.font.open_sans_semi_bold)
        tvMessage.textAlignment = View.TEXT_ALIGNMENT_CENTER
        val tvAction = view.findViewById<TextView>(R.id.snackbar_action)
        tvAction.setTextColor(ContextCompat.getColor(context, R.color.purple))
        tvAction.isAllCaps = false
    }

    private fun Snackbar.setCoffeeClubLayout(context: Context, topMargin: Int = 0) {
        val sideMargins = context.resources.getDimension(R.dimen.spacing_extra_large).toInt()
        val offset = context.resources.getDimension(R.dimen.spacing_extra_small).toInt()

        val params = view.layoutParams
        if (params is CoordinatorLayout.LayoutParams) {
            params.setMargins(sideMargins, topMargin + offset, sideMargins, 16.px)
            params.gravity = Gravity.TOP
        } else if (params is FrameLayout.LayoutParams) {
            params.setMargins(sideMargins, topMargin + offset, sideMargins, 16.px)
            params.gravity = Gravity.TOP
        }

        view.layoutParams = params
        view.background = context.getDrawable(R.drawable.snackbar_background)

        ViewCompat.setElevation(view, 6f)
    }
}