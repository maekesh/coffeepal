package com.app.coffeeclub.utilities.helpers

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.core.view.isVisible
import com.app.coffeeclub.models.HourMinutePair
import com.app.coffeeclub.models.MenuItem
import com.app.coffeeclub.models.OpeningDay
import com.app.coffeeclub.resources.R
import org.threeten.bp.DayOfWeek
import org.threeten.bp.format.TextStyle
import java.lang.StringBuilder
import java.math.BigDecimal
import java.util.*

object ClaimHelper {

    fun getMenuTitlesString(menu: MutableList<MenuItem>) : String {
        val stringBuilder = StringBuilder()
        for(menuItem in menu) {
            stringBuilder.appendln(menuItem.title)
        }
        return stringBuilder.toString()
    }

    fun getOpeningDayString(openingDays: List<OpeningDay>) : String {
        val stringBuilder = StringBuilder()
        val daysDisplayNames = getDaysDisplayName()

        for ((index, _)  in openingDays.withIndex()) {
            stringBuilder.appendln(daysDisplayNames[index].capitalize())
        }
        return stringBuilder.toString()
    }

    fun getOpeningTimeString(context: Context, openingDays: List<OpeningDay>) : String {
        val stringBuilder = StringBuilder()

        for (openingDay in openingDays) {
            val startTime = getTimeString(openingDay.start)
            val endTime = getTimeString(openingDay.end)
            if(openingDay.isClosed || startTime.isBlank() || endTime.isBlank()) {
                stringBuilder.appendln(context.getString(R.string.general_closed))
            } else {
                val time = String.format(context.getString(R.string.general_time, "$startTime - $endTime"))
                stringBuilder.appendln(time)
            }
        }
        return stringBuilder.toString()
    }

    private fun getTimeString(hourMinutePair: HourMinutePair) : String {
        if(hourMinutePair.minute == null || hourMinutePair.hour == null){
            return ""
        }

        return "${getFormattedTimeString(hourMinutePair.hour)}:${getFormattedTimeString(hourMinutePair.minute)}"
    }

    private fun getFormattedTimeString(unit: BigDecimal): String {
        return when {
            unit < 10.toBigDecimal() -> "0$unit"
            else -> "$unit"
        }
    }

    private fun getDaysDisplayName(): List<String> {
        return listOf(
            DayOfWeek.MONDAY.getDisplayName(TextStyle.FULL_STANDALONE , Locale.getDefault()),
            DayOfWeek.TUESDAY.getDisplayName(TextStyle.FULL_STANDALONE , Locale.getDefault()),
            DayOfWeek.WEDNESDAY.getDisplayName(TextStyle.FULL_STANDALONE , Locale.getDefault()),
            DayOfWeek.THURSDAY.getDisplayName(TextStyle.FULL_STANDALONE , Locale.getDefault()),
            DayOfWeek.FRIDAY.getDisplayName(TextStyle.FULL_STANDALONE , Locale.getDefault()),
            DayOfWeek.SATURDAY.getDisplayName(TextStyle.FULL_STANDALONE , Locale.getDefault()),
            DayOfWeek.SUNDAY.getDisplayName(TextStyle.FULL_STANDALONE , Locale.getDefault())
        )
    }

    fun initShowAll(context: Context, tvLeft: TextView, tvRight: TextView?, viewGradient: View, tvShowAll: TextView) {
        // tvLeft and tvRight should have same linecount
        val shouldShowAll = (tvLeft.lineCount - 1) > context.resources.getInteger(R.integer.min_lines_show_all)

        viewGradient.isVisible = shouldShowAll
        tvShowAll.isVisible = shouldShowAll

        setTextViewLines(context, tvLeft, !shouldShowAll)
        setTextViewLines(context, tvRight, !shouldShowAll)

        if(shouldShowAll) {
            tvShowAll.setOnClickListener {
                onTvShowAllClicked(context, tvLeft, tvRight, viewGradient, tvShowAll)
            }
        }
    }

    private fun onTvShowAllClicked(context: Context, tvLeft: TextView, tvRight: TextView?, viewGradient: View, tvShowAll: TextView) {
        val visible = viewGradient.isVisible

        viewGradient.isVisible = !visible
        setTextViewLines(context, tvLeft, visible)
        setTextViewLines(context, tvRight, visible)
        tvShowAll.text = context.getString(if(visible) {
            R.string.general_show_fewer
        } else {
            R.string.general_show_all
        })
    }

    private fun setTextViewLines(context: Context, textView: TextView?, visible: Boolean) {
        textView?.maxLines = (if(visible) {
            context.resources.getInteger(R.integer.max_lines_show_all)
        } else {
            context.resources.getInteger(R.integer.min_lines_show_all)
        })
    }
}