package com.app.coffeeclub.utilities.textwatchers

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.app.coffeeclub.utilities.helpers.CreditCardHelper
import java.lang.StringBuilder

/**
 * Source: https://droidmentor.com/credit-card-form/
 */
class CreditCardExpiryTextWatcher(etCard: EditText) : TextWatcher {

    private var etCard: EditText? = etCard
    private var isDelete: Boolean = false

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        isDelete = before != 0
    }

    override fun afterTextChanged(s: Editable) {
        val source = s.toString()

        CreditCardHelper.formatExpiryString(source, isDelete, etCard)
    }

}