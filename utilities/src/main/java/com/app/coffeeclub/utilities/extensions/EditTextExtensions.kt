package com.app.coffeeclub.utilities.extensions

import android.os.Build
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.app.coffeeclub.resources.R
import dk.idealdev.utilities.afterTextChanged


fun EditText.validate(validator: (String) -> Boolean, message: String, tvLabel: TextView? = null, tvError: TextView? = null, ivStatus: ImageView? = null, hasFocus: Boolean, isNotFinalCheck: Boolean = true) : Boolean {
    this.afterTextChanged {
        this.setErrorState(validator(it), message, tvLabel, tvError, ivStatus, hasFocus)
    }
    val isValid = validator(this.text.toString()) || (this.text.isNullOrBlank() && isNotFinalCheck)
    this.setErrorState(isValid, message, tvLabel, tvError, ivStatus, hasFocus)
    return isValid
}

private fun EditText.setErrorState(isValid: Boolean, message: String, tvLabel: TextView? = null, tvError: TextView? = null, ivStatus: ImageView? = null, hasFocus: Boolean) {
    with(this) {
        setCursorDrawable(if(!isValid && hasFocus) R.drawable.cursor_error else R.drawable.cursor_standard)
        setTextColor(ContextCompat.getColor(context, if(isValid || hasFocus) R.color.gray100 else R.color.red))
    }
    tvLabel?.setTextColor(ContextCompat.getColor(context, when {
        !isValid && hasFocus -> R.color.red_light
        hasFocus -> R.color.purple_light
        else -> R.color.gray100
    }))
    tvError?.text = if(isValid) null else message
    tvError?.isVisible = !isValid
    isActivated = !isValid
    ivStatus?.isActivated = !isValid
    ivStatus?.visibility = if(hasFocus) View.VISIBLE else View.INVISIBLE
}

private fun EditText.setCursorDrawable(drawableRes: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        this.setTextCursorDrawable(drawableRes)
    } else {
        try {
            val field = TextView::class.java.getDeclaredField("mCursorDrawableRes")
            field.isAccessible = true
            field.set(this, drawableRes)
        } catch (ignored: Exception) {}
    }
}
