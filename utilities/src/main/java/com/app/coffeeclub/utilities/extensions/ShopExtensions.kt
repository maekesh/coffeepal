package com.app.coffeeclub.utilities.extensions

import android.content.Context
import com.app.coffeeclub.models.OpeningDay
import com.app.coffeeclub.models.Shop
import com.app.coffeeclub.utilities.R.string
import com.google.android.gms.maps.model.LatLng
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.TextStyle
import java.util.*

val Shop.latLng: LatLng?
    get() {
        if(latitude == null || longitude == null){
            return null
        }
        return LatLng(latitude.toDouble(), longitude.toDouble())
    }

fun Shop.isOpen(): Boolean {
    val now = LocalDateTime.now()
    val nowDayOfWeekOrdinal = now.dayOfWeek.ordinal

    val openingDay = openingHours.singleOrNull { it.day.toInt() == nowDayOfWeekOrdinal }

    openingDay ?: return false

    if(openingDay.isClosed){
        return false
    }

    val startHourBigDecimal = openingDay.start.hour

    startHourBigDecimal ?: return false

    if(openingDay.end.minute == null || openingDay.end.hour == null){
        return false
    }

    val startHour = startHourBigDecimal.toInt()
    val startMinute = openingDay.start.minute.toInt()

    val endHour = openingDay.end.hour.toInt()
    val endMinute = openingDay.end.minute.toInt()

    var isAfterOpening = now.hour >= startHour
    var isBeforeClosing = now.hour <= endHour

    if (now.hour == startHour) {
        isAfterOpening = now.minute >= startMinute
    }

    if (now.hour == endHour) {
        isBeforeClosing = now.minute <= endMinute
    }

    return !openingDay.isClosed && isAfterOpening && isBeforeClosing
}

fun Shop.getOpensOrClosesAtString(context: Context): String {
    val now = LocalDateTime.now()
    val nowDayOfWeekOrdinal = now.dayOfWeek.ordinal

    if (isOpen()) {

        val openingDay = openingHours.singleOrNull { it.day.toInt() == nowDayOfWeekOrdinal }
        openingDay ?: return ""

        val closesAtTime: String
        val end = openingDay.end

        if(end.minute == null || end.hour == null){
            return ""
        }

        closesAtTime = if (end.minute.toInt() > 0) {
            "${end.hour}:${end.minute}"
        } else {
            "${end.hour}"
        }
        return context.getString(string.shopsoverview_closes_at, closesAtTime)
    } else {

        var foundNextOpeningDay = false
        var openingDayNext: OpeningDay? = null
        var nextDay: LocalDateTime = LocalDateTime.now()
        
        if(openingHours.all { it.isClosed }){
            return ""
        }

        //Find next opening day
        while (!foundNextOpeningDay) {

            val nextDayOfWeekOrdinal = nextDay.dayOfWeek.ordinal
            openingDayNext = openingHours.singleOrNull { it.day.toInt() == nextDayOfWeekOrdinal }

            //Check all opening days which are not closed
            if (openingDayNext != null && !openingDayNext.isClosed) {
                //Check if shop has opened today but not open yet
                if (openingDayNext.day.toInt() == nowDayOfWeekOrdinal) {

                    val startHour = openingDayNext.start.hour
                    //Shop has opened today but not open yet
                    if (startHour != null && now.hour < startHour.toInt()) {
                        foundNextOpeningDay = true
                    }

                } else if (openingDayNext.day.toInt() != nowDayOfWeekOrdinal) {
                    foundNextOpeningDay = true
                }
            }

            if (!foundNextOpeningDay) {
                nextDay = nextDay.plusDays(1)
            }
        }

        openingDayNext ?: return ""

        var opensAtTime = ""
        val start = openingDayNext.start

        start.minute ?: return ""

        val openingDayNextDayOfWeek = DayOfWeek.of(openingDayNext.day.toInt() + 1)
        val isOpeningDayToday = nowDayOfWeekOrdinal != openingDayNextDayOfWeek.ordinal
        var openingOtherDayString = ""
        if (isOpeningDayToday) {
            openingOtherDayString = openingDayNextDayOfWeek.getDisplayName(
                    TextStyle.SHORT_STANDALONE,
                    Locale.getDefault()
            )
        }

        opensAtTime += if (start.minute.toInt() > 0) {
            "${start.hour}:${start.minute}"
        } else {
            "${start.hour}"
        }

        return if (isOpeningDayToday) {
            context.getString(
                    string.shopsoverview_opens_at_day,
                    openingOtherDayString,
                    opensAtTime
            )
        } else {
            context.getString(string.shopsoverview_opens_at, opensAtTime)
        }
    }
}

