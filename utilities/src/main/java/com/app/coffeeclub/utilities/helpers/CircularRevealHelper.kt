package com.app.coffeeclub.utilities.helpers

import android.os.Build
import android.view.View
import android.view.ViewAnimationUtils
import android.view.Window
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.animation.doOnEnd
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import kotlin.math.max

object CircularRevealHelper {

    fun showMap(window: Window, windowInsetTop: Int?, containerShopsMap: ConstraintLayout, tvBtnMap: TextView, tvBtnShops: TextView) {
        // get the center for the clipping circle
        val topMargin = windowInsetTop ?: 0
        val cx = containerShopsMap.measuredWidth - (tvBtnMap.measuredWidth / 1.5).toInt()
        val cy = tvBtnShops.y.toInt() + (tvBtnShops.measuredHeight) + topMargin

        // get the final radius for the clipping circle
        val finalRadius = max(containerShopsMap.width, containerShopsMap.height)

        // create the animator for this view (the start radius is zero)
        val anim = ViewAnimationUtils.createCircularReveal(containerShopsMap, cx, cy, 0f, finalRadius.toFloat())

        // change system ui color to light
        SystemUIHelper.setSystemUiColorLight(window)

        // make the view visible and start the animation
        containerShopsMap.isVisible = true
        anim.start()
    }

    fun showVerticalList(window: Window, windowInsetTop: Int?, containerShopsMap: ConstraintLayout, tvBtnMap: TextView, tvBtnShops: TextView) {
        // get the center for the clipping circle
        val topMargin = windowInsetTop ?: 0
        val cx = containerShopsMap.measuredWidth - (tvBtnMap.measuredWidth / 1.5).toInt()
        val cy = tvBtnMap.y.toInt() + (tvBtnMap.measuredHeight) + topMargin

        // get the initial radius for the clipping circle
        val initialRadius = max(containerShopsMap.width, containerShopsMap.height)

        // create the animation (the final radius is zero)
        val anim = ViewAnimationUtils.createCircularReveal(containerShopsMap, cx, cy, initialRadius.toFloat(), 0f)

        // reset system ui color
        SystemUIHelper.resetSystemUiColor(window)

        // make the view invisible when the animation is done
        anim.doOnEnd {
            containerShopsMap.isInvisible = true
        }

        // start the animation
        anim.start()
    }

}