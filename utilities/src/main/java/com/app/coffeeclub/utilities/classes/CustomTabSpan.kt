package com.app.coffeeclub.utilities.classes

import android.text.style.ClickableSpan
import android.view.View
import com.app.coffeeclub.utilities.helpers.CustomTabHelper

class CustomTabSpan(private val url: String) : ClickableSpan() {

    override fun onClick(view: View) {
        CustomTabHelper.openCustomTab(view.context, url)
    }
}
