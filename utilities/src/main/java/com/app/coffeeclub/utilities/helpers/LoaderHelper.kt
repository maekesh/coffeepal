package com.app.coffeeclub.utilities.helpers

import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.app.coffeeclub.utilities.R

import android.view.View.GONE
import androidx.core.view.isVisible
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import io.supercharge.shimmerlayout.ShimmerLayout

object LoaderHelper {


    fun showIsLoadingData(
        loaderContainer: View?,
        loaderText: TextView?,
        loadingText: String?
    ) {
        if (loaderText != null && loadingText != null && !loadingText.isEmpty()) {
            loaderText.text = loadingText
        }

        if (loaderContainer != null) {
            loaderContainer.visibility = View.VISIBLE
        }
    }

    fun hideIsLoadingData(loaderContainer: View?) {
        if (loaderContainer != null) {
            loaderContainer.visibility = GONE
        }
    }


    fun hideIsLoading(shimmerContainer : ShimmerLayout? = null, swipeRefresh: SwipeRefreshLayout? = null) {

        if (swipeRefresh != null && swipeRefresh.isRefreshing) {
            swipeRefresh.isRefreshing = false
        }

        if (shimmerContainer != null) {
            shimmerContainer.stopShimmerAnimation()
            shimmerContainer.animate()
                .alpha(0f)
                .setDuration(300)
                .withEndAction {
                    shimmerContainer.isVisible = false
                }
                .start()
        }
    }

    fun showIsLoading(shimmerContainer : ShimmerLayout? = null, swipeRefresh: SwipeRefreshLayout? = null) {
        if(swipeRefresh != null && swipeRefresh.isRefreshing){
            // Prevent shimmer from being shown at the same time as swipeRefresh
            return
        }

        if (shimmerContainer != null) {
            shimmerContainer.isVisible = true
            shimmerContainer.alpha = 1f
            shimmerContainer.startShimmerAnimation()
        }
    }
}
