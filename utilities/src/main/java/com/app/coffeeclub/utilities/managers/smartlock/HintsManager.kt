package com.app.coffeeclub.utilities.managers.smartlock

import android.content.Intent
import android.content.IntentSender
import androidx.appcompat.app.AppCompatActivity
import com.app.coffeeclub.utilities.managers.smartlock.models.RequestCodes
import com.app.coffeeclub.utilities.managers.smartlock.views.HintView
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.credentials.Credentials
import com.google.android.gms.auth.api.credentials.CredentialsClient
import com.google.android.gms.auth.api.credentials.HintRequest
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import java.lang.Exception
import java.lang.ref.WeakReference

class HintsManager private constructor(builder: HintsManagerBuilder) {

    companion object {

        @JvmStatic
        fun Builder(activity: AppCompatActivity): HintsManagerBuilder
            = HintsManagerBuilder(activity)

        @JvmStatic
        fun withBuilder(hintsManagerBuilder: HintsManagerBuilder): HintsManager {
            return HintsManager(hintsManagerBuilder)
        }
    }

    private var activity: AppCompatActivity

    private var credentialsClient: CredentialsClient
    private var hintRequest: HintRequest? = null

    private var hintView: WeakReference<HintView>

    init {
        activity = builder.activity
        credentialsClient = Credentials.getClient(activity)

        hintRequest = builder.hintRequest

        hintView = WeakReference(builder.hintView)
    }

    fun requestEmailHints() {
        val intent = hintRequest?.let { credentialsClient.getHintPickerIntent(it) } ?: return
        try {
            activity.startIntentSenderForResult(intent.intentSender,
                RequestCodes.RC_HINT_REQUEST,
                null,
                0,
                0,
                0)
        } catch (e: IntentSender.SendIntentException) {
            hintView.get()?.emailHintRequestFailure()
        }
    }

    fun handleEmailHintRequest(resultCode: Int, data: Intent?) {
        if (resultCode == AppCompatActivity.RESULT_CANCELED) {
            hintView.get()?.emailHintRequestCancelled()
        } else {
            val credential: Credential? = data?.getParcelableExtra(Credential.EXTRA_KEY)
            hintView.get()?.onCredentialSelected(credential)
        }
    }

    fun saveCredential(credential: Credential) {
        credentialsClient
            .save(credential)
            ?.addOnCompleteListener { task ->
                handleCredentialSaveResult(task)
            }
    }

    fun handleCredentialSave(resultCode: Int) {
        if (resultCode == AppCompatActivity.RESULT_CANCELED) {
            hintView.get()?.credentialSaveResolutionCancelled()
        } else {
            hintView.get()?.credentialSaveSuccess()
        }
    }

    fun deleteCredential(credential: Credential, onCompleteListener: OnCompleteListener<Void>) {
        credentialsClient
            .delete(credential)
            ?.addOnCompleteListener(onCompleteListener)
    }

    fun clear() {
        hintView.clear()
    }

    private fun handleCredentialSaveResult(task: Task<Void>) {
        if (task.isSuccessful) {
            hintView.get()?.credentialSaveSuccess()
        } else {
            resolveCredentialSaveResult(task.exception)
        }
    }

    private fun resolveCredentialSaveResult(exception: Exception?) {
        if (exception is ResolvableApiException) {
            initiateCredentialSaveResultResolution(exception)
        } else {
            hintView.get()?.credentialSaveResolutionFailure()
        }
    }

    private fun initiateCredentialSaveResultResolution(exception: ResolvableApiException) {
        try {
            exception.startResolutionForResult(activity, RequestCodes.RC_CREDENTIAL_SAVE)
        } catch (e: IntentSender.SendIntentException) {
            hintView.get()?.credentialSaveFailure()
        }
    }

    class HintsManagerBuilder internal constructor(val activity: AppCompatActivity) {

        lateinit var hintView: HintView
            private set

        lateinit var hintRequest: HintRequest
            private set

        fun withHintsView(hintView: HintView) = apply {
            this.hintView = hintView
        }

        fun withHintRequest(hintRequest: HintRequest) = apply {
            this.hintRequest = hintRequest
        }

        fun build(): HintsManager =
            withBuilder(this)
    }

}


