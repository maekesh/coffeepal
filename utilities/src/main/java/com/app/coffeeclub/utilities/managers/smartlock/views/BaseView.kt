package com.app.coffeeclub.utilities.managers.smartlock.views

import com.google.android.gms.auth.api.credentials.Credential

interface BaseView {
    fun onCredentialSelected(credential: Credential?)
}