package com.app.coffeeclub.utilities.extensions

import android.widget.Button
import android.widget.EditText
import android.widget.TextView


fun EditText.showHintText(key: String, dataHashMap: HashMap<*, *>) {
    val data : String = dataHashMap.get(key).toString()
    this.hint = data
}

fun getStringData(key: String, dataHashMap: HashMap<*, *>) : String {
        return dataHashMap.get(key).toString()
}

fun TextView.showText(key: String, dataHashMap: HashMap<*, *>) {
    val data : String = dataHashMap.get(key).toString()
    this.text = data
}

fun Button.showButtonText(key: String, dataHashMap: HashMap<*, *>) {
    val data : String = dataHashMap.get(key).toString()
    this.text = data
}

fun getLocalizationText(key: String, dataHashMap: HashMap<*, *>) : String {
    return dataHashMap.get(key).toString()
}

