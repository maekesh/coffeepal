package com.app.coffeeclub.utilities.helpers

import android.content.Context
import android.content.Intent
import com.google.firebase.messaging.FirebaseMessaging

object AppResetHelper {

    fun resetApplication(context: Context) {
        resetAppData()
        restartApp(context)
    }

    private fun resetAppData() {
        SharedPreferencesHelper.clearAllData()
        FirebaseMessaging.getInstance().isAutoInitEnabled = false
    }

    private fun restartApp(context: Context) {
        val intent = context.packageManager
                .getLaunchIntentForPackage(context.packageName)
        intent?.let {
            it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(it)
        }
    }
}