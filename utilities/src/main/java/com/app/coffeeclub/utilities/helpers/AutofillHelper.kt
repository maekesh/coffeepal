package com.app.coffeeclub.utilities.helpers

import android.os.Build
import android.os.Bundle
import android.view.autofill.AutofillManager

object AutofillHelper {

    const val AUTOFILL_ID_ARG = "AUTOFILL_ID_ARG"

    fun getNextAutofillId(autofillManager: AutofillManager?) : Bundle? {
        val bundle = Bundle()
        return if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            bundle.putParcelable(AUTOFILL_ID_ARG, autofillManager?.nextAutofillId)
            bundle
        } else {
            null
        }
    }

}