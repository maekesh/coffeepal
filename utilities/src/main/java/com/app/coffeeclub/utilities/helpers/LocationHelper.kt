package com.app.coffeeclub.utilities.helpers

import android.location.Location
import com.app.coffeeclub.models.Shop

object LocationHelper {

    fun getDistanceToShop(shop: Shop?, location: Location?) : Float {
        location ?: return Float.MAX_VALUE
        val shopLatitude = shop?.latitude ?: 0.toBigDecimal()
        val shopLongitude = shop?.longitude ?: 0.toBigDecimal()

        val shopLocation = Location("dummyprovider").apply {
            latitude = shopLatitude.toDouble()
            longitude = shopLongitude.toDouble()
        }

        return shopLocation.distanceTo(location)
    }
}