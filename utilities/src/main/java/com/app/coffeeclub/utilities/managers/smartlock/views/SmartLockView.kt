package com.app.coffeeclub.utilities.managers.smartlock.views

import java.lang.Exception

interface SmartLockView : BaseView {
    fun credentialRequestCancelled() {}
    fun credentialRequestFailure(exception: Exception?) {}
}
