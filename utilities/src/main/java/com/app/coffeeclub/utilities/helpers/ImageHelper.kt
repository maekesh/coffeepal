package com.app.coffeeclub.utilities.helpers

import com.app.coffeeclub.models.ImageFile

object ImageHelper {

    fun getCoverUrl(images: List<ImageFile>) : String {
        var url = ""
        if(images.isNotEmpty()) {
            val versions = images.first().versions
            url = when {
                versions.size > 1 -> versions[1].url
                versions.isNotEmpty() -> versions.first().url
                else -> ""
            }
        }
        return url
    }

}