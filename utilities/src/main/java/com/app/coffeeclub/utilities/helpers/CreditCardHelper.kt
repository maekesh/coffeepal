package com.app.coffeeclub.utilities.helpers

import android.widget.EditText
import android.widget.TextView
import com.app.coffeeclub.models.User
import com.stripe.android.model.Card
import java.math.BigDecimal
import java.util.*

object CreditCardHelper {

    fun formatExpiryString(source: String = "", isDelete: Boolean = false, editText: EditText? = null, textView: TextView? = null) {
        val length = source.length

        val stringBuilder = StringBuilder()
        stringBuilder.append(source)

        if (length == 3) {
            if (isDelete) {
                stringBuilder.deleteCharAt(length - 1)
            } else {
                stringBuilder.insert(length - 1, "/")
            }

            editText?.let {
                it.setText(stringBuilder)
                it.setSelection(it.text.length)
            }

            textView?.let {
                it.text = stringBuilder
            }
        }
    }

    fun formatCreditCard(source: String = "", isDelete: Boolean = false, editText: EditText? = null, textView: TextView? = null) {
        val length = source.length

        val stringBuilder = StringBuilder()
        stringBuilder.append(source)

        if (length > 0 && length % 5 == 0) {
            if (isDelete) {
                stringBuilder.deleteCharAt(length - 1)
            } else {
                stringBuilder.insert(length - 1, " ")
            }

            editText?.let {
                it.setText(stringBuilder)
                it.setSelection(it.text.length)
            }

            textView?.let {
                it.text = stringBuilder
            }
        }
    }

    fun getCreditCard(cardNumber: String, expirationDateString: String, cvv: String) : Card? {
        if(expirationDateString.length != 5){ //Comes in format 'mm/yy'
            return null
        }

        val expirationMonthString = expirationDateString.substring(0, 2)
        val expirationYearString = expirationDateString.substring(3, expirationDateString.length)
        val cardNumberNoSpaces = cardNumber.replace(" ", "")

        return try {
            val expirationMonth = expirationMonthString.toInt()
            val expirationYear = expirationYearString.toInt()
            Card.create(cardNumberNoSpaces, expirationMonth, expirationYear, cvv)
        } catch (ex: NumberFormatException) {
            null
        }
    }

    fun hasUserActiveCreditCard(user: User?) : Boolean{
        val hasUserCreditCard = hasUserCreditCard(user)
        val hasCreditCardExpired = hasCreditCardExpired(user?.cardExpYear, user?.cardExpMonth)
        return hasUserCreditCard && !hasCreditCardExpired
    }

    fun hasUserCreditCard(user: User?) : Boolean {
        return user != null
                && !user.last4.isNullOrBlank()
                && !user.cardBrand.isNullOrBlank()
                && user.cardExpMonth != null
                && user.cardExpYear != null
    }

    private fun hasCreditCardExpired(expirationYear: BigDecimal?, expirationMonth: BigDecimal?) : Boolean {
        expirationYear ?: return false
        expirationMonth ?: return false

        val now = Calendar.getInstance(Locale.getDefault())
        val lastValidDate = Calendar.getInstance(Locale.getDefault())
        lastValidDate.set(expirationYear.toInt(), expirationMonth.toInt(), 0)

        return now.after(lastValidDate)
    }
}