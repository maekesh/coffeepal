package com.app.coffeeclub.utilities.helpers

import com.app.coffeeclub.models.User
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId

object SegmentHelper {

    fun getSegment(user: User?) : Segment {
        if(user == null || SharedPreferencesHelper.loadHasSkippedRegistration()) {
            return Segment.VISITING
        }
        val now =  LocalDateTime.now()
        if (user.subscriptionEnd!=null) {
            val expiration = Instant.ofEpochMilli(user.subscriptionEnd.time)
                    .atZone(ZoneId.systemDefault())
                    .toLocalDateTime()

            val hasCreditCard = CreditCardHelper.hasUserCreditCard(user)

            return when {
                now.isBefore(expiration) && user.isSubscriptionCancelled -> Segment.SUBSCRIBED_CHURNED
                now.isAfter(expiration) || user.isSubscriptionCancelled -> Segment.CHURNED
                hasCreditCard && user.isTrialing -> Segment.TRIALING_SUBSCRIBED
                hasCreditCard -> Segment.SUBSCRIBED
                else -> Segment.TRIALING
            }
        } else {
            return Segment.TRIALING
        }
    }

    fun Segment.isTrialing() : Boolean {
        return this == Segment.TRIALING || this == Segment.TRIALING_SUBSCRIBED
    }

    enum class Segment(val id: String) {
        VISITING("1"),
        TRIALING("2"),
        TRIALING_SUBSCRIBED("2.1"),
        SUBSCRIBED("3"),
        SUBSCRIBED_CHURNED("3.1"),
        CHURNED("4")
    }
}