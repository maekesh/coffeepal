package com.app.coffeeclub.utilities.managers.smartlock

import android.content.Intent
import android.content.IntentSender
import androidx.appcompat.app.AppCompatActivity
import com.app.coffeeclub.utilities.managers.smartlock.models.RequestCodes
import com.app.coffeeclub.utilities.managers.smartlock.views.SmartLockView
import com.google.android.gms.auth.api.credentials.*
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.tasks.Task
import java.lang.ref.WeakReference

class SmartLockManager private constructor(builder: SmartLockManagerBuilder) {

    companion object {

        @JvmStatic
        fun Builder(activity: AppCompatActivity): SmartLockManagerBuilder
            = SmartLockManagerBuilder(activity)

        @JvmStatic
        fun withBuilder(smartLockManagerBuilder: SmartLockManagerBuilder): SmartLockManager {
            return SmartLockManager(smartLockManagerBuilder)
        }
    }

    private var activity: AppCompatActivity

    private var credentialsClient: CredentialsClient
    private var credentialsRequest: CredentialRequest? = null

    private var smartLockView: WeakReference<SmartLockView>

    init {
        activity = builder.activity

        credentialsClient = Credentials.getClient(activity)
        credentialsRequest = builder.smartLockCredentialRequest

        smartLockView = WeakReference(builder.smartLockView)
    }

    fun requestCredentials() {
        credentialsRequest?.let {
            credentialsClient
                .request(it)
                ?.addOnCompleteListener { task ->
                    handleCredentialRequestResult(task)
                }
        }
    }

    fun handleCredentialRequest(resultCode: Int, data: Intent?) {
        if (resultCode == AppCompatActivity.RESULT_CANCELED) {
            smartLockView.get()?.credentialRequestCancelled()
        } else {
            val credential: Credential? = data?.getParcelableExtra(Credential.EXTRA_KEY)
            credential?.let {
                smartLockView.get()?.onCredentialSelected(it)
            }
        }
    }

    fun clear() {
        smartLockView.clear()
    }

    private fun handleCredentialRequestResult(credentialRequestResult: Task<CredentialRequestResponse>) {
        if (credentialRequestResult.isSuccessful) {
            smartLockView.get()?.onCredentialSelected(credentialRequestResult.result?.credential)
        } else {
            resolveCredentialRequest(credentialRequestResult.exception)
        }
    }

    private fun resolveCredentialRequest(exception: Exception?) {
        if (exception is ResolvableApiException) {
            initiateCredentialRequestResolution(exception)
        } else {
            smartLockView.get()?.credentialRequestFailure(exception)
        }
    }

    private fun initiateCredentialRequestResolution(exception: ResolvableApiException?) {
        try {
            exception?.startResolutionForResult(activity, RequestCodes.RC_CREDENTIALS_REQUEST)
        } catch (sendIntentException: IntentSender.SendIntentException) {
            smartLockView.get()?.credentialRequestFailure(exception)
        }
    }

    class SmartLockManagerBuilder internal constructor(val activity: AppCompatActivity) {

        lateinit var smartLockView: SmartLockView
            private set

        lateinit var smartLockCredentialRequest: CredentialRequest
            private set

        fun withSmartLockView(smartLockView: SmartLockView) = apply {
            this.smartLockView = smartLockView
        }

        fun withSmartLockCredentialRequest(smartLockCredentialRequest: CredentialRequest) = apply {
            this.smartLockCredentialRequest = smartLockCredentialRequest
        }

        fun build(): SmartLockManager =
            withBuilder(this)
    }

}

