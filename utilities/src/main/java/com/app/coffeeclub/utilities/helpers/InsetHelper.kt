package com.app.coffeeclub.utilities.helpers

import android.view.View
import android.view.ViewGroup

object InsetHelper {

    fun addTopPadding(view: View, inset: Int) {
        view.setPadding(0, inset + view.paddingBottom,0,0)
    }

    fun addBottomPadding(view: View, inset: Int) {
        view.setPadding(0, 0,0, inset + view.paddingBottom)
    }

    fun addTopMargin(viewGroup: ViewGroup, inset: Int) {
        if(viewGroup.layoutParams is ViewGroup.MarginLayoutParams) {
            val params = viewGroup.layoutParams as ViewGroup.MarginLayoutParams
            params.topMargin = inset + params.topMargin
            viewGroup.layoutParams = params
        }
    }

    fun addBottomMargin(viewGroup: ViewGroup, inset: Int) {
        if(viewGroup.layoutParams is ViewGroup.MarginLayoutParams) {
            val params = viewGroup.layoutParams as ViewGroup.MarginLayoutParams
            params.bottomMargin = inset + params.bottomMargin
            viewGroup.layoutParams = params
        }
    }

}