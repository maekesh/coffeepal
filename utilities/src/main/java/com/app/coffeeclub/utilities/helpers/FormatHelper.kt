package com.app.coffeeclub.utilities.helpers

import android.content.Context
import com.app.coffeeclub.resources.R
import com.app.coffeeclub.models.StripePlan
import java.util.*
import kotlin.math.roundToInt

object FormatHelper {

    fun formatPricePerMonth(context: Context, stripePlan: StripePlan): String {
        return String.format(
                "%d ".format((stripePlan.amount.toFloat() / 100f).toInt()) +
                        String.format(
                                context.getString(R.string.general_subscription_price_unit),
                                stripePlan.currency
                        )
        )
    }

    fun formatDistance(distance: Float, context: Context): String {
        val distanceString: String
        val arrivalThresholdInMeters = context.resources.getInteger(R.integer.shop_arrival_threshold_in_meters)

        when {
            //Below threshold show "Close by"
            distance <= arrivalThresholdInMeters -> {
                distanceString = context.getString(R.string.general_distance_close)
            }
            //Round meters to nearest 25
            distance < 1000 -> {
                distanceString = String.format(Locale.getDefault(), "%d m", (distance / 25.0).roundToInt() * 25)
            }
            //More than 10km distance do not show decimals
            distance > 10000 -> {
                distanceString = String.format(Locale.getDefault(), "%d km", (distance / 1000).roundToInt())
            }
            //Show kilometers with one decimal when below 10 km
            else -> {
                distanceString = String.format(Locale.getDefault(), "%.1f km", distance / 1000)
            }
        }

        return distanceString
    }

}