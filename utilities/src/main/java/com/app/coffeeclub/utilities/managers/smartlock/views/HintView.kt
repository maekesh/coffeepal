package com.app.coffeeclub.utilities.managers.smartlock.views

interface HintView: BaseView {
    fun credentialSaveResolutionFailure() {}
    fun credentialSaveResolutionCancelled() {}
    fun credentialSaveFailure() {}
    fun credentialSaveSuccess() {}
    fun emailHintRequestCancelled(){}
    fun emailHintRequestFailure(){}
}
