package dk.idealdev.utilities

import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

/**
 * Example usage:
 *  fun newInstance(title: String): MemberSettingsFragment = instanceOf(ARG_TITLE to title)
 */
inline fun <reified T: Fragment> instanceOf(vararg params: Pair<String, Any?>) : T
        = T::class.java.newInstance().apply { arguments = bundleOf(*params) }

/**
 * Example usage:
 * observe(centersViewModel.stateLiveData, { result ->  /* handle results */ })
 */
inline fun <reified T> Fragment.observe(data: LiveData<T>, crossinline onValue: (T) -> Unit) {
    data.observe(viewLifecycleOwner, Observer { onValue(it) })
}

/**
 * Example usage:
 * getDrawable(R.drawable.item_divider)
 */
fun Fragment.getDrawable(@DrawableRes id: Int) : Drawable? {
    return ContextCompat.getDrawable(requireContext(), id)
}

/**
 * Example usage:
 * private val isMultiSelection: Boolean by getArgument(ARG_IS_MULTI)
 */
inline fun <reified T> Fragment.getArgument(key: String): Lazy<T> = lazy {
    val args = requireArguments()
    return@lazy args.get(key) as T
}