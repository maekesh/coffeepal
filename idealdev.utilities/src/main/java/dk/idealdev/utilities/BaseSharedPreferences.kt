package dk.idealdev.utilities

import android.content.Context
import android.content.SharedPreferences


/**
 * Class wrapping common logic for saving to shared preference.
 * Should be subclasses with wrapper methods and keys for app specific data
 * @sample
 *              public fun getUserFirstName() : String{
                    return getSharedPrefsString(KEY_USER_FIRST_NAME, null);
                }

                public fun setUserFirstName(String name){
                    setSharedPrefString(KEY_USER_FIRST_NAME, name);
                }
 */
abstract class BaseSharedPreferences(context: Context) {

    private val PREF_KEY = "PREF_KEY"

    private val sharedPref: SharedPreferences

    init {
        sharedPref = context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE)
    }

    protected fun clearData() {
        val edit = sharedPref.edit()
        edit.clear()
        edit.apply()
    }

    protected fun setSharedPrefBoolean(key: String, value: Boolean) {
        val edit = sharedPref.edit()
        edit.putBoolean(key, value)
        edit.apply()
    }

    protected fun getSharedPrefBoolean(key: String, defaultValue: Boolean): Boolean {
        return sharedPref.getBoolean(key, defaultValue)
    }

    protected fun getSharedPrefInteger(key: String, defaultValue: Int): Int {
        return sharedPref.getInt(key, defaultValue)
    }

    protected fun setSharedPrefString(key: String, value: String?) {
        val edit = sharedPref.edit()
        edit.putString(key, value)
        edit.apply()
    }

    protected fun setSharedPrefInteger(key: String, value: Int) {
        val edit = sharedPref.edit()
        edit.putInt(key, value)
        edit.apply()
    }

    protected fun getSharedPrefsString(key: String, defaultValue: String?): String? {
        return sharedPref.getString(key, defaultValue)
    }
}
