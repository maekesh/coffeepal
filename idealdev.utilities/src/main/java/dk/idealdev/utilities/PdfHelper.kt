package dk.idealdev.utilities

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast


object PdfHelper {

    fun openPdf(url : String, context: Context, noReaderText : String, chooseReaderText : String){
        try {
            val browserIntent = Intent(Intent.ACTION_VIEW)
            browserIntent.setDataAndType(Uri.parse(url), "application/pdf")
            val chooser = Intent.createChooser(browserIntent, chooseReaderText)
            context.startActivity(chooser)
        } catch (ex : ActivityNotFoundException){
            Toast.makeText(context, noReaderText, Toast.LENGTH_LONG).show()
        }
    }
}