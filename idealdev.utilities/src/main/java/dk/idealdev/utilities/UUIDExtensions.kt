package dk.idealdev.utilities

import java.util.*


fun UUID.isNotEmpty() : Boolean {
    return this != UUID(0,0)
}

fun UUID.isEmpty() : Boolean {
    return this == UUID(0,0)
}

fun String.isUUID() : Boolean {
    return this.matches("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}".toRegex())
}