package dk.idealdev.utilities.response

/**
 * A generic class that holds a value with its loading status. Use for wrapping responses (e.g. network or database) in a consistent manner
 *
 * @param <T>
 * @sample
 *          Resource.success(data)
 *          Resource.error(throwable.getMessage(), throwable)
 *          Resource.loading()
</T> */
class Resource<T>(val status: Status, val data: T? = null, val message: String?, val throwable: Throwable? = null) {

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }

        val resource = other as Resource<*>?

        if (status !== resource!!.status) {
            return false
        }
        if (if (message != null) message != resource!!.message else resource!!.message != null) {
            return false
        }
        return if (data != null) data == resource.data else resource.data == null
    }

    override fun hashCode(): Int {
        var result = status.hashCode()
        result = 31 * result + (message?.hashCode() ?: 0)
        result = 31 * result + (data?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "Resource{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}'
    }

    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String?, throwable: Throwable?): Resource<T> {
            return Resource(Status.ERROR, message = msg,  throwable = throwable)
        }

        fun <T> loading(): Resource<T> {
            return Resource(Status.LOADING, null, null)
        }
    }
}