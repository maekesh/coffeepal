package dk.idealdev.utilities.response

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
