package dk.idealdev.utilities.idealmatch

enum class IdmAuthType(val type : Int)
{
    Unspecified(0),
    Facebook(1),
    LinkedIn(2),
    PhoneNr(3),
    Twitter (4),
    Firebase( 5),
    AccountKit(6);

    companion object {
        private val map = IdmAuthType.values().associateBy(IdmAuthType::type)
        fun fromInt(type: Int) = map[type]
    }
}