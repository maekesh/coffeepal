package dk.idealdev.utilities

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.SeekBar

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}

fun SeekBar.onProgressChanged(onProgressChanged : (seekBar: SeekBar?, progress: Int, fromUser: Boolean) -> Unit){
    this.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            onProgressChanged.invoke(seekBar, progress, fromUser)
        }

        override fun onStartTrackingTouch(p0: SeekBar?) {

        }

        override fun onStopTrackingTouch(p0: SeekBar?) {

        }
    })
}

@Deprecated("Use isVisible(), isInvisible() or isGone() from kotlin extensions instead", ReplaceWith("isVisible = visible", "androidx.core.view.isVisible"))
fun View.visibility(visible : Boolean?) {
    this.visibility = if (visible == null) View.INVISIBLE else if(visible) View.VISIBLE else View.GONE
}
