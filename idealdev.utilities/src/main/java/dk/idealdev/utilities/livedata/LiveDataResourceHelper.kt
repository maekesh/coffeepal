package dk.idealdev.utilities.livedata

import androidx.lifecycle.MutableLiveData
import dk.idealdev.utilities.response.Resource
import dk.idealdev.utilities.response.Status

object LiveDataResourceHelper {
    /**
     * Use cache if available and no force refresh
     * Returns true if cache data is set on livedata, otherwise false
     */
    fun <T>useCacheIfAvailable(liveData: MutableLiveData<Resource<T>>, forceRefresh: Boolean): Boolean {
        val existingData = liveData.value

        if (!forceRefresh && existingData != null && existingData.status == Status.SUCCESS) {
            liveData.postValue(liveData.value)
            return true
        }
        return false
    }
}