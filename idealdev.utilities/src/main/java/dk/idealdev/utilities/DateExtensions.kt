package dk.idealdev.utilities

import java.text.SimpleDateFormat
import java.util.*
import android.text.format.DateUtils
import java.text.ParseException

private const val ISO_8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"


fun Date.toSimpleDateString() : String {
    val format = SimpleDateFormat("d-MM-yyyy", Locale.getDefault())
    return format.format(this)
}


fun Date.toRelativeString() : String {
    val now = System.currentTimeMillis()
    return DateUtils.getRelativeTimeSpanString(this.time, now, DateUtils.DAY_IN_MILLIS).toString()
}

fun Date.toRelativeStringWithTime() : String {
    val now = System.currentTimeMillis()
    return DateUtils.getRelativeTimeSpanString(this.time, now, DateUtils.DAY_IN_MILLIS).toString()
}

fun Date.toIso8601String() : String {
    return SimpleDateFormat(ISO_8601_FORMAT, Locale.getDefault()).format(this)
}

fun String.convertIso8601DateToString(): Date? {

    val format = SimpleDateFormat(ISO_8601_FORMAT, Locale.getDefault())
    try {
        val date = format.parse(this)
        println(date)
        return date
    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return null
}



