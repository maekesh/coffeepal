package dk.idealdev.utilities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import java.util.*

object MapsHelper {
    fun launchMapWithCoordinates(activity: Activity, latitude: String, longitude: String) {
        val uri = String.format(Locale.ENGLISH, "geo:%s,%s?q=%s,%s", latitude, longitude, latitude, longitude)
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        activity.startActivity(intent)
    }

    fun launchMapWithAddress(context: Context, addressLine: String?) {
        if (addressLine == null) {
            return
        }
        val uri = "http://maps.google.com/maps?q=" + Uri.encode(addressLine.replace("\n", ", ").trim { it <= ' ' })
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        context.startActivity(intent)
    }
}