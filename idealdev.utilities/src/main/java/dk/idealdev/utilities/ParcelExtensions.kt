package dk.idealdev.utilities

import android.os.Parcel
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import java.util.*

//DATE
fun Parcel.writeDate(date: Date?) {
    writeLong(date?.time ?: -1)
}

fun Parcel.readDate(): Date {
    val long = readLong()
    return if (long != -1L) Date(long) else Date(0)
}

//INSTANT
fun Parcel.writeInstant(date: Instant?) {
    writeLong(date?.toEpochMilli() ?: -1)
}

fun Parcel.readInstant(): Instant {
    val long = readLong()
    return if (long != -1L) Instant.ofEpochMilli(long) else Instant.ofEpochMilli(0)
}

//LOCALDATETIME
fun Parcel.writeLocalDateTime(date: LocalDateTime?) {
    writeString(date?.toString())
}

fun Parcel.readLocalDateTime(): LocalDateTime {
    val long = readString()
    return if (long != null) LocalDateTime.parse(long) else LocalDateTime.ofInstant(Instant.ofEpochMilli(0), ZoneId.systemDefault())
}


//UUID
fun Parcel.writeUUID(uuid: UUID?) {
    val uuidString = uuid?.toString() ?: UUID(0,0).toString()
    writeString(uuidString)
}

fun Parcel.readUUID(): UUID {
    val readString = readString()
    return if (readString != null && readString.isUUID()) {
        UUID.fromString(readString)
    } else {
        UUID(0,0)
    }
}

//List<Int>

fun Parcel.writeIntList(input:MutableList<Int>?) {
    if(input == null) writeInt(-1)
    input?.size?.let { writeInt(it) } // Save number of elements.
    input?.forEach(this::writeInt) // Save each element.
}

fun Parcel.createIntList() : MutableList<Int>? {
    val size = readInt()
    if(size == -1) return null
    val output = ArrayList<Int>(size)
    for (i in 0 until size) {
        output.add(readInt())
    }
    return output
}

//List<Float>

fun Parcel.writeFloatList(input:MutableList<Float>?) {
    if(input == null) writeInt(-1)
    input?.size?.let { writeInt(it) } // Save number of elements.
    input?.forEach(this::writeFloat) // Save each element.
}

fun Parcel.createFloatList() : MutableList<Float>? {
    val size = readInt()
    if(size == -1) return null
    val output = ArrayList<Float>(size)
    for (i in 0 until size) {
        output.add(readFloat())
    }
    return output
}

//List<UUID>

fun Parcel.writeUUIDList(input: MutableList<UUID>?) {
    if(input == null) writeInt(-1)
    input?.size?.let { writeInt(it) } // Save number of elements.
    input?.forEach { uuid: UUID -> // Save each element.
        writeUUID(uuid)
    }
}

fun Parcel.createUUIDList() : MutableList<UUID>? {
    val size = readInt()
    if(size == -1) return null
    val outPut = ArrayList<UUID>(size)
    for (i in 0 until size) {
        outPut.add(readUUID())
    }
    return outPut
}

//Booleans
fun Parcel.readBoolean() : Boolean {
    return readByte() != 0.toByte()
}

fun Parcel.writeBoolean(boolean: Boolean) {
    writeByte(if (boolean) 1 else 0)
}