CoffeeClub Android
======================


Window Insets
---------------------
What are Window Insets?:
  WindowInsets are insets (or sizes) of system views
  (e.g. status bar, navigation bar),
  that are applied to the window of your application.

***VERY IMPORTANT***
  All Toolbars with id:
  "R.id.toolbar"
  and
  All ConstraintLayouts with id:
  "R.id.container"
  Will get a topPadding with systemWindowInsetTop and
  bottomMargin with systemWindowInsetBottom respectively.

BaseHomeFragment/BaseOnboardingFragment:
  What it does?:
    It makes sure that all fragments observes on
    WindowInsetsViewModel (Look below) and is able to react accordingly.

  Overview:
    - open fun onTopInsetChanged(inset: Int) {}
    - open fun onBottomInsetChanged(inset: Int) {}

WindowInsetsViewModel:
  What it does?:
    It takes the window insets from the activity and distributes it throughout the
    app.

  Theese can vary from device to device due to:
    - Api level
    - Notches
    - System Navigation (Gestures)

  External links:
    [WindowInsets??](https://medium.com/@azizbekian/windowinsets-24e241d4afb9)
    [Droidcon NYC 2017 - Becoming a master window fitter🔧](https://www.youtube.com/watch?v=_mGDMVRO3iE)


SmartLock
------------------------
Based on (credit to):
[AuthManager](https://github.com/charbgr/AuthManager)
[Android: Improving sign-in experience with Google Sign-In and SmartLock](https://medium.com/@p.tournaris/android-improving-sign-in-experience-with-google-sign-in-and-smartlock-f0bfd789602a)

Changes from original:
  - Updated to newest API
  - Removed GoogleAuthManager because it was not needed
  (Google Sign-In, Google Sign-Out, Google Revoke Access)

Managers:
  - HintsManager
  - SmartLockManager

HintsManager:
  This manager creates intent with a (HintRequest)[https://developers.google.com/android/reference/com/google/android/gms/auth/api/credentials/HintRequest] to get credentials.
  For a typical application that supports password based authentication
  and Google sign-in.

  **Credentials of specific Account types,
   you need to have in mind that they will not contain a password,
   due to the fact that Account type and password fields can not co-exist.**

  API overview (all but onCredentialSelected are optional):
    1. fun onCredentialSelected(credential: Credential?)
    2. fun credentialSaveResolutionFailure()
    3. fun credentialSaveResolutionCancelled()
    4. fun credentialSaveFailure()
    5. fun credentialSaveSuccess()
    6. fun emailHintRequestCancelled()
    7. fun emailHintRequestFailure

SmartLockManager:
  This manager creates integration for Google SmartLock or third party password
  managers to autofill and save credentials from the app.

  **Credential is a key element of the SmartLock domain.
  It holds all the credential information (either account type or password,
  a name and a profile picture URI) related to an E-mail address.
  Credentials can either have an Account Type or a Password.**

  API overview (all but onCredentialSelected are optional):
    1. fun onCredentialSelected(credential: Credential?)
    2. fun credentialRequestCancelled()
    3. fun credentialRequestFailure(exception: Exception?)


Features
--------

-

Notes
-----



Authors
-------

`CoffeeClub` was written at Heyday by
[Morten Niklas Sølling](https://bitbucket.org/msheyday/)
[Christian Jensen](https://bitbucket.org/%7B544d6b14-7011-43b2-b126-03414c969998%7D/)
