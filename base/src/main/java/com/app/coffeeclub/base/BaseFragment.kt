package com.app.coffeeclub.base

import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.app.coffeeclub.uicomponents.R
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.showText

abstract class BaseFragment : Fragment() {
    abstract val layoutResId: Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutResId, container, false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if(item.itemId == android.R.id.home) {
            requireActivity().onBackPressed()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        if (toolbar != null) {
            (activity as AppCompatActivity).setSupportActionBar(toolbar)
        }
    }

    protected fun showBackAsWhiteArrow() {
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_left_white)
    }

    protected fun showBackAsWhiteClose() {
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_close_white)
    }

    protected fun showBackAsBlackArrow() {
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_left_black)
    }

    protected fun showBackAsBlackClose() {
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_close_black)
    }

    protected fun setTitle(title : String){
        val toolbarTitle = view?.findViewById<TextView>(R.id.toolbarTitle)
        if (toolbarTitle != null) {
            if(!TextUtils.isEmpty(title)){
                toolbarTitle.showText(title, Language.getLanguageHashMap())
            } else {
                toolbarTitle.text = ""
            }
        } else {
            requireActivity().title = title
        }
    }

    @Deprecated("use by viewModels() for fragment lifecycle or  by activityViewModels() for accessing ViewModels scoped to the activity", ReplaceWith("activityViewModels()", "androidx.fragment.app.activityViewModels"))
    inline fun <reified T : ViewModel> viewModelProvider(requireActivity: Boolean = true) = lazy {
        return@lazy if (requireActivity && isAdded) {
            ViewModelProviders.of(requireActivity()).get(T::class.java)
        } else {
            ViewModelProviders.of(this).get(T::class.java)
        }
    }
}