package com.app.coffeeclub.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.coffeeclub.network.repository.UsersRepository
import dk.idealdev.utilities.livedata.SingleLiveEvent
import dk.idealdev.utilities.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PushViewModel : ViewModel() {
    val pushTokenPutLiveData : MutableLiveData<Resource<Void>> = SingleLiveEvent()

    fun putPushToken(token : String){
        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersMePushTokenTokenPut(pushTokenPutLiveData, token)
        }
    }
}