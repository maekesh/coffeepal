package com.app.coffeeclub.home.shopsoverview.paging

import android.location.Location
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.coffeeclub.home.R
import com.app.coffeeclub.models.custom.ShopAdCard
import com.app.coffeeclub.home.shopsoverview.ShopItemCallback
import com.app.coffeeclub.models.Shop
import com.app.coffeeclub.models.User
import com.app.coffeeclub.utilities.helpers.SharedPreferencesHelper
import dk.idealdev.utilities.response.Resource
import timber.log.Timber

class ShopsPagedListAdapter(private val callback: ShopItemCallback, private val userLiveData:
MutableLiveData<Resource<User>>) :
    PagedListAdapter<Shop, RecyclerView.ViewHolder>(ShopDiff()) {

    private val footerViewType = 2
    private val adCardViewType = 3
    private var pagingState = PagingState.LOADING_INITIAL
    private var userLocation: Location? = SharedPreferencesHelper.loadUserLocation()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            footerViewType -> {
                ListFooterViewHolder.create(parent)
            }
            adCardViewType -> {
                AdCardViewHolder.create(parent, callback, userLiveData)
            }
            else -> {
                ShopViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.row_shop,
                        parent,
                        false
                    ), callback
                )
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when {
            getItemViewType(position) == footerViewType -> (holder as ListFooterViewHolder).bind(
                pagingState
            )
            getItemViewType(position) == adCardViewType -> (holder as AdCardViewHolder).bind()
            else -> (holder as ShopViewHolder).bind(getItem(position), userLocation, position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            position >= super.getItemCount() -> {
                footerViewType
            }
            getItem(position) is ShopAdCard -> {
                adCardViewType
            }
            else -> {
                -1
            }
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (pagingState == PagingState.LOADING_PAGE || pagingState == PagingState.ERROR)
    }

    fun setPagingState(state: PagingState) {
        this.pagingState = state
        Timber.d("setPagingState: ${state.name}.")
        notifyItemChanged(super.getItemCount())
    }

    fun setUserLocation(userLocation: Location?) {
        Timber.d("Setting user location")
        this.userLocation = userLocation
        notifyDataSetChanged()
    }

    private class ShopDiff : DiffUtil.ItemCallback<Shop>() {
        override fun areItemsTheSame(oldItem: Shop, newItem: Shop): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Shop, newItem: Shop): Boolean {
            return oldItem.name == newItem.name &&
                    oldItem.city == newItem.city &&
                    oldItem.company == newItem.company &&
                    oldItem.description == newItem.description &&
                    oldItem.menu == newItem.menu &&
                    oldItem.latitude == newItem.longitude &&
                    oldItem.longitude == newItem.longitude
        }
    }
}