package com.app.coffeeclub.home.profile

import com.app.coffeeclub.home.BaseHomeFragment

abstract class BaseEditProfileFragment : BaseHomeFragment() {

    protected val profileViewModel: ProfileViewModel by viewModelProvider()

}