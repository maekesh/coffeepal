package com.app.coffeeclub.home.shopsoverview.map

import com.app.coffeeclub.models.Shop
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class ShopClusterItem(val shop : Shop) : ClusterItem {
    override fun getSnippet(): String {
        return shop.description
    }

    override fun getTitle(): String {
        return shop.name
    }

    override fun getPosition(): LatLng {
        return LatLng(shop.latitude.toDouble(), shop.longitude.toDouble())
    }
}