package com.app.coffeeclub.home.rateapp

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.coffeeclub.models.UserRatingRequestDto
import com.app.coffeeclub.network.repository.UsersRepository
import dk.idealdev.utilities.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RateAppViewModel : ViewModel() {

    val commentLiveData : MutableLiveData<String> = MutableLiveData()
    val hasRatedLiveData : MutableLiveData<Boolean> = MutableLiveData()
    val ratingLiveData : MutableLiveData<Int> = MutableLiveData()
    val putRatingLiveData : MutableLiveData<Resource<Void>> = MutableLiveData()
    val putRatingAndCommentLiveData : MutableLiveData<Resource<Void>> = MutableLiveData()

    fun submitRating() {
        val body = UserRatingRequestDto().apply {
            rating = ratingLiveData.value?.toBigDecimal()
        }

        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersMeRatingPut(putRatingLiveData, body)
        }
    }

    fun submitRatingAndComment() {
        val body = UserRatingRequestDto().apply {
            rating = ratingLiveData.value?.toBigDecimal()
            description = commentLiveData.value
        }

        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersMeRatingPut(putRatingAndCommentLiveData, body)
        }
    }

}