package com.app.coffeeclub.home

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.amulyakhare.textdrawable.TextDrawable
import com.app.coffeeclub.actions.Actions
import com.app.coffeeclub.actions.Actions.ARG_REQUEST_PERMISSIONS
import com.app.coffeeclub.base.BaseActivity
import com.app.coffeeclub.models.User
import com.app.coffeeclub.resources.R.color
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.uicomponents.dialogs.showCoffeeClubDialog
import com.app.coffeeclub.utilities.helpers.*
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.common.wrappers.InstantApps
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import dk.idealdev.utilities.livedata.observeOnce
import dk.idealdev.utilities.px
import dk.idealdev.utilities.response.Status
import com.app.coffeeclub.utilities.helpers.SegmentHelper.Segment.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.navigation_view.*
import pub.devrel.easypermissions.EasyPermissions
import timber.log.Timber
import com.google.android.gms.instantapps.InstantApps as Instant

class HomeActivity : BaseActivity(R.layout.activity_home) {

    private val windowInsetsViewModel: WindowInsetsViewModel by lazy{ ViewModelProviders.of(this).get(WindowInsetsViewModel::class.java) }
    private val homeViewModel: HomeViewModel by lazy { ViewModelProviders.of(this).get(HomeViewModel::class.java) }
    private val pushViewModel: PushViewModel by lazy { ViewModelProviders.of(this).get(PushViewModel::class.java) }

    private val locationViewModel: LocationViewModel by lazy {
        ViewModelProviders.of(this).get(LocationViewModel::class.java)
    }

    private val navController by lazy { findNavController(R.id.navHostFragment) }

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback

    companion object {
        private const val RC_LOCATION_SETTINGS = 11
        private const val RC_LOCATION = 22
        const val RC_SUBSCRIPTION = 33 //Used to trigger subscription flow from here and other fragments
        private const val RC_INSTALL_APP = 7
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN)
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
            window.statusBarColor = ContextCompat.getColor(window.context, color.gray800_alpha_50)
        }

        initWindowInsetListener()
        initWindowInsetViewModelObservers()

        checkPlayServices()

        if (intent.getBooleanExtra(ARG_REQUEST_PERMISSIONS, false)) {
            requestAppPermissions()
        } else if(!EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)){
            EasyPermissions.requestPermissions(
                this,
                getString(string.shopsoverview_location_permission_message),
                RC_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                if (locationResult.locations.any()) {
                    val newLocation = locationResult.locations.first()
                    SharedPreferencesHelper.saveUserLocation(newLocation)
                    locationViewModel.userLocationLiveData.value = newLocation
                }

                Timber.d("Location update. Got ${locationResult.locations.size} locations")
            }
        }

        checkPushNotificationsRegistry()

        setupLocationRetrieval()
    }

    /**
     * Distributes top and bottom insets to WindowInsetsViewModel
     * To get a better overview; please visit README.md
     */
    private fun initWindowInsetListener() {
        drawer_layout.setOnApplyWindowInsetsListener { _, insets ->
            if(insets.systemWindowInsetTop > 0) {
                windowInsetsViewModel.windowInsetTop.value = insets.systemWindowInsetTop
            }
            if(insets.systemWindowInsetBottom > 0) {
                windowInsetsViewModel.windowInsetBottom.value = insets.systemWindowInsetBottom
            }
            insets.consumeSystemWindowInsets()
        }
    }

    private fun initWindowInsetViewModelObservers() {
        windowInsetsViewModel.windowInsetTop.observeOnce(this, Observer {
            InsetHelper.addTopMargin(navigationViewContainer, it)
        })

        windowInsetsViewModel.windowInsetBottom.observeOnce(this, Observer {
            InsetHelper.addBottomMargin(navigationViewContainer, it)
        })
    }

    override fun onResume() {
        super.onResume()
        checkPlayServices()
        setLoggedInState()
        startLocationUpdates()
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    /**
     * Make sure the right version of Google Play Services are installed
     * https://firebase.google.com/docs/cloud-messaging/android/client?authuser=0#sample-play
     */
    private fun checkPlayServices() {
        GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this)
    }

    private fun checkPushNotificationsRegistry() {
        if (SharedPreferencesHelper.loadHasEnrolledNotifications() && !SharedPreferencesHelper.loadHasSentPushToken()) {
            enableNotifications()
        }
    }

    private fun setLoggedInState() {
        val isUserLoggedIn = SharedPreferencesHelper.loadAccessToken() != null

        setLoggedInUserInfoState(isUserLoggedIn)

        if (isUserLoggedIn) {
            initLoggedIn(findViewById(android.R.id.content))
        } else {
            initNotLoggedIn()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer()
        } else {
            super.onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SUBSCRIPTION && resultCode == Activity.RESULT_OK) {
            homeViewModel.getUser(true)
        }

        if (requestCode == RC_LOCATION_SETTINGS) {
            SharedPreferencesHelper.saveHasSkippedLocationSettings(resultCode != Activity.RESULT_OK)
        }
    }

    @SuppressLint("MissingPermission")
    private fun setupLocationRetrieval() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        if (!EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            return
        }

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    SharedPreferencesHelper.saveUserLocation(location)
                    locationViewModel.userLocationLiveData.value = location
                }
            }


    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        if (!EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            return
        }

        Timber.d("Starting location updates")

        val locationRequest = LocationRequest.create()?.apply {
            interval = 10000
            fastestInterval = 10000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        } ?: return

        checkLocationSettings(locationRequest)
    }

    @SuppressLint("MissingPermission")
    private fun checkLocationSettings(locationRequest: LocationRequest) {
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            // All location settings are satisfied. The client can initialize
            // location requests here.
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                null /* Looper */
            )
        }

        if (!SharedPreferencesHelper.loadHasSkippedLocationSettings()) {
            task.addOnFailureListener { exception ->
                if (exception is ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        exception.startResolutionForResult(
                            this,
                            RC_LOCATION_SETTINGS
                        )
                    } catch (sendEx: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }
                }
            }
        }
    }

    private fun stopLocationUpdates() {
        if (!EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            return
        }

        Timber.d("Stopping location updates")
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    fun openDrawer() {
        drawer_layout.openDrawer(GravityCompat.START)
    }

    private fun closeDrawer() {
        drawer_layout.closeDrawer(GravityCompat.START)
    }

    private fun initLoggedIn(view: View) {
        profileBtn.setOnClickListener {
            deselectMenuItems()
            SystemUIHelper.resetSystemUiColor(window)
            closeDrawer()
            navController.navigate(R.id.action_global_profileFragment)
        }

        cardNoSubscription.setOnClickListener {
            onCardNoSubscriptionClicked()
        }

        tvTerms.isGone = true
        tvPrivacy.isGone = true
        tvGoToLogin.isGone = true
        profileBtn.isVisible = true

        updateMenuTextColors()

        setNavViewClickListeners(true)

        observeUser(view)

        if (homeViewModel.getUserLiveData.value == null) {
            homeViewModel.getUser()
        }
    }

    private fun updateMenuTextColors() {
        setMenuTextColor(tvRateApp)
        setMenuTextColor(tvReferFriends)
        setMenuTextColor(tvCoffeeBars)
    }

    private fun setMenuTextColor(menuItem: TextView) {
        val textColor = ContextCompat.getColor(this, if(menuItem.isSelected) {
            color.gold
        } else {
            color.gold_lighter
        })
        menuItem.setTextColor(textColor)
    }

    private fun showInstallPrompt() {
        Instant.showInstallPrompt(this,null, RC_INSTALL_APP, null)
    }

    private fun setNavViewClickListeners(isLoggedIn: Boolean) {
        tvCoffeeBars.setOnClickListener {
            onCoffeeBarsSelected()
        }

        tvReferFriends.setOnClickListener {
            when {
                isLoggedIn -> {
                    onReferFriendsSelected()
                }
                InstantApps.isInstantApp(this) -> {
                    showInstallPrompt()
                }
                else -> {
                    showLogInErrorMessage()
                }
            }
        }

        tvRateApp.setOnClickListener {
            when {
                isLoggedIn -> {
                    onRateAppSelected()
                }
                InstantApps.isInstantApp(this) -> {
                    showInstallPrompt()
                }
                else -> {
                    showLogInErrorMessage()
                }
            }
        }
    }

    private fun showLogInErrorMessage() {
        MessageHelper.showErrorMessageSnackbar(window.decorView,
                getString(string.menu_login_error_message),
                windowInsetsViewModel.windowInsetTop.value)
    }

    private fun onCoffeeBarsSelected() {
        closeDrawer()
        selectCoffeeBarsMenuItem()
        navController.navigate(R.id.action_global_shopsListFragment)
    }

    fun selectCoffeeBarsMenuItem() {
        selectMenuItem(tvCoffeeBars, ivCoffeeBars)
    }

    private fun onRateAppSelected() {
        closeDrawer()
        selectRateAppMenuItem()
        navController.navigate(R.id.action_global_rate_app_graph)
    }

    fun selectRateAppMenuItem() {
        selectMenuItem(tvRateApp, ivRateApp)
    }

    private fun onReferFriendsSelected() {
        closeDrawer()

        selectReferFriendsMenuItem()
        navController.navigate(R.id.action_global_referFriendsFragment)
    }

    fun selectReferFriendsMenuItem() {
        selectMenuItem(tvReferFriends, ivReferFriends)
    }

    private fun selectMenuItem(menuItem: TextView, indicator: ImageView) {
        if (menuItem.isSelected) {
            return
        }

        deselectMenuItems()
        SystemUIHelper.resetSystemUiColor(window)

        indicator.isVisible = true
        menuItem.isSelected = true

        updateMenuTextColors()
    }

    private fun deselectMenuItems() {
        tvCoffeeBars.isSelected = false
        tvRateApp.isSelected = false
        tvReferFriends.isSelected = false

        updateMenuTextColors()

        ivCoffeeBars.isInvisible = true
        ivRateApp.isInvisible = true
        ivReferFriends.isInvisible = true
    }

    private fun initNotLoggedIn() {
        setProfileImage("")

        setNavViewClickListeners(false)

        tvTerms.isVisible = true
        tvPrivacy.isVisible = true
        tvGoToLogin.isVisible = true
        profileBtn.isVisible = false

        tvReferFriends.setTextColor(ContextCompat.getColor(this, color.gray500))
        tvRateApp.setTextColor(ContextCompat.getColor(this, color.gray500))

        tvTerms.setOnClickListener {
            closeDrawer()
            CustomTabHelper.openCustomTab(this, getString(string.url_terms_and_conditions))
        }

        tvPrivacy.setOnClickListener {
            closeDrawer()
            CustomTabHelper.openCustomTab(this, getString(string.url_privacy_policy))
        }

        tvGoToLogin.setOnClickListener {
            if(InstantApps.isInstantApp(this)) {
                showInstallPrompt()
            } else {
                startActivity(Actions.getOnboardingIntent(this, hasCreditCard = false))
                closeDrawer()
            }
        }

        cardNoSubscription.setOnClickListener {
            onCardNoSubscriptionClicked()
        }
    }

    private fun onCardNoSubscriptionClicked() {
        closeDrawer()
        when {
            InstantApps.isInstantApp(this) -> {
                showInstallPrompt()
            }
            else -> {
                val user = homeViewModel.getUserLiveData.value?.data
                when(SegmentHelper.getSegment(user)) {
                    VISITING -> {
                        startActivity(
                            Actions.getOnboardingIntent(this,
                                goToSignup = true,
                                hasCreditCard = false
                            )
                        )
                    }
                    TRIALING -> {
                        navController.navigate(R.id.action_global_addCreditCard)
                    }
                    else -> {
                        navController.navigate(R.id.action_global_referFriendsFragment)
                    }
                }
            }
        }
    }

    private fun observeUser(view: View) {
        homeViewModel.getUserLiveData.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    val user = it.data
                    if (user != null) {
                        handleUserResult(user)

                        if(intent.getBooleanExtra(ARG_REQUEST_PERMISSIONS, false)) {
                            checkPushToken(user)
                        }
                    }
                }
                Status.ERROR -> {
                    MessageHelper.showErrorMessageSnackbar(view,
                        topMargin = windowInsetsViewModel.windowInsetTop.value)
                }
                Status.LOADING -> {
                }
            }
        })
    }

    private fun handleUserResult(user: User) {
        setLoggedInUserInfoState(true)

        val userName = user.firstName + " " + user.lastName
        tvUserName.text = userName

        val segment = SegmentHelper.getSegment(user)
        tvUserSubscription.text = when(segment) {
            TRIALING, TRIALING_SUBSCRIBED -> {
                getString(string.menu_trialing)
            }
            CHURNED -> {
                getString(string.menu_churned)
            }
            else -> {
                user.subscriptionPlan.orEmpty()
            }
        }

        cardNoSubscription.isVisible = segment != SUBSCRIBED && segment != TRIALING_SUBSCRIBED
        tvCardTitle.text = when(segment) {
            VISITING -> {
                getString(string.menu_card_title_visiting)
            }
            TRIALING -> {
                getString(string.menu_card_title_trialing)
            }
            else -> {
                getString(string.menu_card_title_churned)
            }
        }
        tvCardSubTitle.text = when(segment) {
            VISITING -> {
                getString(string.menu_card_subtitle_visiting)
            }
            TRIALING -> {
                getString(string.menu_card_subtitle_trialing)
            }
            else -> {
                getString(string.menu_card_subtitle_churned)
            }
        }

        val splitUserName = userName.trim().split(" ")
        val initials = if (splitUserName.size > 1) {
            splitUserName.first().first() + "" + splitUserName.last().first()
        } else {
            splitUserName.first().first().toString()
        }

        setProfileImage(initials)
    }

    private fun checkPushToken(user: User) {
        if(!SharedPreferencesHelper.loadHasEnrolledNotifications()) {
            if (!user.pushToken.isNullOrBlank()){
                //Make sure previous users who agreed to push notifications have their new push token updated
                SharedPreferencesHelper.saveHasEnrolledNotifications(true)
                checkPushNotificationsRegistry()
            }
        }
    }

    private fun setProfileImage(initials: String) {
        val drawable = TextDrawable.builder()
            .beginConfig()
            .textColor(ContextCompat.getColor(this, color.gray100))
            .fontSize(16.px)
            .bold()
            .toUpperCase()
            .endConfig()
            .buildRound(initials, ContextCompat.getColor(this, color.gray600))

        ivProfile.isVisible = true
        ivProfile.setImageDrawable(drawable)
    }

    private fun setLoggedInUserInfoState(isLoggedIn: Boolean) {
        tvUserNamePlaceholder.isVisible = !isLoggedIn
        tvUserSubscriptionPlaceholder.isVisible = !isLoggedIn
        tvUserName.isVisible = isLoggedIn
        tvUserSubscription.isVisible = isLoggedIn
    }


    private fun requestAppPermissions() {
        requestLocationPermission()
    }


    private fun enableNotifications() {
        pushViewModel.pushTokenPutLiveData.observe(this, Observer {
            when(it.status){
                Status.SUCCESS -> {
                    FirebaseMessaging.getInstance().isAutoInitEnabled = true
                    SharedPreferencesHelper.saveHasSentPushToken(true)
                }
                Status.ERROR -> {
                    FirebaseMessaging.getInstance().isAutoInitEnabled = false
                    SharedPreferencesHelper.saveHasSentPushToken(false)
                }
                Status.LOADING -> {}
            }
        })

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Timber.w(task.exception, "getInstanceId failed")
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token
                if (token != null) {
                    pushViewModel.putPushToken(token)
                }
            })
    }

    private fun requestLocationPermission() {
        if (!EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            showCoffeeClubDialog(this)
                .setTitle(string.shopsoverview_location_permission_title)
                .setMessage(string.shopsoverview_location_permission_message)
                .positiveButton(string.shopsoverview_location_permission_accept) {
                    EasyPermissions.requestPermissions(
                        this,
                        getString(string.shopsoverview_location_permission_message),
                        RC_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                }
                .negativeButton(string.shopsoverview_location_permission_skip)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}
