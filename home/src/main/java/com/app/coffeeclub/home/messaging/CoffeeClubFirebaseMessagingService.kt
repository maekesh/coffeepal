package com.app.coffeeclub.home.messaging

import com.app.coffeeclub.utilities.helpers.SharedPreferencesHelper
import com.google.firebase.messaging.FirebaseMessagingService
import timber.log.Timber


class CoffeeClubFirebaseMessagingService : FirebaseMessagingService() {

    override fun onNewToken(newToken: String?) {
        super.onNewToken(newToken)
        Timber.d("Push token refreshed: $newToken")
        //Invalidate token so that it will be refreshed on next startup
        SharedPreferencesHelper.saveHasSentPushToken(false)
    }
}