package com.app.coffeeclub.home.profile.creditcard

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.coffeeclub.home.R
import com.app.coffeeclub.home.profile.BaseEditProfileFragment
import com.app.coffeeclub.uicomponents.forms.FormatType
import com.app.coffeeclub.uicomponents.forms.adapters.FormAdapter
import com.app.coffeeclub.uicomponents.forms.items.FormItem
import kotlinx.android.synthetic.main.fragment_edit_credit_card.*
import com.app.coffeeclub.resources.R as res

class EditCreditCardFragment(override val layoutResId: Int = R.layout.fragment_edit_credit_card) : BaseEditProfileFragment() {

    private var creditCardAdapter: FormAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle(getString(res.string.profile_edit_credit_card_title))
        showBackAsWhiteArrow()

        tvCardCarrier.text = profileViewModel.cardBrandLiveData.value

        initAdapter()
        populateAdapter()

        tvBtnAddNewCreditCard.setOnClickListener {
            findNavController().navigate(EditCreditCardFragmentDirections.actionEditCreditCardFragmentToAddCreditCardFragment())
        }
    }

    private fun initAdapter() {
        if(creditCardAdapter == null) {
            creditCardAdapter = FormAdapter()
        }

        rvEditCreditCard.apply {
            adapter = creditCardAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun populateAdapter() {
        val formContentList = mutableListOf<FormItem>()
        formContentList.add(
            FormItem(
                valueLiveData = profileViewModel.cardNumberLiveData,
                label = getString(com.app.coffeeclub.resources.R.string.profile_edit_credit_card_label_credit_card),
                type = FormatType.CardNumber,
                isReadOnly = true
            )
        )
        formContentList.add(
            FormItem(
                valueLiveData = profileViewModel.expirationDateLiveData,
                label = getString(com.app.coffeeclub.resources.R.string.profile_edit_credit_card_label_expiration_date),
                type = FormatType.ExpiryDate,
                isReadOnly = true
            )
        )
        formContentList.add(
            FormItem(
                valueLiveData = profileViewModel.cvvCodeLiveData,
                label = getString(com.app.coffeeclub.resources.R.string.profile_edit_credit_card_label_cvv),
                isReadOnly = true
            )
        )
        creditCardAdapter?.submitList(formContentList)
    }

}