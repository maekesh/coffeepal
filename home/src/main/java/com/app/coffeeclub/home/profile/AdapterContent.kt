package com.app.coffeeclub.home.profile

import android.content.Context
import android.text.InputType
import androidx.core.content.ContextCompat
import com.app.coffeeclub.resources.R
import com.app.coffeeclub.uicomponents.forms.items.ActionFormItem
import com.app.coffeeclub.uicomponents.forms.items.FormItem

object AdapterContent {

    fun getPersonalInformationContent(
            context: Context,
            profileViewModel: ProfileViewModel,
            onBirthDateClicked: (position: Int) -> Unit,
            onGenderClicked: () -> Unit
    ): MutableList<FormItem> {
        val formContentList = mutableListOf<FormItem>()

        formContentList.add(
                FormItem(
                        valueLiveData = profileViewModel.firstNameLiveData,
                        label = context.getString(R.string.profile_overview_personal_label_1),
                        hint = context.getString(R.string.profile_overview_personal_placeholder_1),
                        inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS,
                        isReadOnly = false
                )
        )
        formContentList.add(
                FormItem(
                        valueLiveData = profileViewModel.lastNameLiveData,
                        label = context.getString(R.string.profile_overview_personal_label_2),
                        hint = context.getString(R.string.profile_overview_personal_placeholder_2),
                        inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS,
                        isReadOnly = false
                )
        )
        formContentList.add(
                FormItem(
                        valueLiveData = profileViewModel.dateOfBirthLiveData,
                        label = context.getString(R.string.profile_overview_personal_label_3),
                        hint = context.getString(R.string.profile_overview_personal_placeholder_3),
                        inputType = InputType.TYPE_DATETIME_VARIATION_DATE,
                        clickCallback = { position ->
                            onBirthDateClicked(position)
                        }
                )
        )
        formContentList.add(
                FormItem(
                        valueLiveData = profileViewModel.sexLiveData,
                        label = context.getString(R.string.profile_overview_personal_label_4),
                        hint = context.getString(R.string.profile_overview_personal_placeholder_4),
                        inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS,
                        clickCallback = {
                            onGenderClicked()
                        }
                )
        )
        return formContentList
    }


    fun getLoginDetailContent(
            context: Context,
            profileViewModel: ProfileViewModel,
            onEmailClicked: () -> Unit,
            onPasswordClicked: () -> Unit,
            onLogOutClicked: () -> Unit
    ): MutableList<ActionFormItem> {
        val formContentList = mutableListOf<ActionFormItem>()

        formContentList.add(
                ActionFormItem(
                        valueLiveData = profileViewModel.emailLiveData,
                        label = context.getString(R.string.profile_email_placeholder_1),
                        clickCallback = {
                            onEmailClicked()
                        }
                )
        )
        formContentList.add(
                ActionFormItem(
                        valueLiveData = profileViewModel.passwordLiveData,
                        label = context.getString(R.string.profile_email_placeholder_2),
                        clickCallback = {
                            onPasswordClicked()
                        }
                )
        )
        formContentList.add(
                ActionFormItem(
                        label = context.getString(R.string.profile_overview_sign_out_title),
                        isBtn = true,
                        btnTextColor = ContextCompat.getColor(context, R.color.red),
                        clickCallback = {
                            onLogOutClicked()
                        }
                )
        )
        return formContentList
    }

    fun getMembershipContent(
            context: Context,
            profileViewModel: ProfileViewModel,
            onSubscriptionClicked: () -> Unit,
            onCreditCardClicked: () -> Unit
    ): MutableList<ActionFormItem> {
        val formContentList = mutableListOf<ActionFormItem>()

        formContentList.add(
                ActionFormItem(
                        valueLiveData = profileViewModel.subscriptionLiveData,
                        label = context.getString(R.string.profile_overview_sub_label_1),
                        hint = context.getString(R.string.profile_overview_placeholder_subscription),
                        clickCallback = {
                            onSubscriptionClicked()
                        }
                )
        )
        formContentList.add(
                ActionFormItem(
                        valueLiveData = profileViewModel.cardNumberLiveData,
                        label = context.getString(R.string.profile_overview_sub_label_2),
                        hint = context.getString(R.string.profile_overview_placeholder_credit_card),
                        clickCallback = {
                            onCreditCardClicked()
                        }
                )
        )
        return formContentList
    }

    fun getMiscContent(
            context: Context,
            onTermsAndConditionsClicked: () -> Unit,
            onPrivacyPolicyClicked: () -> Unit,
            onDeleteProfileClicked: () -> Unit
    ): MutableList<ActionFormItem> {
        val formContentList = mutableListOf<ActionFormItem>()

        formContentList.add(
                ActionFormItem(
                        label = context.getString(R.string.profile_overview_version_link_2),
                        clickCallback = {
                            onTermsAndConditionsClicked()
                        }
                )
        )
        formContentList.add(
                ActionFormItem(
                        label = context.getString(R.string.profile_overview_version_link_3),
                        clickCallback = {
                            onPrivacyPolicyClicked()
                        }
                )
        )
        formContentList.add(
                ActionFormItem(
                        label = context.getString(R.string.profile_overview_delete_profile),
                        isBtn = true,
                        btnTextColor = ContextCompat.getColor(context, R.color.red),
                        clickCallback = {
                            onDeleteProfileClicked()
                        }
                )
        )
        return formContentList
    }

    fun getAddressInformationContent(context: Context, profileViewModel: ProfileViewModel): MutableList<FormItem> {
        val formContentList = mutableListOf<FormItem>()
        formContentList.add(
                FormItem(
                        valueLiveData = profileViewModel.countryLiveData,
                        label = context.getString(R.string.sign_up_3_label_1),
                        hint = context.getString(R.string.sign_up_3_placeholder_1),
                        inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS,
                        isReadOnly = false
                )
        )

        formContentList.add(
                FormItem(
                        valueLiveData = profileViewModel.cityLiveData,
                        label = context.getString(R.string.sign_up_3_label_2),
                        hint = context.getString(R.string.sign_up_3_placeholder_2),
                        inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS,
                        isReadOnly = false
                )
        )

        formContentList.add(
                FormItem(
                        valueLiveData = profileViewModel.postalCodeLiveData,
                        label = context.getString(R.string.profile_overview_address_label_3),
                        hint = context.getString(R.string.profile_overview_address_placeholder_3),
                        inputType = InputType.TYPE_CLASS_NUMBER,
                        maxLength = context.resources.getInteger(R.integer.postal_code_length),
                        isReadOnly = false
                )
        )

        formContentList.add(FormItem(
                valueLiveData = profileViewModel.streetLiveData,
                label = context.getString(R.string.sign_up_3_label_4),
                hint = context.getString(R.string.sign_up_3_placeholder_4),
                inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS,
                isReadOnly = false
        ))


        return formContentList
    }
}