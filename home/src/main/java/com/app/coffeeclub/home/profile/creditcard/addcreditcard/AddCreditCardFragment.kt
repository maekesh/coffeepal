package com.app.coffeeclub.home.profile.creditcard.addcreditcard

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.autofill.AutofillManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.coffeeclub.BuildConfig
import com.app.coffeeclub.home.HomeViewModel
import com.app.coffeeclub.home.R
import com.app.coffeeclub.resources.R as res
import com.app.coffeeclub.home.profile.BaseEditProfileFragment
import com.app.coffeeclub.home.profile.creditcard.CreditCardViewModel
import com.app.coffeeclub.uicomponents.forms.FormatType
import com.app.coffeeclub.uicomponents.forms.adapters.InputFormAdapter
import com.app.coffeeclub.uicomponents.forms.items.InputFormItem
import com.app.coffeeclub.utilities.extensions.validate
import com.app.coffeeclub.utilities.helpers.AutofillHelper
import com.app.coffeeclub.utilities.helpers.CreditCardHelper
import com.app.coffeeclub.utilities.helpers.MessageHelper
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.stripe.android.model.Token
import dk.idealdev.utilities.response.Status
import kotlinx.android.synthetic.main.fragment_add_credit_card.*
import kotlinx.android.synthetic.main.fragment_edit_credit_card.rvEditCreditCard
import java.lang.Exception

class AddCreditCardFragment(override val layoutResId: Int = R.layout.fragment_add_credit_card) : BaseEditProfileFragment() {

    private val creditCardViewModel: CreditCardViewModel by viewModelProvider()
    private val homeViewModel: HomeViewModel by viewModelProvider()

    private var creditCardAdapter: InputFormAdapter? = null

    var autofillManager: AutofillManager? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle(getString(res.string.profile_add_credit_card_title))
        showBackAsWhiteArrow()

        initAutofillManager()
        observeInputData()

        initAdapter()
        populateAdapter()

        tvBtnAddNewCreditCard.setOnButtonClickListener {
            onAddNewCardClicked(view)
        }
    }

    private fun observeInputData() {
        creditCardViewModel.newCardNumberLiveData.observe(viewLifecycleOwner, Observer {
            updateSaveButtonEnabledState()
        })

        creditCardViewModel.newExpirationDateLiveData.observe(viewLifecycleOwner, Observer {
            updateSaveButtonEnabledState()
        })

        creditCardViewModel.newCvvCodeLiveData.observe(viewLifecycleOwner, Observer {
            updateSaveButtonEnabledState()
        })
    }

    private fun updateSaveButtonEnabledState() {
        val cardNumber = creditCardViewModel.newCardNumberLiveData.value
        val expirationDateString = creditCardViewModel.newExpirationDateLiveData.value
        val cvv = creditCardViewModel.newCvvCodeLiveData.value

        var creditCard : Card? = null
        if(cardNumber != null && expirationDateString != null && cvv != null) {
            creditCard = CreditCardHelper.getCreditCard(cardNumber, expirationDateString, cvv)
        }

        tvBtnAddNewCreditCard.setButtonEnabled(creditCard != null && creditCard.validateCard())
    }

    private fun onAddNewCardClicked(view: View) {
        val card = getStripeCard(view) ?: return
        if(isCardValid(card, view)) {
            tvBtnAddNewCreditCard.setProgressState(true)
            fetchAndUploadStripeToken(card, view)
        }
    }

    private fun initAutofillManager() {
        @RequiresApi(Build.VERSION_CODES.O)
        autofillManager = ContextCompat.getSystemService(requireContext(), AutofillManager::class.java)
    }

    private fun initAdapter() {
        if(creditCardAdapter == null) {
            creditCardAdapter = InputFormAdapter()
        }

        rvEditCreditCard.apply {
            adapter = creditCardAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    @SuppressLint("InlinedApi")
    private fun populateAdapter() {
        val formContentList = mutableListOf<InputFormItem>()
        formContentList.add(
            InputFormItem(
                valueLiveData = creditCardViewModel.newCardNumberLiveData,
                validation = ::validateNotEmpty,
                label = getString(com.app.coffeeclub.resources.R.string.profile_add_credit_card_label_credit_card),
                hint = getString(com.app.coffeeclub.resources.R.string.profile_add_credit_card_placeholder_credit_card),
                inputType = InputType.TYPE_CLASS_NUMBER,
                autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                autoFillHint = View.AUTOFILL_HINT_CREDIT_CARD_NUMBER,
                type = FormatType.CardNumber,
                maxLength = resources.getInteger(com.app.coffeeclub.resources.R.integer.credit_card_length)
            )
        )
        formContentList.add(
            InputFormItem(
                valueLiveData = creditCardViewModel.newExpirationDateLiveData,
                validation = ::validateNotEmpty,
                label = getString(com.app.coffeeclub.resources.R.string.profile_add_credit_card_label_expiration_date),
                hint = getString(com.app.coffeeclub.resources.R.string.profile_add_credit_card_placeholder_expiration_date),
                inputType = InputType.TYPE_CLASS_NUMBER,
                autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                autoFillHint = View.AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_DATE,
                type = FormatType.ExpiryDate,
                maxLength = resources.getInteger(com.app.coffeeclub.resources.R.integer.expiration_date_length)
            )
        )
        formContentList.add(
            InputFormItem(
                valueLiveData = creditCardViewModel.newCvvCodeLiveData,
                validation = ::validateNotEmpty,
                label = getString(com.app.coffeeclub.resources.R.string.profile_add_credit_card_label_cvv),
                hint = getString(com.app.coffeeclub.resources.R.string.profile_add_credit_card_placeholder_cvv),
                inputType = InputType.TYPE_CLASS_NUMBER,
                autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                autoFillHint = View.AUTOFILL_HINT_CREDIT_CARD_SECURITY_CODE,
                maxLength = resources.getInteger(com.app.coffeeclub.resources.R.integer.cvv_length)
            )
        )
        creditCardAdapter?.submitList(formContentList)
    }

    private fun validateNotEmpty(etValue: EditText, tvLabel: TextView, ivStatus: ImageView, hasFocus: Boolean, isNotFinalCheck: Boolean): Boolean {
        return etValue.validate({ text -> text.isNotBlank() },
            getString(com.app.coffeeclub.resources.R.string.signup_info_missing_fields),
            tvLabel = tvLabel,
            tvError = tvError,
            ivStatus = ivStatus,
            hasFocus = hasFocus,
            isNotFinalCheck = isNotFinalCheck)
    }

    private fun getStripeCard(view: View): Card? {
        val cardNumber = creditCardViewModel.newCardNumberLiveData.value
        val expirationDateString = creditCardViewModel.newExpirationDateLiveData.value
        val cvv = creditCardViewModel.newCvvCodeLiveData.value

        if(cardNumber.isNullOrBlank() || expirationDateString.isNullOrBlank() || cvv.isNullOrBlank() || expirationDateString.length < 4) {
            MessageHelper.showErrorMessageSnackbar(view,
                getString(res.string.profile_add_credit_error_invalid_card_info),
                windowInsetsViewModel.windowInsetTop.value)
            return null
        }

        return CreditCardHelper.getCreditCard(cardNumber, expirationDateString, cvv)
    }

    private fun isCardValid(card: Card, view: View): Boolean {
        val isCardValid = card.validateCard()

        if(!isCardValid){
            MessageHelper.showErrorMessageSnackbar(view,
                getString(res.string.profile_add_credit_error_invalid_card_info),
                windowInsetsViewModel.windowInsetTop.value)
        } else{
            creditCardViewModel.newCardInfo = card
        }

        return isCardValid
    }

    private fun fetchAndUploadStripeToken(cardInfo: Card, view: View) {
        val stripe = Stripe(
            requireContext(),
            BuildConfig.STRIPE_API_KEY
        )
        stripe.createToken(
            cardInfo, object : TokenCallback {
                override fun onSuccess(result: Token) {
                    uploadPaymentToken(result, view)
                }

                override fun onError(e: Exception) {
                    tvBtnAddNewCreditCard.setProgressState(false)
                    MessageHelper.showErrorMessageSnackbar(view,
                        getString(res.string.general_error_generic),
                        windowInsetsViewModel.windowInsetTop.value)
                }
            }
        )
    }

    private fun uploadPaymentToken(result: Token, view: View) {
        creditCardViewModel.uploadPaymentTokenLiveData.observe(viewLifecycleOwner, Observer { data ->
            tvBtnAddNewCreditCard.setProgressState(data.status == Status.LOADING)

            when (data.status) {
                Status.SUCCESS -> {
                    val card = creditCardViewModel.newCardInfo ?: return@Observer

                    homeViewModel.getUserLiveData.value?.data?.last4 = card.last4
                    homeViewModel.getUserLiveData.value?.data?.cardBrand = card.brand
                    homeViewModel.getUserLiveData.value?.data?.cardExpMonth = card.expMonth?.toBigDecimal()
                    homeViewModel.getUserLiveData.value?.data?.cardExpYear = card.expYear?.toBigDecimal()?.plus(2000.toBigDecimal())

                    MessageHelper.showSuccessMessageSnackbar(view,
                        getString(res.string.profile_add_credit_card_success),
                        windowInsetsViewModel.windowInsetTop.value)

                    clearData()
                    findNavController().popBackStack(R.id.profileFragment, false)
                }
                Status.ERROR -> MessageHelper.showErrorMessageSnackbar(view,
                    getString(res.string.general_error_generic),
                    windowInsetsViewModel.windowInsetTop.value)
                Status.LOADING -> {

                }
            }
        })

        creditCardViewModel.uploadPaymentToken(result.id)
    }

    private fun clearData() {
        creditCardViewModel.newCardInfo = null
        creditCardViewModel.newCardNumberLiveData.value = null
        creditCardViewModel.newCvvCodeLiveData.value = null
    }

}