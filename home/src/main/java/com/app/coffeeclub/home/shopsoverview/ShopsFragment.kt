package com.app.coffeeclub.home.shopsoverview

import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.Navigator
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.app.coffeeclub.actions.Actions
import com.app.coffeeclub.home.*
import com.app.coffeeclub.resources.R as res
import com.app.coffeeclub.home.shopsoverview.map.CustomClusterRenderer
import com.app.coffeeclub.home.shopsoverview.map.ShopClusterItem
import com.app.coffeeclub.home.shopsoverview.map.ShopsMapAdapter
import com.app.coffeeclub.home.shopsoverview.paging.PagingState
import com.app.coffeeclub.home.shopsoverview.paging.ShopsPagedListAdapter
import com.app.coffeeclub.models.Shop
import com.app.coffeeclub.models.ShopsResponse
import com.app.coffeeclub.models.custom.ShopAdCard
import com.app.coffeeclub.resources.R.integer
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.utilities.classes.DebouncingQueryTextListener
import com.app.coffeeclub.utilities.extensions.latLng
import com.app.coffeeclub.utilities.helpers.*
import com.google.android.gms.common.wrappers.InstantApps
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.maps.android.clustering.ClusterManager
import dk.idealdev.ui.recyclerview.decorations.HorizontalSpacingItemDecoration
import dk.idealdev.ui.recyclerview.decorations.VerticalSpacingItemDecoration
import dk.idealdev.utilities.dp
import dk.idealdev.utilities.livedata.observeOnce
import dk.idealdev.utilities.response.Resource
import dk.idealdev.utilities.response.Status
import kotlinx.android.synthetic.main.appbar_maps.*
import kotlinx.android.synthetic.main.appbar_shops.*
import kotlinx.android.synthetic.main.content_map.*
import kotlinx.android.synthetic.main.content_shops_list.*
import pub.devrel.easypermissions.EasyPermissions
import timber.log.Timber
import com.google.android.gms.instantapps.InstantApps as Instant

class ShopsFragment(override val layoutResId: Int = R.layout.fragment_shops) : BaseHomeFragment(), OnMapReadyCallback,
        ShopItemCallback {

    private val homeViewModel: HomeViewModel by viewModelProvider()

    private val locationViewModel: LocationViewModel by viewModelProvider()
    private var latestUserLocation: Location? = null
    private var latestSearchQuery: String = ""
    private var latestShopWithinRange: Shop? = null

    private var shopsVerticalListAdapter: ShopsPagedListAdapter? = null
    private var shopsMapAdapter: ShopsMapAdapter? = null
    private var isSearchActive = false

    //Map members
    private var isMapVisible = false
    private val defaultZoom = 15f
    private var selectedClusterItem : ShopClusterItem? = null
    private lateinit var clusterRenderer : CustomClusterRenderer
    private lateinit var clusterManager: ClusterManager<ShopClusterItem>
    private lateinit var googleMap: GoogleMap
    private lateinit var snapHelper : PagerSnapHelper
    private lateinit var mapListLayoutManager : LinearLayoutManager

    companion object {
        private const val RC_INSTALL_APP = 7
    }

    override fun onResume() {
        super.onResume()
        shopsMapAdapter?.notifyDataSetChanged()

        if(::googleMap.isInitialized) {
            googleMap.setOnMapLoadedCallback {
                rvMapShops.post {
                    showVisibleMarkersInList()
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        appBarMaps.bringToFront()

        toolbarTitleShops.text = getString(string.app_name)
        toolbarTitleMaps.text = getString(string.app_name)

        latestUserLocation = SharedPreferencesHelper.loadUserLocation()

        observeUserData()

        val appCompatActivity = requireActivity() as AppCompatActivity

        setupClickListeners(appCompatActivity)

        if (isMapVisible) {
            containerShopsMap.isVisible = true
            onMapSelected(appCompatActivity)
        } else {
            onVerticalListSelected(appCompatActivity)
        }

        //In case we resume from back button we need to update menu item state
        if (appCompatActivity is HomeActivity) {
            appCompatActivity.selectCoffeeBarsMenuItem()
        }

        initMap()

        initVerticalList()
        initMapList()

        observeUserLocation()
        observeShops()

        setupSearch()
    }

    override fun onTopInsetChanged(inset: Int) {
        super.onTopInsetChanged(inset)
        InsetHelper.addTopPadding(toolbarMaps, inset)
        InsetHelper.addTopPadding(toolbarShops, inset)
    }

    override fun onBottomInsetChanged(inset: Int) {
        super.onBottomInsetChanged(inset)
        InsetHelper.addBottomPadding(rvShops, inset)
        InsetHelper.addBottomMargin(rvMapsShopsContainer, inset)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                val requireActivity = requireActivity()
                if (requireActivity is HomeActivity) {
                    requireActivity.openDrawer()
                    return true
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onShopItemClicked(
            item: Shop,
            adapterPosition: Int,
            imageView: ImageView,
            gradient: View?,
            solid: View,
            shopName: TextView,
            openClosedStatus: TextView,
            divider: View,
            openClosedTime: TextView,
            distance: TextView,
            isFromMap: Boolean
    ) {
        val extras: Navigator.Extras

        if (isFromMap) {
            extras = FragmentNavigatorExtras(
                    imageView to imageView.transitionName,
                    solid to solid.transitionName,
                    shopName to shopName.transitionName,
                    openClosedStatus to openClosedStatus.transitionName,
                    divider to divider.transitionName,
                    openClosedTime to openClosedTime.transitionName,
                    distance to distance.transitionName
            )
        } else {
            extras = FragmentNavigatorExtras(
                    imageView to imageView.transitionName,
                    gradient!! to gradient.transitionName,
                    solid to solid.transitionName,
                    shopName to shopName.transitionName,
                    openClosedStatus to openClosedStatus.transitionName,
                    divider to divider.transitionName,
                    openClosedTime to openClosedTime.transitionName,
                    distance to distance.transitionName
            )
        }


        findNavController().navigate(
                ShopsFragmentDirections.actionShopsListFragmentToClaimFragment(
                    item,
                    isFromMap,
                    !isFromMap,
                    adapterPosition),
            extras)
    }

    override fun onSubscriptionAdClicked() {
        val user = homeViewModel.getUserLiveData.value?.data
        val segment = SegmentHelper.getSegment(user)

        when {
            InstantApps.isInstantApp(requireContext()) -> {
                Instant.showInstallPrompt(requireActivity(), null, RC_INSTALL_APP, null)
            }
            else -> {
                when(segment) {
                    SegmentHelper.Segment.VISITING -> {
                        startActivity(
                            Actions.getOnboardingIntent(requireContext(),
                                goToSignup = true,
                                hasCreditCard = false
                            )
                        )
                    }
                    SegmentHelper.Segment.TRIALING -> {
                        findNavController().navigate(R.id.action_global_addCreditCard)
                    }
                    else -> {
                        findNavController().navigate(R.id.action_global_referFriendsFragment)
                    }
                }
            }
        }

    }

    private fun setupClickListeners(appCompatActivity: AppCompatActivity) {
        tvBtnMap.setOnClickListener {
            onVerticalListSelected(appCompatActivity)
            CircularRevealHelper.showVerticalList(requireActivity().window, windowInsetsViewModel.windowInsetTop.value, containerShopsMap, tvBtnMap, tvBtnShops)
        }

        tvBtnShops.setOnClickListener {
            onMapSelected(appCompatActivity)
            CircularRevealHelper.showMap(requireActivity().window, windowInsetsViewModel.windowInsetTop.value, containerShopsMap, tvBtnMap, tvBtnShops)
        }

        appCompatActivity.onBackPressedDispatcher.addCallback(this) {
            if (isMapVisible) {
                tvBtnMap.performClick()
            } else {
                requireActivity().finish()
            }
        }
    }

    private fun observeUserData() {
        homeViewModel.getUserLiveData.observe(viewLifecycleOwner, Observer {
            val user = it.data
            if (user != null) {
                val segment = SegmentHelper.getSegment(user)
                val showSubscriptionAd = segment != SegmentHelper.Segment.SUBSCRIBED
                    && segment != SegmentHelper.Segment.TRIALING_SUBSCRIBED
                SharedPreferencesHelper.saveShowCardSubscriptionAd(showSubscriptionAd)
            }
        })
    }

    private fun setupSearch() {
        searchView.setOnQueryTextListener(DebouncingQueryTextListener(lifecycle) { queryFilter ->
            queryFilter ?: return@DebouncingQueryTextListener

            if (queryFilter != latestSearchQuery && queryFilter.isEmpty() || queryFilter.length > 2) {
                homeViewModel.loadShopsWithSearchParameters(queryFilter)
            }

            isSearchActive = shouldShowSearchResultCount(queryFilter)
            latestSearchQuery = queryFilter
        })
    }

    private fun shouldShowSearchResultCount(queryFilter : String) : Boolean {
        return queryFilter.isNotEmpty() && queryFilter.length > 2
    }

    private fun onMapSelected(appCompatActivity: AppCompatActivity) {
        isMapVisible = true
        SystemUIHelper.setSystemUiColorLight(requireActivity().window)
        appCompatActivity.setSupportActionBar(toolbarMaps)
        appCompatActivity.supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(com.app.coffeeclub.resources.R.drawable.ic_burger_black)
        }
        showVisibleMarkersInList()
    }

    private fun onVerticalListSelected(appCompatActivity: AppCompatActivity) {
        isMapVisible = false
        SystemUIHelper.resetSystemUiColor(requireActivity().window)
        appCompatActivity.setSupportActionBar(toolbarShops)
        appCompatActivity.supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(com.app.coffeeclub.resources.R.drawable.ic_burger_white)
        }
    }

    private fun observeShops() {
        homeViewModel.shopsList.observe(viewLifecycleOwner, Observer {
            Timber.d("Got paged shops list")
            shopsVerticalListAdapter?.setUserLocation(SharedPreferencesHelper.loadUserLocation())
            shopsVerticalListAdapter?.submitList(it)
        })

        homeViewModel.getPagingState().observe(viewLifecycleOwner, Observer { state ->
            when (state) {
                PagingState.SUCCESS -> {
                    LoaderHelper.hideIsLoading(swipeRefresh = swipeRefresh)
                    progressBarShopsList.isVisible = false
                }
                PagingState.LOADING_INITIAL -> {
                    if (!swipeRefresh.isRefreshing) {
                        progressBarShopsList.isVisible = true
                    }
                }
                PagingState.ERROR -> {
                    LoaderHelper.hideIsLoading(null, swipeRefresh)
                    progressBarShopsList.isVisible = false
                    isSearchActive = false
                    view?.let {
                        MessageHelper.showErrorMessageSnackbar(it,
                        topMargin = windowInsetsViewModel.windowInsetTop.value)
                    }

                }
                else -> {
                }
            }

            shopsVerticalListAdapter?.setPagingState(state ?: PagingState.SUCCESS)
        })

        homeViewModel.getTotalCount().observe(viewLifecycleOwner, Observer { totalCount ->
            tvSearchAmount.isVisible = isSearchActive
            var adjustedCount = totalCount
            if(SharedPreferencesHelper.loadShowCardSubscriptionAd()){
                adjustedCount--
            }

            tvSearchAmount.text = if(totalCount == 0) {
                getString(string.shopsoverview_search_amount_none)
            } else {
                String.format(getString(string.shopsoverview_search_amount), adjustedCount)
            }
        })
    }

    //region Vertical Shops list

    private fun initVerticalList() {
        val searchText = searchView.findViewById<TextView>(androidx.appcompat.R.id.search_src_text)
        val typeface = ResourcesCompat.getFont(requireContext(), res.font.open_sans_regular)
        searchText.typeface = typeface

        if (shopsVerticalListAdapter == null) {
            shopsVerticalListAdapter = ShopsPagedListAdapter(this, homeViewModel.getUserLiveData)
        }

        rvShops.layoutManager = LinearLayoutManager(requireContext())
        rvShops.addItemDecoration(VerticalSpacingItemDecoration(16.dp))
        rvShops.apply {
            adapter = shopsVerticalListAdapter
            postponeEnterTransition()
            viewTreeObserver
                    .addOnPreDrawListener {
                        startPostponedEnterTransition()
                        true
                    }
        }

        swipeRefresh.setOnRefreshListener {
            homeViewModel.forceRefreshShopsPaginatedList()
        }
    }

    //endregion

    private fun observeUserLocation() {
        locationViewModel.userLocationLiveData.observe(viewLifecycleOwner, Observer { newLocation ->
            if (latestUserLocation == null) {
                latestUserLocation = newLocation
                isSearchActive = shouldShowSearchResultCount(searchView.query.toString())
                homeViewModel.loadShopsWithSearchParameters(searchView.query.toString())
            } else if (newLocation != latestUserLocation) {

                val distanceDifferenceInMeters = newLocation.distanceTo(latestUserLocation)
                val distanceTrigger = resources.getInteger(integer.shops_map_movement_before_list_update_in_meters)
                //Update list if user location has changed enough since last check
                if (distanceDifferenceInMeters >= distanceTrigger) {
                    Timber.d("User has moved above threshold")
                    shopsVerticalListAdapter?.setUserLocation(newLocation)
                    shopsMapAdapter?.setUserLocation(newLocation)
                    isSearchActive = shouldShowSearchResultCount(searchView.query.toString())
                    homeViewModel.loadShopsWithSearchParameters(searchView.query.toString())
                    latestUserLocation = newLocation
                }
            }
            checkHasEnteredShopRadius(newLocation)
        })
    }

    private fun checkHasEnteredShopRadius(newLocation: Location) {
        val shopArrivalThresholdInMeters = resources.getInteger(integer.shop_arrival_threshold_in_meters)

        val shopsWithinRange = homeViewModel.shopsList.value?.filter {
            LocationHelper.getDistanceToShop(it, newLocation) <= shopArrivalThresholdInMeters
        }

        if (shopsWithinRange != null && shopsWithinRange.size == 1) {
            val shop = shopsWithinRange[0]
            if (shop != latestShopWithinRange) {
                latestShopWithinRange = shop
                findNavController().navigate(ShopsFragmentDirections.actionShopsListFragmentToClaimFragment(shop))
            }
        }
    }

    //region Maps

    private fun initMap() {
        val supportMapFragment = childFragmentManager.findFragmentById(R.id.googleMap) as SupportMapFragment
        supportMapFragment.getMapAsync(this)
    }

    private fun setUpClustering() {
        if(view == null){
            return
        }

        // Position the googleMap (only first location update).
        locationViewModel.userLocationLiveData.observeOnce(viewLifecycleOwner, Observer<Location> { userLocation ->
            //If no longer in default position don't zoom in to users position
            if (googleMap.cameraPosition.target.latitude > 1 && googleMap.cameraPosition.target.longitude > 1) {
                return@Observer
            }

            if (userLocation != null) {
                googleMap.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                                LatLng(userLocation.latitude, userLocation.longitude),
                                defaultZoom
                        )
                )
            }
        })

        if (!::clusterManager.isInitialized) {
            clusterManager = ClusterManager(requireContext(), googleMap)
        }

        clusterRenderer = CustomClusterRenderer(requireContext(), googleMap, clusterManager)
        clusterManager.renderer = clusterRenderer

        googleMap.setOnMarkerClickListener(clusterManager)
        //Show only visible items in list
        googleMap.setOnCameraIdleListener {
            clusterManager.onCameraIdle()
            showVisibleMarkersInList()
        }

        setupClusterItemClickListener()
        // Add cluster items (markers) to the cluster manager.
        addMapItems()
    }

    private fun showVisibleMarkersInList() {
        val visibleRegion = googleMap.projection.visibleRegion.latLngBounds

        val visibleItems =
                clusterManager.algorithm.items.filter { visibleRegion.contains(it.position) }.map { it.shop }

        val visibleItemsMutable = visibleItems.toMutableList()

        if (visibleItemsMutable.isNotEmpty() && SharedPreferencesHelper.loadShowCardSubscriptionAd()) {
            visibleItemsMutable.add(1, ShopAdCard())
        }
        clusterManager.cluster()

        val sortedList = ShopHelper.sortListByDistance(
                locationViewModel.userLocationLiveData.value,
                visibleItemsMutable
        )

        //Make sure we have a selected item on init
        if(selectedClusterItem == null && !sortedList.isNullOrEmpty()){
            //Get snapped item
            val snapView = snapHelper.findSnapView(mapListLayoutManager)
            var position = 0
            if(snapView != null) {
                position = mapListLayoutManager.getPosition(snapView)
            }

            selectedClusterItem = clusterManager.algorithm.items.find { it.position == sortedList[position].latLng }
        }

        val selectedMatch = visibleItemsMutable.find { it.latLng == selectedClusterItem?.position }
        if(selectedMatch != null){
            if(selectedClusterItem != null) {
                val marker = clusterRenderer.getMarker(selectedClusterItem)
                marker?.alpha = 1f
            }
        }

        shopsMapAdapter?.submitList(sortedList)
    }

    private fun setupClusterItemClickListener() {

        clusterManager.setOnClusterItemClickListener { marker ->

            //Scroll to selected item in list
            for (shop in clusterManager.algorithm.items) {
                if (shop.position == marker.position) {
                    val indexOfShop = shopsMapAdapter?.getIndexOfShop(shop.shop)
                    if (indexOfShop != null) {
                        rvMapShops.smoothScrollToPosition(indexOfShop)
                    }
                }
            }
            true
        }
    }

    private fun initMapList() {
        if (shopsMapAdapter == null) {
            shopsMapAdapter = ShopsMapAdapter(this, homeViewModel.getUserLiveData)
        }

        rvMapShops.apply {
            adapter = shopsMapAdapter
            postponeEnterTransition()
            viewTreeObserver
                    .addOnPreDrawListener {
                        startPostponedEnterTransition()
                        true
                    }
        }

        mapListLayoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
        rvMapShops.layoutManager = mapListLayoutManager
        rvMapShops.addItemDecoration(HorizontalSpacingItemDecoration(64.dp))
        snapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(rvMapShops)

        rvMapShops.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    onMapListSelectionChanged(snapHelper, mapListLayoutManager)
                }
            }
        })


        //Make sure navigation bar is light when entering map
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            containerShopsMap.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }
    }

    private fun onMapListSelectionChanged(snapHelper: PagerSnapHelper, layoutManager: LinearLayoutManager) {
        val centerView = snapHelper.findSnapView(layoutManager)
        //Center pin for selected shop
        if (centerView != null) {
            val pos = layoutManager.getPosition(centerView)
            shopsMapAdapter?.let { adapter ->
                val selectedItem = adapter.currentList[pos]
                if (selectedItem != null && selectedItem !is ShopAdCard) {
                    handleSelectedMapListItem(selectedItem)
                }
            }
        }
    }

    private fun handleSelectedMapListItem(selectedItem: Shop) {
        if(selectedClusterItem != null){
            clusterRenderer.getMarker(selectedClusterItem)?.alpha = 0.5f
        }

        //Select marker when not clustered
        for (marker in clusterManager.markerCollection.markers) {
            if (marker.position == selectedItem.latLng){
                val clusterItem = clusterRenderer.getClusterItem(marker)
                if(clusterItem != null){
                    selectedClusterItem = clusterItem
                    clusterRenderer.getMarker(selectedClusterItem)?.alpha = 1f
                }
            }
        }

        //Select marker if it is clustered
        for (clusterMaker in clusterManager.clusterMarkerCollection.markers) {
            val cluster = clusterRenderer.getCluster(clusterMaker)
            if(cluster != null){
                for (clusterItem in cluster.items) {
                    if(clusterItem.position == selectedItem.latLng){
                        selectedClusterItem = clusterItem
                        clusterRenderer.getMarker(selectedClusterItem)?.alpha = 1f
                    }
                }
            }
        }

        googleMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                                selectedItem.latitude.toDouble(),
                                selectedItem.longitude.toDouble()
                        ), getMinimumZoom()
                )
        )
    }

    private fun getMinimumZoom() : Float {
        val currentZoom = googleMap.cameraPosition?.zoom

        return if(currentZoom != null && currentZoom < defaultZoom){
            defaultZoom
        } else{
            currentZoom ?: defaultZoom
        }
    }

    override fun onMapReady(map: GoogleMap?) {
        map ?: return

        googleMap = map
        googleMap.uiSettings.isRotateGesturesEnabled = false
        googleMap.uiSettings.isMyLocationButtonEnabled = false
        googleMap.uiSettings.isMapToolbarEnabled = false

        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.mapstyle))
        enableMyLocation()
        setUpClustering()
    }

    private fun addMapItems() {
        homeViewModel.getAllShopsLiveData.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    handleAllShopsSuccessResponse(it)
                }
                Status.ERROR -> {
                    view?.let { view ->
                        MessageHelper.showErrorMessageSnackbar(
                            view,
                            topMargin = windowInsetsViewModel.windowInsetTop.value)
                    }
                }
                Status.LOADING -> {
                }
            }
        })

        if (homeViewModel.getAllShopsLiveData.value == null) {
            homeViewModel.getAllShops()
        }
    }

    private fun handleAllShopsSuccessResponse(it: Resource<ShopsResponse>) {
        val items = it.data?.items
        if (items == null) {
            view?.let { view ->
                MessageHelper.showErrorMessageSnackbar(
                    view,
                    topMargin = windowInsetsViewModel.windowInsetTop.value)
            }
            return
        }

        if(!clusterManager.algorithm.items.isNullOrEmpty()){
            return
        }
        shopsMapAdapter?.setUserLocation(SharedPreferencesHelper.loadUserLocation())

        for (shop in items) {
            if (shop.latitude != null && shop.longitude != null) {
                val shopClusterItem = ShopClusterItem(shop)
                clusterManager.addItem(shopClusterItem)
            }
        }

        showVisibleMarkersInList()
    }

    @SuppressLint("MissingPermission")
    private fun enableMyLocation() {
        if (EasyPermissions.hasPermissions(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            googleMap.isMyLocationEnabled = true
        }
    }
    //endregion

}
