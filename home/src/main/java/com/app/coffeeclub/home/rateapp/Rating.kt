package com.app.coffeeclub.home.rateapp

enum class Rating(val minValue : Int) {
    Detractor(6),
    Passive(8),
    Promoter(10)
}