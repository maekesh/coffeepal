package com.app.coffeeclub.home.profile

import android.text.Editable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.coffeeclub.models.DeleteUserRequestDto
import com.app.coffeeclub.models.UpdateUserEmailDto
import com.app.coffeeclub.models.User
import com.app.coffeeclub.models.UserDto
import com.app.coffeeclub.network.repository.UsersRepository
import dk.idealdev.utilities.livedata.SingleLiveEvent
import dk.idealdev.utilities.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class ProfileViewModel : ViewModel() {

    val firstNameLiveData: MutableLiveData<String> = MutableLiveData()
    val lastNameLiveData: MutableLiveData<String> = MutableLiveData()
    val dateOfBirthLiveData: MutableLiveData<Date> = MutableLiveData()
    val sexLiveData: MutableLiveData<UserDto.SexEnum> = MutableLiveData()

    val streetLiveData: MutableLiveData<String> = MutableLiveData()
    val countryLiveData: MutableLiveData<String> = MutableLiveData()
    val cityLiveData: MutableLiveData<String> = MutableLiveData()
    val postalCodeLiveData: MutableLiveData<String> = MutableLiveData()

    val emailLiveData: MutableLiveData<String> = MutableLiveData()
    val passwordLiveData: MutableLiveData<String> = MutableLiveData()

    val subscriptionLiveData: MutableLiveData<String> = MutableLiveData()

    val cardNumberLiveData: MutableLiveData<String> = MutableLiveData()
    val expirationDateLiveData: MutableLiveData<String> = MutableLiveData()
    val cvvCodeLiveData: MutableLiveData<String> = MutableLiveData()
    val cardBrandLiveData: MutableLiveData<String> = MutableLiveData()

    var hasSaveableChanges = false

    val deleteUserLiveData: MutableLiveData<Resource<Void>> = SingleLiveEvent()
    val updateEmailLiveData: MutableLiveData<Resource<Void>> = SingleLiveEvent()
    val updateUserLiveData: MutableLiveData<Resource<User>> = SingleLiveEvent()

    fun deleteUser(password: String) {
        val body = DeleteUserRequestDto().apply {
            this.password = password
        }

        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersMeDelete(deleteUserLiveData, body)
        }
    }

    fun updateEmail(password: String, newEmail: String) {
        val body = UpdateUserEmailDto().apply {
            this.password = password
            this.newEmail = newEmail
        }

        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersMeEmailPut(updateEmailLiveData, body)
        }
    }

    fun updateUser() {
        val updatedUser = UserDto().apply {
            dateOfBirth = dateOfBirthLiveData.value
            email = emailLiveData.value
            postalCode = postalCodeLiveData.value
            sex = sexLiveData.value
            firstName = firstNameLiveData.value?.trim()
            lastName = lastNameLiveData.value?.trim()
        }

        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersMePut(updateUserLiveData, updatedUser)
        }
    }

}