package com.app.coffeeclub.home.referfriends

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context.CLIPBOARD_SERVICE
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.app.coffeeclub.home.BaseNavigationDrawerFragment
import com.app.coffeeclub.home.HomeActivity
import com.app.coffeeclub.home.HomeViewModel
import com.app.coffeeclub.home.R
import com.app.coffeeclub.models.User
import com.app.coffeeclub.utilities.helpers.CustomTabHelper
import com.app.coffeeclub.utilities.helpers.MessageHelper
import kotlinx.android.synthetic.main.fragment_refer_friends.*
import com.app.coffeeclub.resources.R as res


class ReferFriendsFragment(override val layoutResId : Int = R.layout.fragment_refer_friends) : BaseNavigationDrawerFragment() {

    private val homeViewModel : HomeViewModel by viewModelProvider()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val appCompatActivity = requireActivity()

        setTitle(getString(res.string.menu_refer_friends))

        //In case we resume from back button we need to update menu item state
        if (appCompatActivity is HomeActivity) {
            appCompatActivity.selectReferFriendsMenuItem()
        }

        tvTerms.setOnClickListener {
            CustomTabHelper.openCustomTab(requireContext(), getString(res.string.url_terms_and_conditions))
        }

        homeViewModel.getUserLiveData.observe(viewLifecycleOwner, Observer {
            val user = it.data
            if(user != null){
                val referralCode = user.myReferralCode
                tvBtnShare.text = referralCode
                tvBtnShare.setOnClickListener {
                    onShareClicked(user)
                }
                tvCopyCode.setOnClickListener {
                    val myClipboard= requireContext().getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
                    val myClip = ClipData.newPlainText(getString(res.string.refer_friends_copy_code), referralCode)
                    myClipboard.setPrimaryClip(myClip)
                    MessageHelper.showSuccessMessageSnackbar(view,
                        getString(res.string.refer_friends_copy_code_completed),
                        windowInsetsViewModel.windowInsetTop.value)
                }
            }
        })
    }

    private fun onShareClicked(user: User) {
        val shareBody = getString(res.string.refer_friends_share_template, user.myReferralCode)
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getString(res.string.app_name))
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        startActivity(Intent.createChooser(sharingIntent, getString(res.string.menu_refer_friends)))
    }


}
