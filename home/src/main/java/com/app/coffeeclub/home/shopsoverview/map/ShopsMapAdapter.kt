package com.app.coffeeclub.home.shopsoverview.map

import android.location.Location
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.app.coffeeclub.home.R
import com.app.coffeeclub.models.custom.ShopAdCard
import com.app.coffeeclub.home.shopsoverview.ShopItemCallback
import com.app.coffeeclub.models.Shop
import com.app.coffeeclub.models.User
import com.app.coffeeclub.utilities.helpers.SharedPreferencesHelper
import dk.idealdev.utilities.response.Resource
import timber.log.Timber

class ShopsMapAdapter(private val callback: ShopItemCallback,
                      private val userLiveData: MutableLiveData<Resource<User>>) :
    ListAdapter<Shop, RecyclerView.ViewHolder>(ShopDiff()) {

    private var userLocation: Location? = SharedPreferencesHelper.loadUserLocation()
    private val adCardViewType = 3

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == adCardViewType) {
            AdCardMapViewHolder.create(parent, callback, userLiveData)
        } else {
            ShopMapViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_map_shop, parent, false), callback
            )
        }
    }

    fun getIndexOfShop(shop: Shop): Int {
        return currentList.indexOf(shop)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(getItemViewType(position) == adCardViewType){
            (holder as AdCardMapViewHolder).bind()
        } else{
            (holder as ShopMapViewHolder).bind(getItem(position), userLocation, position)
        }
    }

    fun setUserLocation(userLocation: Location?) {
        Timber.d("Setting user location")
        this.userLocation = userLocation
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position) is ShopAdCard) {
            adCardViewType
        } else {
            -1
        }
    }

    private class ShopDiff : DiffUtil.ItemCallback<Shop>() {
        override fun areItemsTheSame(oldItem: Shop, newItem: Shop): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Shop, newItem: Shop): Boolean {
            return oldItem == newItem
        }
    }
}