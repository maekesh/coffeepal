package com.app.coffeeclub.home

import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LocationViewModel : ViewModel() {
    val userLocationLiveData: MutableLiveData<Location> = MutableLiveData()
}