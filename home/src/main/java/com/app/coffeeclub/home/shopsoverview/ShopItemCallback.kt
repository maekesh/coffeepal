package com.app.coffeeclub.home.shopsoverview

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.app.coffeeclub.models.Shop

interface ShopItemCallback {
    fun onShopItemClicked(
        item: Shop,
        adapterPosition : Int,
        imageView: ImageView,
        gradient: View?,
        solid: View,
        shopName: TextView,
        openClosedStatus: TextView,
        divider: View,
        openClosedTime: TextView,
        distance : TextView,
        isFromMap : Boolean
    )    
    fun onSubscriptionAdClicked()
}
