package com.app.coffeeclub.home

import androidx.lifecycle.*
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.app.coffeeclub.home.shopsoverview.paging.PagingState
import com.app.coffeeclub.home.shopsoverview.paging.ShopsDataSource
import com.app.coffeeclub.home.shopsoverview.paging.ShopsDataSourceFactory
import com.app.coffeeclub.models.Shop
import com.app.coffeeclub.models.ShopsResponse
import com.app.coffeeclub.models.User
import com.app.coffeeclub.network.repository.ShopsRepository
import com.app.coffeeclub.network.repository.UsersRepository
import dk.idealdev.utilities.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.math.BigDecimal

class HomeViewModel : ViewModel() {

    var shopsList: LiveData<PagedList<Shop>>
    private val shopsDataSourceFactory: ShopsDataSourceFactory = ShopsDataSourceFactory()
    private val pageSize = 10

    val getUserLiveData : MutableLiveData<Resource<User>> = MutableLiveData()
    val getAllShopsLiveData : MutableLiveData<Resource<ShopsResponse>> = MutableLiveData()

    init {
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize)
            .setEnablePlaceholders(true)
            .setPrefetchDistance(5)
            .build()

        shopsList = LivePagedListBuilder(shopsDataSourceFactory, config).build()
    }

    fun loadShopsWithSearchParameters(queryFilter : String){
        shopsDataSourceFactory.queryFilter = if(queryFilter.isBlank()) null else queryFilter
        forceRefreshShopsPaginatedList()
    }

    fun forceRefreshShopsPaginatedList() {
        shopsDataSourceFactory.shopDataSourceLiveData.value?.invalidate()
    }

    fun getPagingState(): LiveData<PagingState> = Transformations.switchMap<ShopsDataSource,
            PagingState>(shopsDataSourceFactory.shopDataSourceLiveData, ShopsDataSource::state)

    fun getTotalCount(): LiveData<Int> = Transformations.switchMap<ShopsDataSource,
            Int>(shopsDataSourceFactory.shopDataSourceLiveData, ShopsDataSource::totalCountLiveData)

    fun getUser(forceRefresh : Boolean = false) {
        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersMeGet(getUserLiveData, forceRefresh)
        }
    }

    fun getAllShops(){
        viewModelScope.launch(Dispatchers.IO) {
            ShopsRepository.shopsGet(getAllShopsLiveData, limit = BigDecimal.valueOf(1000), page = BigDecimal.valueOf(1))
        }
    }

}