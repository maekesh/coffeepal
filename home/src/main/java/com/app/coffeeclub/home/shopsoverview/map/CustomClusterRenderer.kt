package com.app.coffeeclub.home.shopsoverview.map

import android.content.Context
import android.graphics.Color
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.app.coffeeclub.resources.R.color
import com.google.android.gms.maps.model.Marker

class CustomClusterRenderer(val context: Context, map: GoogleMap, clusterManager: ClusterManager<ShopClusterItem>)
    : DefaultClusterRenderer<ShopClusterItem>(context, map, clusterManager) {

    override fun onBeforeClusterItemRendered(item: ShopClusterItem?, markerOptions: MarkerOptions?) {
        super.onBeforeClusterItemRendered(item, markerOptions)
        if(item != null){
            //Set color and alpha on marker
            val floatArrayOf = FloatArray(3)
            Color.colorToHSV(ContextCompat.getColor(context, color.purple), floatArrayOf)
            markerOptions?.icon(BitmapDescriptorFactory.defaultMarker(floatArrayOf[0]))
            markerOptions?.alpha(0.5f)
        }
    }

    override fun getColor(clusterSize: Int): Int {
        return ContextCompat.getColor(context, color.colorPrimary)
    }

}