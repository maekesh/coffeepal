package com.app.coffeeclub.home.shopsoverview.paging

enum class PagingState {
    LOADING_INITIAL, LOADING_PAGE, ERROR, SUCCESS
}