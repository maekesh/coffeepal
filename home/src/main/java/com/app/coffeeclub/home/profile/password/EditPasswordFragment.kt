package com.app.coffeeclub.home.profile.password

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.autofill.AutofillManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.coffeeclub.home.R
import com.app.coffeeclub.home.profile.BaseEditProfileFragment
import com.app.coffeeclub.uicomponents.forms.FormHelper
import com.app.coffeeclub.uicomponents.forms.adapters.InputFormAdapter
import com.app.coffeeclub.uicomponents.forms.items.InputFormItem
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.getLocalizationText
import com.app.coffeeclub.utilities.extensions.getStringData
import com.app.coffeeclub.utilities.extensions.validate
import com.app.coffeeclub.utilities.helpers.AutofillHelper
import com.app.coffeeclub.utilities.helpers.MessageHelper
import com.app.coffeeclub.utilities.helpers.ValidationHelper
import dk.idealdev.utilities.response.Status
import kotlinx.android.synthetic.main.fragment_edit_password.*
import com.app.coffeeclub.resources.R as res

class EditPasswordFragment(override val layoutResId: Int = R.layout.fragment_edit_password) : BaseEditProfileFragment() {

    private val passwordViewModel: PasswordViewModel by viewModelProvider()

    private var passwordAdapter: InputFormAdapter? = null

    private var autofillManager: AutofillManager? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle(getString(res.string.profile_password_title))
        showBackAsWhiteArrow()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            autofillManager = ContextCompat.getSystemService(requireContext(), AutofillManager::class.java)
        }

        initAdapter()
        populateAdapter()

        observeChangePassword(view)
        observeInputData()
        tvSaveChanges.setText(getLocalizationText(getString(com.app.coffeeclub.resources.R.string.profile_password_button), Language.getLanguageHashMap()))
        tvSaveChanges.setOnButtonClickListener {
            if (areInputsValid()) {
                passwordViewModel.changePassword()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        passwordViewModel.clear()
    }

    private fun observeInputData() {
        passwordViewModel.currentPasswordLiveData.observe(viewLifecycleOwner, Observer {
            updateSaveButtonEnabledState()
        })

        passwordViewModel.newPasswordLiveData.observe(viewLifecycleOwner, Observer {
            updateSaveButtonEnabledState()
        })

        passwordViewModel.repeatedNewPasswordLiveData.observe(viewLifecycleOwner, Observer {
            updateSaveButtonEnabledState()
        })
    }

    private fun updateSaveButtonEnabledState() {
        val currentPassword = passwordViewModel.currentPasswordLiveData.value
        val newPassword = passwordViewModel.newPasswordLiveData.value
        val repeatedNewPassword = passwordViewModel.repeatedNewPasswordLiveData.value

        val currentPasswordValid = currentPassword != null && ValidationHelper.isPasswordValid(requireContext(), currentPassword)
        val newPasswordValid = newPassword != null && ValidationHelper.isPasswordValid(requireContext(), newPassword)
        val repeatedNewPasswordValid = repeatedNewPassword != null && repeatedNewPassword == newPassword

        tvSaveChanges.setButtonEnabled(currentPasswordValid && newPasswordValid && repeatedNewPasswordValid)
    }

    private fun observeChangePassword(view: View) {
        passwordViewModel.changePasswordLiveData.observe(viewLifecycleOwner, Observer {

            tvSaveChanges.setProgressState(it.status == Status.LOADING)

            when (it.status) {
                Status.SUCCESS -> {
                    MessageHelper.showSuccessMessageSnackbar(view,
                            getStringData(getString(com.app.coffeeclub.resources.R.string.password_changed), Language.getLanguageHashMap()),
                            windowInsetsViewModel.windowInsetTop.value
                    )
                    navigateToSource()
                }
                Status.ERROR -> {
                    MessageHelper.showErrorMessageSnackbar(view,
                            topMargin = windowInsetsViewModel.windowInsetTop.value)
                }
                Status.LOADING -> {
                }
            }
        })
    }

    private fun navigateToSource() {
        requireActivity().onBackPressed()
    }

    private fun initAdapter() {
        if (passwordAdapter == null) {
            passwordAdapter = InputFormAdapter()
        }

        rvEditPassword.apply {
            adapter = passwordAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    @SuppressLint("InlinedApi")
    private fun populateAdapter() {
        val formContentList = mutableListOf<InputFormItem>()
        formContentList.add(
                InputFormItem(
                        valueLiveData = passwordViewModel.currentPasswordLiveData,
                        validation = ::validateCurrentPassword,
                        label = getString(res.string.profile_password_label_1),
                        hint = getString(res.string.profile_password_placeholder_1),
                        inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD,
                        autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager)
                )
        )
        formContentList.add(
                InputFormItem(
                        valueLiveData = passwordViewModel.newPasswordLiveData,
                        validation = ::validatePasswordLength,
                        label = getString(res.string.profile_password_label_2),
                        hint = getString(res.string.profile_password_placeholder_2),
                        inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD,
                        autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                        autoFillHint = View.AUTOFILL_HINT_PASSWORD
                )
        )
        formContentList.add(
                InputFormItem(
                        valueLiveData = passwordViewModel.repeatedNewPasswordLiveData,
                        validation = ::validatePasswordMatch,
                        label = getString(res.string.profile_password_label_3),
                        hint = getString(res.string.profile_password_placeholder_3),
                        inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD,
                        autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                        autoFillHint = View.AUTOFILL_HINT_PASSWORD
                )
        )
        passwordAdapter?.submitList(formContentList)
    }

    private fun validateCurrentPassword(etValue: EditText, tvLabel: TextView, ivStatus: ImageView, hasFocus: Boolean, isNotFinalCheck: Boolean) : Boolean {
        return true
    }
    private fun validatePasswordLength(etValue: EditText, tvLabel: TextView, ivStatus: ImageView, hasFocus: Boolean, isNotFinalCheck: Boolean): Boolean {
        return etValue.validate({ password -> ValidationHelper.isPasswordValid(requireContext(), password) },
                getStringData(getString(com.app.coffeeclub.resources.R.string.password_length), Language.getLanguageHashMap()),
                tvLabel = tvLabel,
                tvError = tvError,
                ivStatus = ivStatus,
                hasFocus = hasFocus,
                isNotFinalCheck = isNotFinalCheck
        )
    }

    private fun validatePasswordMatch(etValue: EditText, tvLabel: TextView, ivStatus: ImageView, hasFocus: Boolean, isNotFinalCheck: Boolean): Boolean {
        return etValue.validate({ password -> password == passwordViewModel.newPasswordLiveData.value },
                getStringData(getString(com.app.coffeeclub.resources.R.string.password_match), Language.getLanguageHashMap()),
                tvLabel = tvLabel,
                tvError = tvError,
                ivStatus = ivStatus,
                hasFocus = hasFocus,
                isNotFinalCheck = isNotFinalCheck
        )
    }

    private fun areInputsValid(): Boolean {
        val list = passwordAdapter?.currentList ?: return false
        val layoutManager = rvEditPassword.layoutManager ?: return false

        return FormHelper.validateForm(requireActivity(), list, layoutManager)
    }
}