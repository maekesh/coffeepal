package com.app.coffeeclub.home.profile.subscription

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.app.coffeeclub.home.HomeViewModel
import com.app.coffeeclub.home.R
import com.app.coffeeclub.home.profile.BaseEditProfileFragment
import com.app.coffeeclub.models.StripePlan
import com.app.coffeeclub.models.StripeProduct
import com.app.coffeeclub.models.StripeSubscription
import com.app.coffeeclub.onboarding.signup.step4.SubscriptionAdapter
import com.app.coffeeclub.uicomponents.dialogs.showCoffeeClubDialog
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.getStringData
import com.app.coffeeclub.utilities.extensions.showText
import com.app.coffeeclub.utilities.helpers.CustomTabHelper
import com.app.coffeeclub.utilities.helpers.MessageHelper
import com.app.coffeeclub.utilities.helpers.SegmentHelper
import com.app.coffeeclub.utilities.helpers.SegmentHelper.Segment.*
import com.app.coffeeclub.utilities.helpers.SegmentHelper.isTrialing
import com.app.coffeeclub.utilities.helpers.SubscriptionHelper
import dk.idealdev.utilities.response.Status
import kotlinx.android.synthetic.main.fragment_edit_subscription.*
import java.util.*
import com.app.coffeeclub.resources.R as res

class EditSubscriptionFragment(override val layoutResId: Int = R.layout.fragment_edit_subscription) : BaseEditProfileFragment(), SubscriptionAdapter.ItemCallback {

    private val subscriptionViewModel: SubscriptionViewModel by viewModelProvider()
    private val homeViewModel: HomeViewModel by viewModelProvider()
    private var subscriptionAdapter: SubscriptionAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle(getString(res.string.profile_overview_sub_title))
        showBackAsWhiteArrow()
        setActionListener()
        initAdapter()
        changeSubscription()
        observeFetchSubscriptions()
        observeFetchMySubscription()
        subscriptionViewModel.fetchSubscriptionProducts()
        subscriptionViewModel.fetchMySubscription()
    }

    private fun setActionListener() {
        tvBtnCancel.setOnClickListener {
            onCancelSubscriptionClicked()
        }

        changePlanBtn.setOnClickListener {
            changePlanSubscription()
        }


    }

    private fun changePlanSubscription() {
        if (isSelectedPlanYearly()){
            onYearlyChangeSubscrption()
        } else {
            onMonthlyChangeSubscrption()
        }
    }

    private fun performUpdateSubscriptionAPICall (){
        getChangePlanData()?.let {
            subscriptionViewModel.changeSubscriptionPlan(it)
        }
    }

    private fun getChangePlanData(): StripePlan? {
        var changePlanRequest: StripePlan ?= null
        val availablePlans = subscriptionViewModel.getAvailableProductsLiveData.value?.data?.get(0)?.plans
        availablePlans?.forEachIndexed { index, stripePlan ->
            if (stripePlan.id != subscriptionViewModel.getSubscriptionMeLiveData.value?.data?.plan?.id) {
                changePlanRequest = stripePlan
            }
        }
        return changePlanRequest
    }


    private fun initAdapter() {
        subscriptionAdapter = SubscriptionAdapter(this@EditSubscriptionFragment, true)
        rvSubscriptions.apply {
            adapter = subscriptionAdapter
            layoutManager = GridLayoutManager(requireContext(), 2)
        }
    }

    private fun observeFetchSubscriptions() {
        subscriptionViewModel.getAvailableProductsLiveData.observe(viewLifecycleOwner, Observer { it ->
            progressBar.isVisible = it.status == Status.LOADING

            when (it.status) {
                Status.SUCCESS -> {
                    val products = it.data ?: return@Observer
                    showPlanList(products)
                    getMySubscriptionProduct(products, subscriptionViewModel.getSubscriptionMeLiveData.value?.data)
                    selectedPlans()
                    subscriptionViewModel.getSubscriptionMeLiveData.value?.data?.let{data->
                        initUIFrom(data)
                    }

                }
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    MessageHelper.showErrorMessageSnackbar(requireView(),
                            topMargin = windowInsetsViewModel.windowInsetTop.value)
                }
            }
        })
    }

    private fun showPlanList(plans: List<StripeProduct>) {
        subscriptionAdapter?.submitList(updateList(plans[0].plans))
    }


    private fun observeFetchMySubscription() {
        subscriptionViewModel.getSubscriptionMeLiveData.observe(viewLifecycleOwner, Observer {
            progressBar.isVisible = it.status == Status.LOADING

            when (it.status) {
                Status.SUCCESS -> {
                    val mySubscription = it.data ?: return@Observer
                    selectedPlans()
                    getMySubscriptionProduct(subscriptionViewModel.getAvailableProductsLiveData.value?.data, mySubscription)
                    initUIFrom(mySubscription)
                }
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    MessageHelper.showErrorMessageSnackbar(requireView(),
                            topMargin = windowInsetsViewModel.windowInsetTop.value)
                }
            }
        })
    }

    private fun updateList(plans: List<StripePlan>?): List<StripePlan>? {
        plans?.forEachIndexed { index, stripePlan ->
            if (index == 0 && stripePlan.interval == getString(com.app.coffeeclub.resources.R.string.year)) {
                Collections.swap(plans, 0, 1)
            }
        }
        return plans
    }

    private fun selectedPlans() {
        val availablePlans = subscriptionViewModel.getAvailableProductsLiveData.value?.data?.get(0)?.plans
        availablePlans?.forEachIndexed { index, stripePlan ->
            if (stripePlan.id == subscriptionViewModel.getSubscriptionMeLiveData.value?.data?.plan?.id) {
                subscriptionAdapter?.selectItem(stripePlan)
            }
        }
    }

    private fun getMySubscriptionProduct(products: List<StripeProduct>?, mySubscription: StripeSubscription?) {
        products ?: return
        mySubscription ?: return

        findCurrentProduct(products, mySubscription)
    }

    private fun findCurrentProduct(products: List<StripeProduct>, mySubscription: StripeSubscription) {
        products.forEach { product ->
            if (product.plans.any { it.id == mySubscription.plan?.id }) {
                subscriptionViewModel.myProductLiveData.value = product
                return
            }
        }
    }

    private fun isSelectedPlanYearly() : Boolean {
        var isYearly = false
        val availablePlans = subscriptionViewModel.getAvailableProductsLiveData.value?.data?.get(0)?.plans
        availablePlans?.forEachIndexed { index, stripePlan ->
            if (stripePlan.id == subscriptionViewModel.getSubscriptionMeLiveData.value?.data?.plan?.id) {
                if (stripePlan.interval == getString(com.app.coffeeclub.resources.R.string.year)) {
                    isYearly = true
                }
            }
        }
        return isYearly
    }


    private fun initUIFrom(mySubscription: StripeSubscription) {
        val segment = SegmentHelper.getSegment(homeViewModel.getUserLiveData.value?.data)
        val isChurned = segment == SUBSCRIBED_CHURNED

        tvCurrentSubscriptionTitle.showText(getString(res.string.profile_sub_headline_1), Language.getLanguageHashMap())
        tvBtnCancel.showText(getString(res.string.profile_sub_cancel_button), Language.getLanguageHashMap())
        changePlanBtn.isVisible = true
        paymentCycleLabel.showText(getString(res.string.profile_sub_label_1), Language.getLanguageHashMap())
        nextPaymentLabel.showText(getString(res.string.profile_sub_label_2), Language.getLanguageHashMap())

        if(mySubscription.updateSubscription!=null){
            changePlanBtn.visibility = View.GONE
            updatePlanTitle.visibility = View.VISIBLE
            updatePlanTxt.visibility = View.VISIBLE
            if(mySubscription.updateSubscription.duration.equals(getString(com.app.coffeeclub.resources.R.string.year))){
                updatePlanTitle.showText(getString(com.app.coffeeclub.resources.R.string.profile_sub_monthly_headline), Language.getLanguageHashMap())
                updatePlanTxt.showText(getString(com.app.coffeeclub.resources.R.string.profile_sub_monthly_paragraph), Language.getLanguageHashMap())
            } else {
                updatePlanTitle.showText(getString(com.app.coffeeclub.resources.R.string.profile_sub_yearly_headline), Language.getLanguageHashMap())
                updatePlanTxt.showText(getString(com.app.coffeeclub.resources.R.string.profile_sub_yearly_paragraph), Language.getLanguageHashMap())
            }
        } else if (isSelectedPlanYearly()) {
            changePlanBtn.text = getStringData(getString(com.app.coffeeclub.resources.R.string.profile_sub_yearly_button), Language.getLanguageHashMap())
            changePlanBtn.background = context?.getDrawable(com.app.coffeeclub.resources.R.drawable.button_tertiary_onblack_normal_selector)
            yearlyPlanChangeDiscountTxt.visibility = View.GONE
        } else {
            changePlanBtn.showText(getString(res.string.profile_sub_monthly_headline), Language.getLanguageHashMap())
            yearlyPlanChangeDiscountTxt.showText(getString(res.string.profile_sub_small_copy), Language.getLanguageHashMap())
        }

//        tvSubscriptionName.text = when {
//            isChurned -> getString(res.string.profile_edit_subscription_subscription_churned)
//            segment.isTrialing() -> getString(res.string.profile_edit_subscription_subscription_trialing_cost)
//            else -> {
//                mySubscription.name
//            }
//        }

        paymentCycletxt.text = getString(if (isChurned) {
            res.string.profile_edit_subscription_payment_timeline_expires
        } else {
            res.string.profile_edit_subscription_payment_timeline
        })

//        tvSubscriptionDescription.isVisible = !isChurned
//        tvSubscriptionDescription.text = mySubscription.description

        if (mySubscription.currentPeriodStart != null && mySubscription.currentPeriodEnd != null) {
            paymentCycletxt.text = SubscriptionHelper.getSubscriptionTimeLine(requireContext(), mySubscription)
            nextPaymentTxt.text = if (isChurned) {
                getString(res.string.claim_not_subscribed_button_dismiss)
            } else {
                SubscriptionHelper.getSubscriptionNextPayment(requireContext(), mySubscription)
            }
            nextPaymentTxt.setTextColor(ContextCompat.getColor(requireContext(),
                    if (isChurned) {
                        res.color.red_light
                    } else {
                        res.color.gray100
                    })
            )
        }

//        tvFreeTrial.text = SubscriptionHelper.getFreeTrial(
//            requireContext(),
//            mySubscription,
//            hasCreditCard = segment == TRIALING_SUBSCRIBED
//        )
//
//        tvFreeTrial.isVisible = segment.isTrialing()
//        tvFreeTrialTitle.isVisible = segment.isTrialing()
//
//        tvBtnCancelSubscription.isVisible = segment == SUBSCRIBED
//        tvBtnContinueSubscription.isVisible = isChurned
//
//        if(isChurned) {
//            tvBtnContinueSubscription.setOnClickListener {
//                val stripeProduct = subscriptionViewModel.getAvailableProductsLiveData.value?.data?.first()
//                if (stripeProduct != null) {
//                    changeSubscription(stripeProduct)
//                }
//            }
//        } else {
//            tvBtnCancelSubscription.setOnClickListener {
//                onCancelSubscriptionClicked()
//            }
//        }
    }

    private fun onMonthlyChangeSubscrption(){
        showCoffeeClubDialog(requireContext())
                .setTitle(res.string.profile_sub_cancel_dialog_title)
                .setMessage(res.string.profile_sub_monthly_dialog_paragraph)
                .secondaryButton(res.string.profile_sub_cancel_dialog_link_1) {
                    readConditions()
                }
                .positiveButton(res.string.profile_sub_monthly_dialog_button, true) {
                    performUpdateSubscriptionAPICall()
                }
                .negativeButton(res.string.profile_sub_cancel_dialog_link_2)
    }

    private fun onYearlyChangeSubscrption(){
        showCoffeeClubDialog(requireContext())
                .setTitle(res.string.profile_sub_cancel_dialog_title)
                .setMessage(res.string.profile_sub_yearly_dialog_paragraph)
                .secondaryButton(res.string.profile_sub_cancel_dialog_link_1) {
                    readConditions()
                }
                .positiveButton(res.string.profile_sub_yearly_dialog_button, true) {
                    performUpdateSubscriptionAPICall()
                }
                .negativeButton(res.string.profile_sub_cancel_dialog_link_2)
    }



    private fun onCancelSubscriptionClicked() {
        showCoffeeClubDialog(requireContext())
                .setTitle(res.string.profile_sub_cancel_dialog_title)
                .setMessage(res.string.profile_sub_cancel_dialog_paragraph)
                .secondaryButton(res.string.profile_sub_cancel_dialog_link_1) {
                    readConditions()
                }
                .positiveButton(res.string.profile_sub_cancel_dialog_button, true) {
                    cancelSubscription()
                }
                .negativeButton(res.string.profile_sub_cancel_dialog_link_2)
    }

    private fun cancelSubscription() {
        subscriptionViewModel.cancelSubscriptionPlan()

        subscriptionViewModel.deleteSubscriptionLiveData.observe(viewLifecycleOwner, Observer {
//            tvBtnCancelSubscription.isEnabled = it.status != Status.LOADING
//            pbCancelSubscription.isVisible = it.status == Status.LOADING

            when (it.status) {
                Status.SUCCESS -> {
                    view?.let { view ->
                        MessageHelper.showSuccessMessageSnackbar(view,
                                getStringData(getString(res.string.profile_sub_cancel_placeholder_2), Language.getLanguageHashMap()),
                                windowInsetsViewModel.windowInsetTop.value)
                    }

                    homeViewModel.getUser(true)
                    findNavController().popBackStack()
                }
                Status.LOADING -> {
                }
                Status.ERROR -> {
                }
            }
        })
    }

//    override fun onChangeSubscriptionClicked(stripeProduct: StripeProduct) {
//        changeSubscription(stripeProduct)
//    }

    private fun readConditions() {
        CustomTabHelper.openCustomTab(requireContext(), getString(res.string.url_terms_and_conditions))
    }

    private fun changeSubscription() {
        subscriptionViewModel.postSubscriptionLiveData.observe(viewLifecycleOwner, Observer {
            progressBar.isVisible = it.status == Status.LOADING

            when (it.status) {
                Status.SUCCESS -> {
                    view?.let { view ->
                        MessageHelper.showSuccessMessageSnackbar(
                                view,
                                getString(res.string.profile_edit_subscription_success),
                                windowInsetsViewModel.windowInsetTop.value)
                    }
                    homeViewModel.getUser(true)
                    subscriptionViewModel.fetchMySubscription(true)
                }
                Status.LOADING -> {
                }
                Status.ERROR -> {

                }
            }
        })
    }

    override fun onItemClicked(item: StripePlan) {

    }
}