package com.app.coffeeclub.home.claim

import android.Manifest
import android.animation.LayoutTransition
import android.content.Intent
import android.content.res.ColorStateList
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.view.*
import android.view.animation.AnimationUtils
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.app.coffeeclub.actions.Actions
import com.app.coffeeclub.home.BaseHomeFragment
import com.app.coffeeclub.home.HomeActivity
import com.app.coffeeclub.home.HomeViewModel
import com.app.coffeeclub.home.LocationViewModel
import com.app.coffeeclub.home.R
import com.app.coffeeclub.models.Claim
import com.app.coffeeclub.models.ClaimDto
import com.app.coffeeclub.models.Shop
import com.app.coffeeclub.network.glide.GlideApp
import com.app.coffeeclub.uicomponents.dialogs.showCoffeeClubDialog
import com.app.coffeeclub.utilities.extensions.getOpensOrClosesAtString
import com.app.coffeeclub.utilities.extensions.isOpen
import com.app.coffeeclub.utilities.helpers.*
import com.ebanx.swipebtn.SwipeButton
import dk.idealdev.utilities.response.Resource
import dk.idealdev.utilities.response.Status
import kotlinx.android.synthetic.main.bottom_swipe_container.*
import kotlinx.android.synthetic.main.fragment_claim.*
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.temporal.ChronoUnit
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.HttpException
import java.net.HttpURLConnection
import com.app.coffeeclub.resources.R as res

class ClaimFragment(override val layoutResId: Int = R.layout.fragment_claim) : BaseHomeFragment() {

    private val args: ClaimFragmentArgs by navArgs()

    private val locationViewModel: LocationViewModel by viewModelProvider()
    private val homeViewModel: HomeViewModel by viewModelProvider()
    private val claimViewModel: ClaimViewModel by viewModelProvider()

    private var imagePagerAdapter: ImagePagerAdapter? = null
    private var distanceInMeters: Float = Float.MAX_VALUE
    private lateinit var currentSwipeButton: SwipeButton
    private var countDownTimer : CountDownTimer? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = super.onCreateView(inflater, container, savedInstanceState)
        val cover = root?.findViewById<ImageView>(R.id.ivTransitionCover)
        val gradient = root?.findViewById<View>(R.id.vGradient)
        val solid = root?.findViewById<View>(R.id.vSolid)
        val tvOpenClosingStatus = root?.findViewById<TextView>(R.id.tvOpenClosedStatus)
        val tvOpenClosingTime = root?.findViewById<TextView>(R.id.tvOpenClosingTime)
        val tvDistance = root?.findViewById<TextView>(R.id.tvDistanceFromUser)
        val textDivider = root?.findViewById<View>(R.id.vTextDivider)
        val titleToolbar = root?.findViewById<Toolbar>(R.id.toolbarClaim)
        val title = root?.findViewById<TextView>(R.id.tvTitle)

        setupTransitions(
                args,
                cover,
                gradient,
                solid,
                tvOpenClosingStatus,
                textDivider,
                tvOpenClosingTime,
                titleToolbar,
                title,
                tvDistance
        )

        title?.text = args.shop.name

        GlideApp.with(this)
                .load(ImageHelper.getCoverUrl(args.shop.images))
                .into(cover!!)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        container.layoutTransition.enableTransitionType(LayoutTransition.CHANGING)

        initUI(args.shop)
    }

    override fun onTopInsetChanged(inset: Int) {
        super.onTopInsetChanged(inset)
        InsetHelper.addTopMargin(toolbarClaim, inset)
    }

    override fun onBottomInsetChanged(inset: Int) {
        super.onBottomInsetChanged(inset)
        InsetHelper.addBottomPadding(containerSlider, inset)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_claim, menu)
    }

    private fun initDistance() {
        locationViewModel.userLocationLiveData.observe(viewLifecycleOwner, Observer { userLocation ->
            val shop = args.shop

            if (userLocation != null && shop.latitude != null && shop.longitude != null) {
                val distanceInMeters = LocationHelper.getDistanceToShop(shop, userLocation)

                tvDistanceFromUser.isVisible = true
                tvDistanceFromUser.text = FormatHelper.formatDistance(distanceInMeters, requireContext())
            }
        })
    }

    private fun initUI(shop: Shop) {
        initCollapsingToolbar(shop)
        initDescription(shop)
        initSwipe()
        initDistance()
        initDirections()
    }

    private fun initDirections() {
        tvBtnDirections.setOnClickListener {
            val encodedAddress = Uri.encode(tvAddress.text.toString())
            val gmmIntentUri = Uri.parse("google.navigation:q=$encodedAddress")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }

    private fun initCollapsingToolbar(shop: Shop) {
        showBackAsWhiteArrow()
        var imageUrls = shop.images.map { it.versions[1].url }

        dotIndicator.isVisible = imageUrls.size > 1

        if(imageUrls.isEmpty()) {
            imageUrls = listOf("")
        }
        
        imagePagerAdapter = ImagePagerAdapter(requireContext(), imageUrls, args.adapterPosition)
        viewPagerImages.adapter = imagePagerAdapter
        dotIndicator.setupWithViewPager(viewPagerImages)

        tvOpenClosedStatus.text =
                getString(if (shop.isOpen()) res.string.shopsoverview_open else res.string.shopsoverview_closed)
        tvOpenClosedStatus.setTextColor(
                ContextCompat.getColor(
                        requireContext(),
                        if (shop.isOpen()) res.color.gray100 else res.color.red
                )
        )
        val opensOrClosesString = shop.getOpensOrClosesAtString(requireContext())
        tvOpenClosingTime.text = opensOrClosesString
        vTextDivider.isVisible = opensOrClosesString.isNotBlank()
    }

    private fun initDescription(shop: Shop) {
        tvDescription.text = shop.description
        tvAddress.text = String.format("${shop.street} ${shop.streetNumber}, ${shop.city}")

        tvMenuTitles.text = ClaimHelper.getMenuTitlesString(shop.menu)

        tvOpeningHoursDays.text = ClaimHelper.getOpeningDayString(shop.openingHours)
        tvOpeningHoursTime.text = ClaimHelper.getOpeningTimeString(requireContext(), shop.openingHours)

        tvOpeningHoursTime.post {
            ClaimHelper.initShowAll(
                    requireContext(),
                    tvMenuTitles,
                    null,
                    vMenuGradient,
                    tvMenuShowAll
            )
        }

        tvOpeningHoursTime.post {
            ClaimHelper.initShowAll(
                    requireContext(),
                    tvOpeningHoursDays,
                    tvOpeningHoursTime,
                    vOpeningHoursGradient,
                    tvOpeningHoursShowAll
            )
        }

        tvEmail.text = shop.contactEmail
        tvEmail.setOnClickListener {
            onEmailClicked(shop)
        }

        tvPhone.text = shop.contactPhone
        tvPhone.setOnClickListener {
            onPhoneClicked(shop)
        }
    }

    private fun onEmailClicked(shop: Shop) {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:") // only email apps should handle this
            putExtra(Intent.EXTRA_EMAIL, arrayOf(shop.contactEmail))
        }
        if (intent.resolveActivity(requireActivity().packageManager) != null) {
            startActivity(intent)
        }
    }

    private fun onPhoneClicked(shop: Shop) {
        val intent = Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel:${shop.contactPhone}")
        }
        if (intent.resolveActivity(requireActivity().packageManager) != null) {
            startActivity(intent)
        }
    }

    private fun initSwipe() {
        slideUpSlider()
        initCountdownUi()
        initSwipeBtn(swipeBtnBasic)

        homeViewModel.getUserLiveData.observe(viewLifecycleOwner, Observer {
            val user = it.data

            //Check for latest user claim and show countdown if still within the claim period
            val latestClaim = user?.latestClaim
            if (latestClaim != null) {
                updateSliderUi(latestClaim)
            }
        })
    }

    private fun initCountdownUi() {
        tvCountDown.setTextColor(ContextCompat.getColor(requireContext(), res.color.gray100))
        vCountDownBackground.background =
                ContextCompat.getDrawable(requireContext(), res.drawable.swipe_button_basic_background)
        progressBarClaim.indeterminateTintList =
                ColorStateList.valueOf(ContextCompat.getColor(requireContext(), res.color.gray50))
    }

    private fun initSwipeBtn(swipeButton: SwipeButton) {
        currentSwipeButton = swipeButton
        swipeButton.setOnActiveListener {
            onButtonSwiped()
        }
    }

    private fun onButtonSwiped() {
        val user = homeViewModel.getUserLiveData.value?.data
        val segment = SegmentHelper.getSegment(user)
        if (segment == SegmentHelper.Segment.VISITING || segment == SegmentHelper.Segment.CHURNED) {
            showNotSubscribedDialog()
        } else if(!EasyPermissions.hasPermissions(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            showNoLocationDialog()
        } else {
            val showDistanceThresholdInMeters = resources.getInteger(res.integer.shop_arrival_threshold_in_meters)
            if (distanceInMeters > showDistanceThresholdInMeters) {
                showDistanceToGreatDialog()
            } else {
                claimCoffee()
            }
        }
    }

    private fun showNotSubscribedDialog() {
        showCoffeeClubDialog(requireContext())
            .setTitle(res.string.claim_not_subscribed_title)
            .setMessage(res.string.claim_not_subscribed_description)
            .positiveButton(res.string.claim_not_subscribed_button_confirm) {
                requireActivity().startActivityForResult(
                    Actions.getOnboardingIntent(
                        requireContext(),
                        goToSignup = true,
                        hasCreditCard = false
                    ),
                    HomeActivity.RC_SUBSCRIPTION
                )
            }
            .negativeButton(res.string.claim_not_subscribed_button_dismiss)
    }

    private fun showNoLocationDialog() {
        showCoffeeClubDialog(requireContext())
            .setTitle(res.string.claim_no_gps_title)
            .setMessage(getString(res.string.claim_no_gps_description, args.shop.name))
            .positiveButton(res.string.claim_no_gps_confirm) {
                claimCoffee()
            }
            .negativeButton(res.string.claim_no_gps_dismiss)
    }

    private fun showDistanceToGreatDialog() {
        showCoffeeClubDialog(requireContext())
            .setTitle(res.string.claim_not_within_radius_title)
            .setMessage(getString(res.string.claim_not_within_radius_description, args.shop.name))
            .positiveButton(res.string.claim_not_within_radius_button_confirm) {
                claimCoffee()
            }
            .negativeButton(res.string.claim_not_within_radius_button_dismiss)
    }
    private fun claimCoffee() {
        claimViewModel.postClaimLiveData.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    handlePostClaimSuccess(it)
                }
                Status.ERROR -> {
                    handlePostClaimError(it)
                }
                Status.LOADING -> {
                }
            }
        })

        val claimDto = ClaimDto().apply {
            shopId = args.shop.id
        }
        claimViewModel.postClaim(claimDto)
    }

    private fun handlePostClaimSuccess(it: Resource<Claim>) {
        val claim = it.data
        if (claim != null) {
            showCoffeeClaimed(claim)
        } else {
            showErrorMessage(res.string.general_error_generic)
        }
    }

    private fun handlePostClaimError(it: Resource<Claim>) {
        val throwable = it.throwable
        if (throwable is HttpException) {
            when (throwable.code()) {
                HttpURLConnection.HTTP_PAYMENT_REQUIRED -> showErrorMessage(res.string.claim_error_402_no_subscription)
                HttpURLConnection.HTTP_CONFLICT -> showErrorMessage(res.string.claim_error_409_already_claimed)
                else -> {
                    showErrorMessage(res.string.general_error_generic)
                }
            }
        } else {
            showErrorMessage(res.string.general_error_generic)
        }
    }

    private fun showErrorMessage(@StringRes messageResId: Int) {
        view?.let { view ->
            MessageHelper.showErrorMessageSnackbar(view,
                getString(messageResId),
                windowInsetsViewModel.windowInsetTop.value
            )
        }
    }

    private fun showCoffeeClaimed(claim: Claim) {
        //Call get user to make sure the latest claim is update
        homeViewModel.getUser(true)
        MessageHelper.showSnackbarWithAction(
                currentSwipeButton,
                getString(res.string.claim_swipe_btn_after),
                getString(res.string.general_regret),
                View.OnClickListener {
                    //Regret action pressed
                    currentSwipeButton.isVisible = true
                    deleteClaim(claim)
                }, topMargin = windowInsetsViewModel.windowInsetTop.value)
    }

    private fun deleteClaim(claim: Claim) {
        claimViewModel.deleteClaimLiveData.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    currentSwipeButton.isVisible = true
                    vCountDownBackground.isVisible = false
                    currentSwipeButton.isVisible = true
                    tvPremiumCountDownLabel.isVisible = false
                    tvBasicCountDownLabel.isVisible = false
                    tvSwipeHint.text = getString(res.string.claim_swipe_hint_before)
                    homeViewModel.getUser(true)
                }
                Status.ERROR -> {
                    view?.let { view ->
                        MessageHelper.showErrorMessageSnackbar(view,
                            topMargin = windowInsetsViewModel.windowInsetTop.value)
                    }
                }
                Status.LOADING -> {
                }
            }
        })
        claimViewModel.deleteClaim(claim.id)
    }

    private fun updateSliderUi(claim: Claim) {
        tvCountDown ?: return

        if(!::currentSwipeButton.isInitialized) {
            return
        }

        val claimCreated = Instant.ofEpochMilli(claim.createdAt.time)
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime()
        val now = LocalDateTime.now()
        val secondsBetween = ChronoUnit.SECONDS.between(claimCreated, now)
        val claimCountDownSeconds = resources.getInteger(res.integer.claim_countdown_in_seconds)

        //Allow swipe if latest claim is not today
        if (claimCreated.toLocalDate() != now.toLocalDate()) {
            currentSwipeButton.isVisible = true
            return
        }


        tvCountDown.isVisible = true
        vCountDownBackground.isVisible = true
        currentSwipeButton.isInvisible = true

        //Do not show countdown since claim has expired
        if (secondsBetween > claimCountDownSeconds) {
            showClaimExpired()
            return
        }

        //If a claim is active but claim page is the wrong shop, then hide the swipe view
        if (claim.shopId != args.shop.id && containerSwipe != null) {
            containerSwipe!!.animate().alpha(0f).start()
            return
        }

        //Show countdown
        startCountdown(claimCountDownSeconds, secondsBetween)
    }

    private fun slideUpSlider() {
        val slideUpAnimation = AnimationUtils.loadAnimation(requireContext(), res.anim.slide_in_bottom)
        slideUpAnimation.interpolator = DecelerateInterpolator()
        containerSlider.startAnimation(slideUpAnimation)
    }

    private fun startCountdown(claimCountDownSeconds: Int, secondsBetween: Long) {
        tvSwipeHint?.text = getString(res.string.claim_swipe_hint_during)
        progressBarClaim.isVisible = true

        if(countDownTimer != null){
            countDownTimer?.cancel()
        }

        countDownTimer = object : CountDownTimer(1000 * (claimCountDownSeconds - secondsBetween), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val durationSeconds = millisUntilFinished / 1000
                tvCountDown?.text = String.format(
                        "%02d:%02d:%02d", durationSeconds / 3600,
                        (durationSeconds % 3600) / 60, (durationSeconds % 60)
                )
            }

            override fun onFinish() {
                showClaimExpired()
            }
        }.start()
    }

    private fun showClaimExpired() {
        tvCountDown?.text = getString(res.string.claim_swipe_btn_after)
        tvSwipeHint?.text = getString(res.string.claim_swipe_hint_after)
        progressBarClaim?.isVisible = false
        tvBasicCountDownLabel?.isVisible = false
        tvPremiumCountDownLabel?.isVisible = false
    }

    override fun onDestroy() {
        super.onDestroy()
        requireActivity().title = null
    }

}
