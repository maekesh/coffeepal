package com.app.coffeeclub.home

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.app.coffeeclub.base.BaseFragment

abstract class BaseNavigationDrawerFragment : BaseHomeFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
    }

    private fun setupToolbar() {
        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity).supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(com.app.coffeeclub.resources.R.drawable.ic_burger_white)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                val requireActivity = requireActivity()
                if(requireActivity is HomeActivity){
                    requireActivity.openDrawer()
                    return true
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
