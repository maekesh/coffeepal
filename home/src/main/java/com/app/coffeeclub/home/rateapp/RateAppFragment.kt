package com.app.coffeeclub.home.rateapp

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.SeekBar
import android.widget.TextView
import com.app.coffeeclub.home.BaseNavigationDrawerFragment
import com.app.coffeeclub.home.HomeActivity
import com.app.coffeeclub.home.R
import com.app.coffeeclub.resources.R as res
import kotlinx.android.synthetic.main.fragment_rate_app.*
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.app.coffeeclub.home.HomeViewModel
import com.app.coffeeclub.utilities.helpers.MessageHelper
import dk.idealdev.utilities.response.Status

class RateAppFragment(override val layoutResId: Int = R.layout.fragment_rate_app) : BaseNavigationDrawerFragment() {

    private val homeViewModel: HomeViewModel by viewModelProvider()
    private val rateAppViewModel: RateAppViewModel by viewModelProvider()

    private lateinit var thumbView: View

    private val emojiis by lazy { resources.getStringArray(res.array.seekbar_emoji_list) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        thumbView = LayoutInflater.from(requireContext()).inflate(R.layout.layout_seekbar_thumb, null, false)

        setTitle(getString(res.string.menu_rate_app))
        tvBtnRate.setText(getString(res.string.rate_app_btn_rate))

        val appCompatActivity = requireActivity()

        //In case we resume from back button we need to update menu item state
        if (appCompatActivity is HomeActivity) {
            appCompatActivity.selectRateAppMenuItem()
        }

        initSeekbarSteps()
        initSeekbar()

        observeUser()
        observeHasRated()

        tvBtnRate.setOnButtonClickListener {
            onBtnRateClicked(view)
        }

        tvBtnRateAgain.setOnClickListener {
            rateAppViewModel.hasRatedLiveData.value = false
        }
    }

    private fun observeUser() {
        homeViewModel.getUserLiveData.observe(viewLifecycleOwner, Observer {
            val user = it.data ?: return@Observer
            if(user.rating != null && rateAppViewModel.ratingLiveData.value == null) {
                rateAppViewModel.ratingLiveData.value = user.rating.toInt()
                rateAppViewModel.hasRatedLiveData.value = true
            }
        })
    }

    private fun initSeekbarSteps() {
        for(index in 1..10) {
            val textView = LayoutInflater.from(requireContext()).inflate(R.layout.row_rate_step, null, false) as TextView
            val params = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.weight = 1f
            textView.text = index.toString()
            textView.layoutParams = params
            containerSeekbarSteps.addView(textView)
        }
    }

    private fun initSeekbar() {
        var initialState = true

        //Set pointer as initial state
        seekbar.thumb = getThumb(10)

        seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                //Reset in case you already has rated
                if(rateAppViewModel.hasRatedLiveData.value == true && fromUser) {
                    rateAppViewModel.hasRatedLiveData.value = false
                }
                //Offset progress by one first time when max is readjustet
                val currentProgress = if(initialState && progress == 6) {
                    progress - 1
                } else {
                    progress
                }

                if(initialState) {
                    handleInitialState(currentProgress)
                    initialState = false
                }

                seekbar.thumb = getThumb(currentProgress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
    }

    private fun handleInitialState(currentProgress: Int) {
        seekbar.max = 9
        seekbar.progress = currentProgress
        seekbar.progressDrawable = ContextCompat.getDrawable(requireContext(), res.drawable.seekbar_progress)
        (thumbView.findViewById(R.id.containerThumbBackground) as LinearLayout)
            .setBackgroundResource(res.drawable.seekbar_thumb_background)
        tvBtnRate.setButtonEnabled(true)
    }

    fun getThumb(progress: Int): Drawable {
        (thumbView.findViewById(R.id.tvProgress) as TextView).text = emojiis[progress]

        thumbView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        val bitmap =
            Bitmap.createBitmap(thumbView.measuredWidth, thumbView.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        thumbView.layout(0, 0, thumbView.measuredWidth, thumbView.measuredHeight)
        thumbView.draw(canvas)

        return BitmapDrawable(resources, bitmap)
    }

    private fun observeHasRated() {
        rateAppViewModel.hasRatedLiveData.observe(viewLifecycleOwner, Observer { hasRated ->
            vDimmer.isVisible = hasRated
            tvBtnRate.isVisible = !hasRated
            tvBtnRateAgain.isVisible = hasRated

            if(hasRated) {
                val rating = rateAppViewModel.ratingLiveData.value ?: return@Observer
                handleInitialState(rating - 1)
                seekbar.thumb = getThumb(rating - 1)
                tvHeadline.text = getString(res.string.rate_app_headline_has_rated)
                tvSubHeadline.text = getString(res.string.rate_app_sub_headline_has_rated)
                tvBtnRate.setText(getString(res.string.rate_app_btn_rate_has_rated))
            }
        })
    }

    private fun onBtnRateClicked(view: View) {
        val rating = seekbar.progress + 1
        rateAppViewModel.ratingLiveData.value = rating
        rateAppViewModel.hasRatedLiveData.value = true

        rateAppViewModel.putRatingLiveData.observe(viewLifecycleOwner, Observer {
            tvBtnRate.setProgressState(it.status == Status.LOADING)
            when(it.status) {
                Status.SUCCESS -> {
                    findNavController().navigate(RateAppFragmentDirections.actionRateAppFragmentToCommentRatingFragment(rating))
                    MessageHelper.showSuccessMessageSnackbar(view,
                        getString(res.string.comment_rating_rating_submitted_message),
                        windowInsetsViewModel.windowInsetTop.value)
                }
                Status.LOADING -> { }
                Status.ERROR -> {
                    MessageHelper.showErrorMessageSnackbar(view,
                        topMargin = windowInsetsViewModel.windowInsetTop.value)
                }
            }
        })

        rateAppViewModel.submitRating()
    }
}