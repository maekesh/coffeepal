package com.app.coffeeclub.home.profile.deleteprofile

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.app.coffeeclub.base.BaseFragment
import com.app.coffeeclub.home.BaseHomeFragment
import com.app.coffeeclub.home.R
import com.app.coffeeclub.resources.R as res
import com.app.coffeeclub.home.profile.ProfileViewModel
import com.app.coffeeclub.utilities.extensions.validate
import com.app.coffeeclub.utilities.helpers.AppResetHelper
import com.app.coffeeclub.utilities.helpers.CustomTabHelper
import com.app.coffeeclub.utilities.helpers.MessageHelper
import com.app.coffeeclub.utilities.helpers.ValidationHelper
import dk.idealdev.utilities.afterTextChanged
import dk.idealdev.utilities.response.Status
import kotlinx.android.synthetic.main.fragment_delete_profile.*
import kotlinx.android.synthetic.main.fragment_delete_profile.etPassword
import kotlinx.android.synthetic.main.fragment_delete_profile.tvError
import kotlinx.android.synthetic.main.fragment_edit_email.*

class DeleteProfileFragment(override val layoutResId: Int = R.layout.fragment_delete_profile) : BaseHomeFragment() {

    private val profileViewModel: ProfileViewModel by viewModelProvider()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle(getString(res.string.profile_delete_profile_title))
        showBackAsWhiteArrow()

        profileViewModel.deleteUserLiveData.observe(viewLifecycleOwner, Observer {
            tvBtnDeleteProfile.isEnabled = it.status != Status.LOADING
            pbDeteleProfile.isVisible = it.status == Status.LOADING

            when(it.status){
                Status.SUCCESS -> {
                    AppResetHelper.resetApplication(requireContext())
                }
                Status.LOADING -> {}
                Status.ERROR -> {
                    MessageHelper.showErrorMessageSnackbar(view,
                        topMargin = windowInsetsViewModel.windowInsetTop.value)
                }
            }
        })

        etPassword.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            validatePasswordLength(hasFocus)
        }

        etPassword.afterTextChanged {
            if(it.isNotBlank()) {
                tvBtnDeleteProfile.isEnabled = ValidationHelper.isPasswordValid(requireContext(), etPassword.text.toString())
            }
        }

        tvBtnReadConditions.setOnClickListener {
            CustomTabHelper.openCustomTab(requireContext(), getString(res.string.url_terms_and_conditions))
        }

        tvBtnDeleteProfile.setOnClickListener {
            if(validatePasswordLength(isNotFinalCheck = false)) {
                profileViewModel.deleteUser(etPassword.text.toString().trim())
            }
        }
    }

    private fun validatePasswordLength(hasFocus: Boolean = false, isNotFinalCheck: Boolean = true): Boolean {
        return etPassword.validate(
                { password -> ValidationHelper.isPasswordValid(requireContext(), password)},
                getString(res.string.profile_delete_profile_validation_length_error_message),
                tvError = tvError,
                hasFocus = hasFocus,
                isNotFinalCheck = isNotFinalCheck)
    }
}