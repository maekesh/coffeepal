package com.app.coffeeclub.home.profile.email

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.app.coffeeclub.home.HomeViewModel
import com.app.coffeeclub.home.R
import com.app.coffeeclub.resources.R as res
import com.app.coffeeclub.home.profile.BaseEditProfileFragment
import com.app.coffeeclub.utilities.extensions.validate
import com.app.coffeeclub.utilities.helpers.MessageHelper
import com.app.coffeeclub.utilities.helpers.ValidationHelper
import dk.idealdev.utilities.afterTextChanged
import dk.idealdev.utilities.response.Status
import kotlinx.android.synthetic.main.fragment_edit_email.*
import retrofit2.HttpException
import java.net.HttpURLConnection

class EditEmailFragment(override val layoutResId: Int = R.layout.fragment_edit_email) : BaseEditProfileFragment() {
    private val homeViewModel: HomeViewModel by viewModelProvider()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle(getString(res.string.profile_edit_email_title))
        showBackAsWhiteArrow()

        profileViewModel.emailLiveData.observe(viewLifecycleOwner, Observer {
            if(etEmail.text.isBlank()) {
                etEmail.setText(it)
            }
        })


        etEmail.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if(etEmail.text.isNotBlank()) {
                validateEmail(hasFocus)
            }
        }

        etEmail.afterTextChanged { updateSaveButtonEnabledState() }

        etPassword.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if(etPassword.text.isNotBlank()) {
                validatePasswordLength(hasFocus)
            }
        }

        etPassword.afterTextChanged { updateSaveButtonEnabledState() }

        tvSaveChanges.setOnButtonClickListener {
            if(isValidated()) {
                saveChanges(view)
            }
        }
    }

    private fun isValidated() : Boolean {
        return validateEmail(isNotFinalCheck = false) && validatePasswordLength(isNotFinalCheck = false)
    }

    private fun updateSaveButtonEnabledState(){
        val emailValid = ValidationHelper.isEmailValid(etEmail.text.toString())
        val passwordValid = ValidationHelper.isPasswordValid(requireContext(), etPassword.text.toString())
        tvSaveChanges.setButtonEnabled(emailValid && passwordValid)
    }

    private fun saveChanges(view: View) {
        val enteredEmail = etEmail.text.toString().trim()
        val password = etPassword.text.toString().trim()

        observeUpdateUser(view)
        profileViewModel.emailLiveData.value = enteredEmail
        profileViewModel.updateEmail(password, enteredEmail)
    }

    private fun observeUpdateUser(view: View) {
        profileViewModel.updateEmailLiveData.observe(viewLifecycleOwner, Observer {
            tvSaveChanges.setProgressState(it.status == Status.LOADING)

            when (it.status) {
                Status.SUCCESS -> {
                    homeViewModel.getUser(true)
                    MessageHelper.showSuccessMessageSnackbar(view,
                        getString(res.string.profile_edit_email_saved),
                        windowInsetsViewModel.windowInsetTop.value)
                }
                Status.ERROR -> {
                    val throwable = it.throwable
                    if (throwable is HttpException && throwable.code() == HttpURLConnection.HTTP_CONFLICT) {
                        MessageHelper.showErrorMessageSnackbar(view,
                            getString(res.string.profile_edit_email_error_409_existing_email),
                            windowInsetsViewModel.windowInsetTop.value)
                    } else {
                        MessageHelper.showErrorMessageSnackbar(view,
                            topMargin = windowInsetsViewModel.windowInsetTop.value)
                    }
                }
                Status.LOADING -> {
                }
            }
        })
    }

    private fun validatePasswordLength(hasFocus: Boolean = false, isNotFinalCheck: Boolean = true): Boolean {
        return etPassword.validate(
                { password -> ValidationHelper.isPasswordValid(requireContext(), password)},
                getString(res.string.signup_password_validation_length_error_message),
                tvError = tvError,
                hasFocus = hasFocus,
                isNotFinalCheck = isNotFinalCheck)
    }

    private fun validateEmail(hasFocus: Boolean = false, isNotFinalCheck: Boolean = true): Boolean {
        return etEmail.validate(
                { email -> ValidationHelper.isEmailValid(email) },
                getString(res.string.profile_edit_email_invalid),
                tvError = tvError,
                hasFocus = hasFocus,
                isNotFinalCheck = isNotFinalCheck)
    }

}