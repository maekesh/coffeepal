package com.app.coffeeclub.home.claim

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.coffeeclub.models.Claim
import com.app.coffeeclub.models.ClaimDto
import com.app.coffeeclub.models.ClaimsResponse
import com.app.coffeeclub.network.repository.ClaimsRepository
import dk.idealdev.utilities.livedata.SingleLiveEvent
import dk.idealdev.utilities.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.math.BigDecimal

class ClaimViewModel: ViewModel() {

    val postClaimLiveData: MutableLiveData<Resource<Claim>> = SingleLiveEvent()
    val deleteClaimLiveData: MutableLiveData<Resource<Void>> = SingleLiveEvent()
    val claimsLiveData: MutableLiveData<Resource<ClaimsResponse>> = SingleLiveEvent()

    fun postClaim(claimDto: ClaimDto) {
        viewModelScope.launch(Dispatchers.IO) {
            ClaimsRepository.claimsPost(postClaimLiveData, claimDto)
        }
    }

    fun getClaims(shopId : BigDecimal){
        viewModelScope.launch(Dispatchers.IO) {
            ClaimsRepository.claimsGet(claimsLiveData, shopId, BigDecimal.valueOf(1), BigDecimal.valueOf(10))
        }
    }

    fun deleteClaim(claimId : String){
        viewModelScope.launch(Dispatchers.IO) {
            ClaimsRepository.claimsIdDelete(deleteClaimLiveData, claimId)
        }
    }
}