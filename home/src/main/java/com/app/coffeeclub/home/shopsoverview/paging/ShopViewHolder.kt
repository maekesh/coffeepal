package com.app.coffeeclub.home.shopsoverview.paging

import android.location.Location
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.app.coffeeclub.home.R
import com.app.coffeeclub.home.shopsoverview.ShopItemCallback
import com.app.coffeeclub.models.Shop
import com.app.coffeeclub.network.glide.GlideApp
import com.app.coffeeclub.utilities.helpers.FormatHelper
import com.app.coffeeclub.utilities.helpers.ImageHelper
import kotlinx.android.synthetic.main.row_shop.view.*
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.resources.R.color
import com.app.coffeeclub.resources.R.array
import com.app.coffeeclub.utilities.extensions.getOpensOrClosesAtString
import com.app.coffeeclub.utilities.extensions.isOpen
import com.app.coffeeclub.utilities.helpers.LocationHelper
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class ShopViewHolder(itemView: View, private val callback: ShopItemCallback) :
    RecyclerView.ViewHolder(itemView) {

    private val placeholders = itemView.context.resources.obtainTypedArray(array.coffeshops_placeholders)

    fun bind(item: Shop?, userLocation: Location?, position: Int) {
        if (item == null) { //Placeholder
            itemView.tvShopName.text = itemView.context.getString(string.general_loading)
            return
        }

        with(itemView) {
            tvShopName.text = item.name
            tvOpenClosedStatus.text = context.getString(if(item.isOpen()) string.shopsoverview_open else string.shopsoverview_closed)
            tvOpenClosedStatus.setTextColor(ContextCompat.getColor(context, if(item.isOpen()) color.gray100 else color.red))
            val opensOrClosesString = item.getOpensOrClosesAtString(context)
            tvOpenClosingTime.text = opensOrClosesString
            vTextDivider.isVisible = opensOrClosesString.isNotBlank()

            tvDistanceFromUser.isVisible = userLocation != null && item.latitude != null && item.longitude != null

            if (userLocation != null && item.latitude != null && item.longitude != null) {
                val distanceInMeters = LocationHelper.getDistanceToShop(item, userLocation)

                tvDistanceFromUser.text = FormatHelper.formatDistance(distanceInMeters, context)
            }

            GlideApp.with(this)
                    .load(ImageHelper.getCoverUrl(item.images))
                    .placeholder(placeholders.getResourceId(position % 8, -1))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(ivShopImage)


            ViewCompat.setTransitionName(
                this.ivShopImage,
                context.getString(R.string.transition_name_image, item.id.toString())
            )

            ViewCompat.setTransitionName(
                this.vGradient,
                context.getString(R.string.transition_name_gradient, item.id.toString())
            )


            ViewCompat.setTransitionName(
                this.vSolid,
                context.getString(R.string.transition_name_solid, item.id.toString())
            )

            ViewCompat.setTransitionName(
                this.tvOpenClosedStatus,
                context.getString(R.string.transition_name_open_closed_status, item.id.toString())
            )

            ViewCompat.setTransitionName(
                this.vTextDivider,
                context.getString(R.string.transition_name_open_closed_divider, item.id.toString())
            )

            ViewCompat.setTransitionName(
                this.tvOpenClosingTime,
                context.getString(R.string.transition_name_open_closed_time, item.id.toString())
            )

            ViewCompat.setTransitionName(
                this.tvShopName,
                context.getString(R.string.transition_name_shop_name, item.id.toString())
            )

            ViewCompat.setTransitionName(
                this.tvDistanceFromUser,
                context.getString(R.string.transition_name_distance, item.id.toString())
            )

            this.setOnClickListener {
                callback.onShopItemClicked(
                        item,
                        position,
                        this.ivShopImage,
                        this.vGradient,
                        this.vSolid,
                        this.tvShopName,
                        this.tvOpenClosedStatus,
                        this.vTextDivider,
                        this.tvOpenClosingTime,
                        this.tvDistanceFromUser,
                        false
                )
            }
        }
    }
}
