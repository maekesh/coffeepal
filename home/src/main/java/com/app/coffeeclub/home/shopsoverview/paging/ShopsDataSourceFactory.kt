package com.app.coffeeclub.home.shopsoverview.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.app.coffeeclub.models.Shop

class ShopsDataSourceFactory : DataSource.Factory<Int, Shop>() {
    var queryFilter : String? = null
    val shopDataSourceLiveData = MutableLiveData<ShopsDataSource>()
    lateinit var shopsDataSource : ShopsDataSource

    override fun create(): DataSource<Int, Shop> {
        shopsDataSource = ShopsDataSource()
        shopsDataSource.queryFilter = queryFilter
        shopDataSourceLiveData.postValue(shopsDataSource)
        return shopsDataSource
    }
}