package com.app.coffeeclub.home.shopsoverview.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.app.coffeeclub.models.Shop
import com.app.coffeeclub.models.ShopsResponse
import com.app.coffeeclub.models.custom.ShopAdCard
import com.app.coffeeclub.network.api.ShopsApi
import com.app.coffeeclub.network.repository.ShopsRepository
import com.app.coffeeclub.utilities.helpers.SharedPreferencesHelper
import dk.idealdev.utilities.response.Resource
import dk.idealdev.utilities.response.Status
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.math.BigDecimal

class ShopsDataSource(var queryFilter: String? = null) : PageKeyedDataSource<Int, Shop>() {
    var state: MutableLiveData<PagingState> = MutableLiveData()
    var totalCountLiveData: MutableLiveData<Int> = MutableLiveData()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Shop>) {
        GlobalScope.launch(Dispatchers.IO) {

            val result = loadData(params.requestedLoadSize, 1)

            handleLoadResult(result) { groupedListResult ->
                if (groupedListResult != null) {
                    val items = groupedListResult.items
                    var totalCount = groupedListResult.totalItems.toInt()

                    if(SharedPreferencesHelper.loadShowCardSubscriptionAd()) {
                        if(items.isNotEmpty()) {
                            items.add(1, ShopAdCard())
                            totalCount++
                        } else{
                            items.add(ShopAdCard())
                            totalCount++
                        }
                    }

                    totalCountLiveData.postValue(totalCount)
                    callback.onResult(items, 0, totalCount,  null, 2)
                } else {
                    updateState(PagingState.ERROR)
                }
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Shop>) {
        GlobalScope.launch(Dispatchers.IO) {

            val result = loadData(params.requestedLoadSize, params.key)

            handleLoadResult(result) { groupedListResult ->
                if (groupedListResult != null) {
                    callback.onResult(groupedListResult.items, params.key + 1)
                } else {
                    updateState(PagingState.ERROR)
                }
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Shop>) {
        //Not used
    }

    private suspend fun loadData(loadSize: Int, pageNumber: Int): Resource<ShopsResponse> {
        updateState(if (pageNumber == 1) PagingState.LOADING_INITIAL else PagingState.LOADING_PAGE)

        val location = SharedPreferencesHelper.loadUserLocation()

        return ShopsRepository.safeApiResult {
            if(location != null){
                ShopsApi.Factory.getApi().shopsGet(null, BigDecimal.valueOf(location.longitude), BigDecimal.valueOf(location.latitude), queryFilter, loadSize.toBigDecimal(), pageNumber.toBigDecimal()).await()
            } else{
                ShopsApi.Factory.getApi().shopsGet(null, null, null, queryFilter, loadSize.toBigDecimal(), pageNumber.toBigDecimal()).await()
            }
        }
    }

    private fun handleLoadResult(
        result: Resource<ShopsResponse>,
        callback: (groupedListResult: ShopsResponse?) -> Unit
    ) {
        when (result.status) {
            Status.SUCCESS -> {
                callback.invoke(result.data)
                updateState(PagingState.SUCCESS)
            }
            Status.ERROR -> {
                updateState(PagingState.ERROR)
            }
            else -> {
            }
        }
    }

    private fun updateState(state: PagingState) {
        this.state.postValue(state)
    }
}