package com.app.coffeeclub.home.rateapp.comment

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.navigation.fragment.navArgs
import com.app.coffeeclub.base.BaseFragment
import com.app.coffeeclub.home.R
import com.app.coffeeclub.resources.R as res
import com.app.coffeeclub.home.rateapp.RateAppViewModel
import com.app.coffeeclub.home.rateapp.Rating
import dk.idealdev.utilities.afterTextChanged
import kotlinx.android.synthetic.main.fragment_comment_rating.*
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.app.coffeeclub.home.BaseHomeFragment
import com.app.coffeeclub.utilities.helpers.MessageHelper
import dk.idealdev.utilities.response.Status

class CommentRatingFragment(override val layoutResId: Int = R.layout.fragment_comment_rating) : BaseHomeFragment() {

    private val rateAppViewModel: RateAppViewModel by viewModelProvider()

    private val args: CommentRatingFragmentArgs by navArgs()

    companion object {
        private const val RC_PLAY_STORE = 22
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showBackAsWhiteArrow()
        setTitle(getString(if(isPromoter()) {
            res.string.comment_rating_title_promoter_android
        } else {
            res.string.comment_rating_title_detractor_or_passive
        }))

        initHeadline()
        initSubHeadline()
        initCommentField()
        initSubmitBtn()

        tvBtnSubmit.setOnClickListener {
            onBtnSubmitClicked()
        }

        tvBtnSkip.setOnClickListener {
            submitRatingAndComment(true)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == RC_PLAY_STORE) {
            findNavController().popBackStack()
        }
    }

    private fun showSubmittedSnackbar() {
        view?.let {
            MessageHelper.showSuccessMessageSnackbar(it,
                getString(res.string.comment_rating_rating_submitted_message),
                windowInsetsViewModel.windowInsetTop.value)
        }
    }

    private fun initHeadline() {
        tvHeadline.text = getString(if(isDetractor()) {
            res.string.comment_rating_headline_detractor
        } else {
            res.string.comment_rating_headline_passive_or_promoter
        })
    }

    private fun initSubHeadline() {
        tvSubHeadline.text = getString(when {
            isDetractor() -> {
                res.string.comment_rating_sub_headline_detractor
            }
            isPromoter() -> {
                res.string.comment_rating_sub_headline_promoter_android
            }
            else -> {
                res.string.comment_rating_sub_headline_passive
            }
        })
    }

    private fun initCommentField() {
        val shouldBeVisible = isDetractor() || isPassive()

        etCommentOnRating.isVisible = shouldBeVisible

        if(shouldBeVisible) {
            etCommentOnRating.afterTextChanged {
                rateAppViewModel.commentLiveData.value = it
                tvBtnSubmit.isEnabled = it.isNotBlank()
            }
        } else {
            tvBtnSubmit.isEnabled = true
        }
    }

    private fun initSubmitBtn() {
        tvBtnSubmit.text = getString(if(isPromoter()) {
            res.string.comment_rating_btn_submit_promoter_android
        } else {
            res.string.comment_rating_btn_submit_detractor_or_passive
        })
    }

    private fun onBtnSubmitClicked() {
        if(isPromoter()) {
            requireActivity().onBackPressed()
            launchPlayStore()
        } else {
            submitRatingAndComment()
        }
    }

    private fun launchPlayStore() {
        val packageName = requireContext().packageName
        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName"))
            startActivityForResult(intent, RC_PLAY_STORE)
        } catch (activityNotFoundException: android.content.ActivityNotFoundException) {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://play.google.com/store/apps/details?id=$packageName")
            )
            startActivityForResult(intent, RC_PLAY_STORE)
        }

    }

    private fun submitRatingAndComment(shouldSkip: Boolean = false) {
        if(shouldSkip || etCommentOnRating.text.toString().isBlank()) {
            findNavController().popBackStack()
        } else {
            observeSubmitRating()
            rateAppViewModel.submitRatingAndComment()
        }
    }

    private fun observeSubmitRating() {
        rateAppViewModel.putRatingAndCommentLiveData.observe(viewLifecycleOwner, Observer {
            when(it.status) {
                Status.SUCCESS -> {
                    showSubmittedSnackbar()
                    findNavController().popBackStack()
                }
                Status.LOADING -> { }
                Status.ERROR -> { }
            }
        })
    }

    private fun isPromoter() : Boolean {
        return args.rating > Rating.Passive.minValue
    }

    private fun isPassive() : Boolean {
        return  args.rating <= Rating.Passive.minValue &&
                args.rating > Rating.Detractor.minValue
    }

    private fun isDetractor() : Boolean {
        return  args.rating <= Rating.Detractor.minValue
    }

}