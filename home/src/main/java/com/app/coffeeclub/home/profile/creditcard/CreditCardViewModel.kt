package com.app.coffeeclub.home.profile.creditcard

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.coffeeclub.models.PaymentSourceDto
import com.app.coffeeclub.network.repository.UsersRepository
import com.stripe.android.model.Card
import dk.idealdev.utilities.livedata.SingleLiveEvent
import dk.idealdev.utilities.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CreditCardViewModel : ViewModel() {

    val newCardNumberLiveData: MutableLiveData<String> = MutableLiveData()
    val newExpirationDateLiveData: MutableLiveData<String> = MutableLiveData()
    val newCvvCodeLiveData: MutableLiveData<String> = MutableLiveData()
    var newCardInfo: Card? = null

    val uploadPaymentTokenLiveData : MutableLiveData<Resource<Any>> = SingleLiveEvent()

    fun uploadPaymentToken(token : String){
        val postData = PaymentSourceDto().apply {
            sourceToken = token
        }

        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersMePaymentSourcePost(uploadPaymentTokenLiveData, postData)
        }
    }
}