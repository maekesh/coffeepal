package com.app.coffeeclub.home.shopsoverview.map

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.app.coffeeclub.home.R
import com.app.coffeeclub.home.shopsoverview.ShopItemCallback
import com.app.coffeeclub.models.User
import com.app.coffeeclub.utilities.helpers.SegmentHelper
import com.app.coffeeclub.utilities.helpers.SegmentHelper.Segment.*
import dk.idealdev.utilities.response.Resource
import kotlinx.android.synthetic.main.row_map_subscription_ad.view.ivCardImage
import kotlinx.android.synthetic.main.row_map_subscription_ad.view.tvCardButton
import kotlinx.android.synthetic.main.row_map_subscription_ad.view.tvCardDescription
import kotlinx.android.synthetic.main.row_map_subscription_ad.view.tvCardTitle
import com.app.coffeeclub.resources.R as res

class AdCardMapViewHolder(view: View,
                          private val callback: ShopItemCallback,
                          private val userLiveData: MutableLiveData<Resource<User>>) : RecyclerView.ViewHolder(view) {

    fun bind() {
        itemView.setOnClickListener {
            callback.onSubscriptionAdClicked()
        }

        val segment = SegmentHelper.getSegment(userLiveData.value?.data)
        val context = itemView.context

        itemView.tvCardTitle.text = context.getString(when(segment) {
            VISITING -> res.string.shopsoverview_card_subscription_title_visiting
            TRIALING -> res.string.shopsoverview_card_subscription_title_trialing
            CHURNED -> res.string.shopsoverview_card_subscription_title_churned
            else -> -1
        })

        itemView.tvCardDescription.text = context.getString(when(segment) {
            VISITING -> res.string.shopsoverview_card_subscription_description_visiting
            TRIALING -> res.string.shopsoverview_card_subscription_description_trialing
            CHURNED -> res.string.shopsoverview_card_subscription_description_churned
            else -> -1
        })

        itemView.tvCardButton.text = context.getString(when(segment) {
            VISITING -> res.string.shopsoverview_card_subscription_button_visiting
            TRIALING -> res.string.shopsoverview_card_subscription_button_trialing
            CHURNED -> res.string.shopsoverview_card_subscription_button_churned
            else -> -1
        })

        itemView.ivCardImage.setImageResource(when(segment) {
            VISITING -> res.drawable.coffee_cup
            TRIALING -> res.drawable.beans
            CHURNED -> res.drawable.muffin
            else -> -1
        })
    }

    companion object {
        fun create(parent: ViewGroup, callback: ShopItemCallback, userLiveData: MutableLiveData<Resource<User>>): AdCardMapViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_map_subscription_ad, parent, false)

            return AdCardMapViewHolder(view, callback, userLiveData)
        }
    }
}