package com.app.coffeeclub.home.profile.subscription

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.coffeeclub.models.PlanDto
import com.app.coffeeclub.models.StripePlan
import com.app.coffeeclub.models.StripeProduct
import com.app.coffeeclub.models.StripeSubscription
import com.app.coffeeclub.network.repository.SubscriptionsRepository
import dk.idealdev.utilities.livedata.SingleLiveEvent
import dk.idealdev.utilities.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SubscriptionViewModel : ViewModel() {

    val myProductLiveData: MutableLiveData<StripeProduct> = MutableLiveData()
    val getAvailableProductsLiveData: MutableLiveData<Resource<List<StripeProduct>>> = MutableLiveData()
    val getSubscriptionMeLiveData: MutableLiveData<Resource<StripeSubscription>> = MutableLiveData()
    val deleteSubscriptionLiveData: MutableLiveData<Resource<Void>> = SingleLiveEvent()
    val postSubscriptionLiveData: MutableLiveData<Resource<Void>> = SingleLiveEvent()

    fun fetchSubscriptionProducts(forceRefresh : Boolean = false){
        viewModelScope.launch(Dispatchers.IO) {
            SubscriptionsRepository.usersMeSubscriptionAvailableGet(getAvailableProductsLiveData, forceRefresh)
        }
    }

    fun fetchMySubscription(forceRefresh : Boolean = false){
        viewModelScope.launch(Dispatchers.IO) {
            SubscriptionsRepository.usersMeSubscriptionGet(getSubscriptionMeLiveData, forceRefresh)
        }
    }

    fun cancelSubscriptionPlan() {
        viewModelScope.launch(Dispatchers.IO) {
            SubscriptionsRepository.usersMeSubscriptionDelete(deleteSubscriptionLiveData)
        }
    }

    fun changeSubscriptionPlan(selectedStripePlan: StripePlan){
        val postData = PlanDto().apply{
            planId = selectedStripePlan.id
        }
        viewModelScope.launch(Dispatchers.IO) {
            SubscriptionsRepository.changeSubscriptionPlan(postSubscriptionLiveData, postData)
        }
    }
}