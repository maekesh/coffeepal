package com.app.coffeeclub.home.claim

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.transition.Transition
import androidx.transition.TransitionInflater
import com.app.coffeeclub.home.R
import kotlinx.android.synthetic.main.fragment_claim.*

fun ClaimFragment.setupTransitions(
        args: ClaimFragmentArgs,
        cover: ImageView?,
        gradient: View?,
        solid: View?,
        openClosedText: TextView?,
        openClosedDivider: View?,
        openClosedTime: TextView?,
        titleToolbar: Toolbar?,
        titleTextView: TextView?,
        distance: TextView?
) {
    cover ?: return
    gradient ?: return
    solid ?: return
    openClosedText ?: return
    openClosedDivider ?: return
    openClosedTime ?: return
    distance ?: return
    titleTextView ?: return

    //We need to set title before any children appear in toolbar
    (activity as AppCompatActivity).setSupportActionBar(titleToolbar)
    titleToolbar?.title = args.shop.name

    when {
        args.isFromMap -> {
            cover.transitionName = getString(R.string.transition_name_map_image, args.shop.id.toString())
            solid.transitionName = getString(R.string.transition_name_map_solid, args.shop.id.toString())
            openClosedText.transitionName =
                    getString(R.string.transition_name_map_open_closed_status, args.shop.id.toString())
            openClosedDivider.transitionName =
                    getString(R.string.transition_name_map_open_closed_divider, args.shop.id.toString())
            openClosedTime.transitionName =
                    getString(R.string.transition_name_map_open_closed_time, args.shop.id.toString())
            distance.transitionName = getString(R.string.transition_name_map_distance, args.shop.id.toString())
            titleTextView.transitionName = getString(R.string.transition_name_map_shop_name, args.shop.id.toString())
        }
        args.isFromList -> {
            cover.transitionName = getString(R.string.transition_name_image, args.shop.id.toString())
            gradient.transitionName = getString(R.string.transition_name_gradient, args.shop.id.toString())
            solid.transitionName = getString(R.string.transition_name_solid, args.shop.id.toString())
            openClosedText.transitionName =
                    getString(R.string.transition_name_open_closed_status, args.shop.id.toString())
            openClosedDivider.transitionName =
                    getString(R.string.transition_name_open_closed_divider, args.shop.id.toString())
            openClosedTime.transitionName =
                    getString(R.string.transition_name_open_closed_time, args.shop.id.toString())
            distance.transitionName = getString(R.string.transition_name_distance, args.shop.id.toString())
            titleTextView.transitionName = getString(R.string.transition_name_shop_name, args.shop.id.toString())
        }
        else -> {
            titleTextView.isVisible = false
            titleToolbar?.isVisible = true
        }
    }

    val enterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    enterTransition.addListener(object : Transition.TransitionListener {
        override fun onTransitionEnd(transition: Transition) {
            viewPagerImages.isVisible = true
            titleTextView.post {
                titleTextView.transitionName = ""
                titleTextView.isVisible = false
            }

            titleToolbar?.isVisible = true
            titleToolbar?.getChildAt(0)?.transitionName =
                    getString(R.string.transition_name_shop_name, args.shop.id.toString())
        }

        override fun onTransitionResume(transition: Transition) {

        }

        override fun onTransitionPause(transition: Transition) {

        }

        override fun onTransitionCancel(transition: Transition) {

        }

        override fun onTransitionStart(transition: Transition) {

        }
    })
    sharedElementEnterTransition = enterTransition

    val exitTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    sharedElementReturnTransition = exitTransition
}