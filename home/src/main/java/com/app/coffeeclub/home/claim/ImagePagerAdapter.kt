package com.app.coffeeclub.home.claim

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.app.coffeeclub.home.R
import com.app.coffeeclub.resources.R as res


import com.app.coffeeclub.network.glide.GlideApp
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.row_image.view.*

class ImagePagerAdapter(val context: Context, private val bodyImages: List<String>, private val shopsAdapterPosition: Int) : PagerAdapter() {

    private var inflater: LayoutInflater = LayoutInflater.from(context)
    private val placeholders = context.resources.obtainTypedArray(res.array.coffeshops_placeholders)

            override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = inflater.inflate(R.layout.row_image, container,false)

        loadImage(view.ivShopImage, bodyImages[position], position)
        container.addView(view)

        return view
    }


    private fun loadImage(imageView: ImageView, imageSet: String, position: Int) {
        val placeholderPosition = shopsAdapterPosition + position

        GlideApp.with(context)
                .load(imageSet)
                .placeholder(placeholders.getResourceId(placeholderPosition % 8, -1))
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageView)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return `object` == view
    }

    override fun getCount(): Int {
        return bodyImages.size
    }
}

interface OnImageViewerDismissListener {
    fun onImageChange(position: Int)
}