package com.app.coffeeclub.home.profile

import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.app.coffeeclub.home.BaseNavigationDrawerFragment
import com.app.coffeeclub.home.HomeActivity
import com.app.coffeeclub.home.HomeViewModel
import com.app.coffeeclub.home.R

import com.app.coffeeclub.models.User
import com.app.coffeeclub.models.UserDto
import com.app.coffeeclub.uicomponents.dialogs.showCoffeeClubDialog
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.uicomponents.forms.adapters.ActionFormAdapter
import com.app.coffeeclub.uicomponents.forms.adapters.FormAdapter
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.showText
import com.app.coffeeclub.utilities.helpers.*
import com.app.coffeeclub.utilities.viewmodels.GenderSelectionViewModel
import com.google.android.gms.common.wrappers.InstantApps
import com.google.android.gms.instantapps.InstantApps as Instant
import dk.idealdev.utilities.KeyboardUtil
import dk.idealdev.utilities.response.Status
import kotlinx.android.synthetic.main.appbar_profile.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.row_list_footer.*
import java.util.*

class ProfileFragment(override val layoutResId: Int = R.layout.fragment_profile) : BaseNavigationDrawerFragment() {

    private val profileViewModel: ProfileViewModel by viewModelProvider()
    private val homeViewModel: HomeViewModel by viewModelProvider()
    private val genderViewModel: GenderSelectionViewModel by viewModelProvider()

    private var personalInformationAdapter: FormAdapter? = null
    private var addressInformationAdpater: FormAdapter? = null
    private var loginDetailAdapter: ActionFormAdapter? = null
    private var membershipAdapter: ActionFormAdapter? = null
    private var miscAdapter: ActionFormAdapter? = null

    companion object {
        private const val RC_INSTALL_APP = 7
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        super.onViewCreated(view, savedInstanceState)
        toolbarTitleProfile.showText(getString(string.profile_overview_title), Language.getLanguageHashMap())

        initViews()

        observeUser()
        observeGender()

        initAppVersion()

        initAdapters()
        initRecyclerViews()
        populateAdapters()

        setupBackBehavior()

        setupSaveButton(view)
    }

    private fun initViews() {
        tvMembershipTitle.showText(getString(string.profile_overview_sub_label_1), Language.getLanguageHashMap())
        tvPersonalInformationTitle.showText(getString(string.profile_overview_personal_title), Language.getLanguageHashMap())
        tvAddressInformation.showText(getString(string.profile_address_title), Language.getLanguageHashMap())
        tvLoginDetailsTitle.showText(getString(string.profile_sign_in_title), Language.getLanguageHashMap())
    }

    override fun onTopInsetChanged(inset: Int) {
        super.onTopInsetChanged(inset)
        InsetHelper.addTopPadding(toolbar, inset)
    }

    override fun onBottomInsetChanged(inset: Int) {
        super.onBottomInsetChanged(inset)
        InsetHelper.addBottomPadding(nestedScrollViewProfile, inset)
    }

    private fun observeGender() {
        genderViewModel.selectedGender.observe(viewLifecycleOwner, Observer {
            profileViewModel.sexLiveData.value = it
            personalInformationAdapter?.notifyItemChanged(3)
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            if (profileViewModel.hasSaveableChanges) {
                showCoffeeClubDialog(requireContext())
                    .setTitle(string.profile_overview_discard_changes_title)
                    .setMessage(string.profile_overview_discard_changes_message)
                    .positiveButton(string.profile_overview_discard_changes_confirm) {
                        onDiscardChangesFromDrawer()
                    }
                    .negativeButton(string.profile_overview_discard_changes_cancel)
                return true
            }
            return super.onOptionsItemSelected(item)
        } else {
            return super.onOptionsItemSelected(item)
        }
    }

    private fun onDiscardChangesFromDrawer() {
        homeViewModel.getUserLiveData.value?.data?.let { user ->
            setUnsavableProfileInfoFrom(user)
            setDataChangedState(false)
            populatePersonalInformationAdapter()
        }

        (requireActivity() as HomeActivity).openDrawer()
    }

    private fun setupSaveButton(view: View) {
        profileViewModel.updateUserLiveData.observe(viewLifecycleOwner, Observer {
            pbProfileSave.isVisible = it.status == Status.LOADING
            tvBtnSave.isVisible = it.status != Status.LOADING

            when (it.status) {
                Status.SUCCESS -> {
                    homeViewModel.getUser(true)
                    MessageHelper.showSuccessMessageSnackbar(
                        rvPersonalInformation,
                        getString(string.profile_overview_changes_saved),
                        windowInsetsViewModel.windowInsetTop.value
                    )
                    setDataChangedState(false)
                }
                Status.ERROR -> {
                    MessageHelper.showErrorMessageSnackbar(view,
                        topMargin = windowInsetsViewModel.windowInsetTop.value)
                }
                Status.LOADING -> {
                }
            }
        })

        tvBtnSave.setOnClickListener {
            KeyboardUtil.hideKeyboard(requireActivity())
            requireActivity().window.decorView.clearFocus()
            val firstName = profileViewModel.firstNameLiveData.value?.trim()
            val lastName = profileViewModel.lastNameLiveData.value?.trim()

            if(firstName.isNullOrBlank() || lastName.isNullOrBlank()){
                MessageHelper.showErrorMessageSnackbar(view,
                    getString(string.signup_info_missing_fields),
                    windowInsetsViewModel.windowInsetTop.value)
                return@setOnClickListener
            }

            //TODO make sure inputs are valid
            profileViewModel.updateUser()
        }
    }

    private fun setupBackBehavior() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            if (profileViewModel.hasSaveableChanges) {
                showCoffeeClubDialog(requireContext())
                    .setTitle(string.profile_overview_discard_changes_title)
                    .setMessage(string.profile_overview_discard_changes_message)
                    .positiveButton(string.profile_overview_discard_changes_confirm) {
                        setDataChangedState(false)
                        findNavController().popBackStack()
                    }
                    .negativeButton(string.profile_overview_discard_changes_cancel)
            } else {
                findNavController().popBackStack()
            }
        }
    }

    private fun observeUser() {
        homeViewModel.getUserLiveData.observe(viewLifecycleOwner, Observer {
            val user = it.data
            if (user != null) {
                if(!profileViewModel.hasSaveableChanges) {
                    setSaveableProfileInfoFrom(user)
                }
                setUnsavableProfileInfoFrom(user)
                //Make sure changes in user data are reflected in membership adapter
                membershipAdapter?.notifyDataSetChanged()
                listenForSavableProfileChange()
            }
        })
    }

    private fun setSaveableProfileInfoFrom(user: User) {
        profileViewModel.firstNameLiveData.value = user.firstName
        profileViewModel.lastNameLiveData.value = user.lastName
        profileViewModel.dateOfBirthLiveData.value = user.dateOfBirth
        profileViewModel.sexLiveData.value = UserDto.SexEnum.fromValue(user.sex)
        profileViewModel.postalCodeLiveData.value = user.postalCode
        profileViewModel.countryLiveData.value = user.country
        profileViewModel.cityLiveData.value = user.city
        profileViewModel.streetLiveData.value = user.street

    }

    private fun setUnsavableProfileInfoFrom(user: User) {
        profileViewModel.emailLiveData.value = user.email
        profileViewModel.passwordLiveData.value = getString(string.password_placeholder)
        profileViewModel.subscriptionLiveData.value = when(SegmentHelper.getSegment(user)) {
            SegmentHelper.Segment.TRIALING, SegmentHelper.Segment.TRIALING_SUBSCRIBED -> {
                getString(string.profile_edit_subscription_subscription_trialing)
            }
            SegmentHelper.Segment.SUBSCRIBED -> {
                user.subscriptionPlan
            }
            else -> {
                null
            }
        }

        if(CreditCardHelper.hasUserCreditCard(user)) {
            profileViewModel.cardNumberLiveData.value = String.format(getString(string.card_placeholder), user.last4)
            profileViewModel.expirationDateLiveData.value = "${user.cardExpMonth}/${user.cardExpYear}"
            profileViewModel.cvvCodeLiveData.value = getString(string.cvv_placeholder)
            profileViewModel.cardBrandLiveData.value = user.cardBrand
        }
    }

    private fun initAdapters() {
        if (membershipAdapter == null) {
            membershipAdapter = ActionFormAdapter()
        }

        if (personalInformationAdapter == null) {
            personalInformationAdapter = FormAdapter()
        }

        if (loginDetailAdapter == null) {
            loginDetailAdapter = ActionFormAdapter()
        }

        if (miscAdapter == null) {
            miscAdapter = ActionFormAdapter()
        }

        if (addressInformationAdpater == null) {
            addressInformationAdpater = FormAdapter()
        }
    }

    private fun initRecyclerViews() {
        initRecyclerView(rvMembership, membershipAdapter)
        initRecyclerView(rvPersonalInformation, personalInformationAdapter)
        initRecyclerView(rvLoginDetails, loginDetailAdapter)
        initRecyclerView(rvMisc, miscAdapter)
        initRecyclerView(rvAddressInformation, addressInformationAdpater)
    }

    private fun initRecyclerView(recyclerView: RecyclerView, formAdapter: FormAdapter?) {
        recyclerView.apply {
            adapter = formAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun initRecyclerView(recyclerView: RecyclerView, actionFormAdapter: ActionFormAdapter?) {
        recyclerView.apply {
            adapter = actionFormAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun populateAdapters() {
        populateMembershipAdapter()
        populatePersonalInformationAdapter()
        populateAddressInformationAdapater()
        populateLoginDetailAdapter()
        populateMiscAdapter()
    }

    private fun populateAddressInformationAdapater() {
        addressInformationAdpater?.submitList(
                AdapterContent.getAddressInformationContent(requireContext(), profileViewModel)
        )
    }

    private fun populateMembershipAdapter() {
        membershipAdapter?.submitList(
            AdapterContent.getMembershipContent(
                requireContext(),
                profileViewModel,
                ::onSubscriptionClicked,
                ::onCreditCardClicked
            )
        )
    }

    private fun onSubscriptionClicked() {
        val user = homeViewModel.getUserLiveData.value?.data
        when {
            InstantApps.isInstantApp(requireContext()) -> {
                Instant.showInstallPrompt(requireActivity(), null, RC_INSTALL_APP, null)
            }
            else -> findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToEditSubscriptionFragment())
        }
    }

    private fun onCreditCardClicked() {
        if (profileViewModel.cardNumberLiveData.value == null) {
            findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToAddCreditCardFragment())
        } else {
            findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToEditCreditCardFragment())
        }
    }

    private fun populatePersonalInformationAdapter() {
        personalInformationAdapter?.submitList(
            AdapterContent.getPersonalInformationContent(
                requireContext(),
                profileViewModel,
                ::onBirthdayClicked,
                ::onGenderClicked
            )
        )
    }

    private fun onBirthdayClicked(position: Int) {
        var userDobCalendar : Calendar? = null
        val userDayOfBirth = profileViewModel.dateOfBirthLiveData.value
        if(userDayOfBirth != null){
            userDobCalendar = Calendar.getInstance()
            userDobCalendar.time = userDayOfBirth
        }

        val datePicker = DateOfBirthHelper.buildDatePicker(requireContext(), userDobCalendar) {
            profileViewModel.dateOfBirthLiveData.value = it
            personalInformationAdapter?.notifyItemChanged(position)
        }

        datePicker.show(requireFragmentManager(), "Datepickerdialog")
    }

    private fun onGenderClicked() {
        findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToGenderItemListDialogFragment())
    }

    private fun populateLoginDetailAdapter() {
        loginDetailAdapter?.submitList(
            AdapterContent.getLoginDetailContent(
                requireContext(),
                profileViewModel,
                ::onEmailClicked,
                ::onPasswordClicked,
                ::onLogOutClicked
            )
        )
    }

    private fun onEmailClicked() {
        findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToEditEmailFragment())
    }

    private fun onPasswordClicked() {
        findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToEditPasswordFragment())
    }

    private fun onLogOutClicked() {
        showCoffeeClubDialog(requireContext())
            .setTitle(string.profile_overview_logout_confirm_title)
            .setMessage(string.profile_overview_logout_confirm_message)
            .positiveButton(string.profile_overview_logout_confirm_yes) {
                AppResetHelper.resetApplication(requireContext())
            }
            .negativeButton(string.profile_overview_logout_confirm_no)
    }

    private fun populateMiscAdapter() {
        miscAdapter?.submitList(
            AdapterContent.getMiscContent(
                requireContext(),
                ::onTermsAndConditionsClicked,
                ::onPrivacyPolicyClicked,
                ::onDeleteProfileClicked
            )
        )
    }

    private fun onTermsAndConditionsClicked() {
        CustomTabHelper.openCustomTab(requireContext(), getString(string.url_terms_and_conditions))
    }

    private fun onPrivacyPolicyClicked() {
        CustomTabHelper.openCustomTab(requireContext(), getString(string.url_privacy_policy))
    }

    private fun onDeleteProfileClicked() {
        findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToDeleteProfileFragment())
    }

    private fun listenForSavableProfileChange() {
        profileViewModel.firstNameLiveData.observe(viewLifecycleOwner, Observer { firstName ->
            val user: User? = homeViewModel.getUserLiveData.value?.data
            if (firstName != user?.firstName) {
                setDataChangedState(true)
            }
        })

        profileViewModel.lastNameLiveData.observe(viewLifecycleOwner, Observer { lastName ->
            val user: User? = homeViewModel.getUserLiveData.value?.data
            if (lastName != user?.lastName) {
                setDataChangedState(true)
            }
        })

        profileViewModel.dateOfBirthLiveData.observe(viewLifecycleOwner, Observer { birthDate ->
            val user: User? = homeViewModel.getUserLiveData.value?.data
            if (birthDate != user?.dateOfBirth) {
                setDataChangedState(true)
            }
        })

        profileViewModel.sexLiveData.observe(viewLifecycleOwner, Observer { sex ->
            val user: User? = homeViewModel.getUserLiveData.value?.data
            if (sex != UserDto.SexEnum.fromValue(user?.sex)) {
                setDataChangedState(true)
            }
        })

        profileViewModel.postalCodeLiveData.observe(viewLifecycleOwner, Observer { postalCode ->
            val user: User? = homeViewModel.getUserLiveData.value?.data
            if (postalCode != user?.postalCode) {
                setDataChangedState(true)
            }
        })

        profileViewModel.countryLiveData.observe(viewLifecycleOwner, Observer { country ->
            val user: User? = homeViewModel.getUserLiveData.value?.data
            if (country != user?.country) {
                setDataChangedState(true)
            }
        })

        profileViewModel.streetLiveData.observe(viewLifecycleOwner, Observer { street ->
            val user: User? = homeViewModel.getUserLiveData.value?.data
            if (street != user?.street) {
                setDataChangedState(true)
            }
        })

        profileViewModel.cityLiveData.observe(viewLifecycleOwner, Observer { city ->
            val user: User? = homeViewModel.getUserLiveData.value?.data
            if (city != user?.city) {
                setDataChangedState(true)
            }
        })
    }

    private fun setDataChangedState(dataHasChanged: Boolean) {
        tvBtnSave.isEnabled = dataHasChanged
        profileViewModel.hasSaveableChanges = dataHasChanged
    }

    private fun initAppVersion() {
        try {
            val pInfo = requireActivity().packageManager.getPackageInfo(requireActivity().packageName, 0)
            tvMiscTitle.text = getVersionString(pInfo)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    private fun getVersionString(pInfo: PackageInfo): String {
        val versionName = pInfo.versionName
        val versionCode = if (Build.VERSION.SDK_INT >= 28) {
            pInfo.longVersionCode.toString()
        } else {
            pInfo.versionCode.toString()
        }
        return getString(string.app_version_template, versionName, versionCode)
    }
}