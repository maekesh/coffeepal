package com.app.coffeeclub.home.shopsoverview.paging

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.app.coffeeclub.home.R
import kotlinx.android.synthetic.main.row_list_footer.view.*

class ListFooterViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(status: PagingState?) {
        itemView.progressBar.isVisible = status == PagingState.LOADING_PAGE
    }

    companion object {
        fun create(parent: ViewGroup): ListFooterViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_list_footer, parent, false)

            return ListFooterViewHolder(view)
        }
    }
}