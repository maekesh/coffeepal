package com.app.coffeeclub.home.profile.password

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.coffeeclub.models.UpdateUserPasswordDto
import com.app.coffeeclub.network.repository.UsersRepository
import dk.idealdev.utilities.livedata.SingleLiveEvent
import dk.idealdev.utilities.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PasswordViewModel : ViewModel() {

    val currentPasswordLiveData: MutableLiveData<String> = MutableLiveData()
    val newPasswordLiveData: MutableLiveData<String> = MutableLiveData()
    val repeatedNewPasswordLiveData: MutableLiveData<String> = MutableLiveData()

    val changePasswordLiveData : MutableLiveData<Resource<Void>> = SingleLiveEvent()

    fun changePassword(){
        val postData = UpdateUserPasswordDto().apply {
            oldPassword = currentPasswordLiveData.value
            newPassword = newPasswordLiveData.value
        }

        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersMePasswordPut(changePasswordLiveData, postData)
        }
    }

    fun clear() {
        currentPasswordLiveData.value = null
        newPasswordLiveData.value = null
        repeatedNewPasswordLiveData.value = null
    }

}