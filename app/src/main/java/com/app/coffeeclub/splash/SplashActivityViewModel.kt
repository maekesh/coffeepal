package com.app.coffeeclub.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.coffeeclub.models.LanguageRequest
import com.app.coffeeclub.network.repository.UsersRepository
import com.google.gson.JsonObject
import dk.idealdev.utilities.response.Resource
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import java.util.*

class SplashActivityViewModel: ViewModel() {

    private val  _languageResponse: MutableLiveData<Resource<JsonObject>> = MutableLiveData()
    val languageDetailsLiveData: LiveData<Resource<JsonObject>> get() = _languageResponse

    init {
        getLanguageDetails()
    }

    fun getLanguageDetails() {
        val request: LanguageRequest = LanguageRequest(Locale.getDefault().language)
        viewModelScope.launch {
            UsersRepository.getLanguageFromServer(_languageResponse, request)
        }
    }
}