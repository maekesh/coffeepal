package com.app.coffeeclub.splash
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.app.coffeeclub.actions.Actions
import com.app.coffeeclub.utilities.helpers.SharedPreferencesHelper
import com.google.android.gms.common.wrappers.InstantApps
import dk.idealdev.utilities.response.Status

class SplashActivity : AppCompatActivity() {

    private val splashActivityViewModel: SplashActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataObserver()
//        loadUiScreen()
    }

    private fun dataObserver() {
        splashActivityViewModel.languageDetailsLiveData.observe(this, Observer {data->
            when(data.status) {
                Status.SUCCESS ->{
                    SharedPreferencesHelper.saveLanguageData(data.data.toString())
                    loadUiScreen()

                }
                Status.ERROR -> {
                    Log.d("result is ", data.message.toString())
                    loadUiScreen()
                }
                Status.LOADING ->{

                }
            }
        })
    }

    private fun loadUiScreen() {
        when {
            InstantApps.isInstantApp(this) -> {
                startActivity(Actions.getHomeIntent(this))
            }
            SharedPreferencesHelper.loadAccessToken().isNullOrBlank() -> {
                if (SharedPreferencesHelper.loadHasSkippedRegistration()) {
                    startActivity(Actions.getHomeIntent(this))
                } else {
                    startActivity(Actions.getOnboardingIntent(this, hasCreditCard = false))
                }
            }
            else -> {
                startActivity(Actions.getHomeIntent(this))
            }
        }
    }
}
