package com.app.coffeeclub.onboarding.signup

import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.app.coffeeclub.onboarding.signup.SignUpStep.*
import com.app.coffeeclub.onboarding.signup.step1.CreateUserFragment
import com.app.coffeeclub.onboarding.signup.step2.FinishUserFragment
import com.app.coffeeclub.onboarding.signup.step3.ReferralCodeFragment
import com.app.coffeeclub.onboarding.signup.step4.SubscriptionFragment
import com.app.coffeeclub.onboarding.signup.step5.CreditCardFragment

class SignUpPageAdapter(
    fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    var currentFragment : BaseSignUpStepFragment? = null

    override fun getItem(position: Int): BaseSignUpStepFragment = when (position) {
        CreateUser.step -> CreateUserFragment()
        FinishUser.step -> FinishUserFragment()
        ReferralCode.step -> ReferralCodeFragment()
        Subscription.step -> SubscriptionFragment()
        else -> CreateUserFragment()
    }

    override fun getCount(): Int = values().size

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {

        if(currentFragment != `object`){
            currentFragment = `object` as BaseSignUpStepFragment
        }

        super.setPrimaryItem(container, position, `object`)

    }
}

enum class SignUpStep(val step: Int) {
    CreateUser(0),
    FinishUser(1),
    ReferralCode(2),
    Subscription(3),

}