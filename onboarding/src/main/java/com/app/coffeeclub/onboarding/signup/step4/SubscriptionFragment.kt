package com.app.coffeeclub.onboarding.signup.step4


import android.annotation.SuppressLint
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.coffeeclub.models.StripePlan
import com.app.coffeeclub.onboarding.R
import com.app.coffeeclub.onboarding.signup.BaseSignUpStepFragment
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.uicomponents.forms.FormatType
import com.app.coffeeclub.uicomponents.forms.adapters.InputFormAdapter
import com.app.coffeeclub.uicomponents.forms.items.InputFormItem
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.getStringData
import com.app.coffeeclub.utilities.extensions.showText
import com.app.coffeeclub.utilities.extensions.validate
import com.app.coffeeclub.utilities.helpers.AutofillHelper
import com.app.coffeeclub.utilities.helpers.CreditCardHelper
import com.app.coffeeclub.utilities.helpers.MessageHelper
import dk.idealdev.utilities.response.Status
import kotlinx.android.synthetic.main.fragment_credit_card.*
import kotlinx.android.synthetic.main.fragment_subscription.*
import kotlinx.android.synthetic.main.fragment_subscription.rvCreditCard
import java.util.*

class SubscriptionFragment(override val layoutResId: Int = R.layout.fragment_subscription) : BaseSignUpStepFragment(), SubscriptionAdapter.ItemCallback {

    private lateinit var subscriptionAdapter: SubscriptionAdapter

    private var creditCardFormAdapter: InputFormAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        initAdapter()
        initCreditAdapter()
        observeSubscriptions(view)
        signUpViewModel.fetchSubscriptions()
    }

    private fun initViews() {
        tvSubscriptionTitle.showText(getString(string.sign_up_5_title), Language.getLanguageHashMap())
        tvSubscriptionDescription.showText(getString(string.sign_up_5_paragraph), Language.getLanguageHashMap())
        tvSubscriptionDescriptionSmall.showText(getString(string.sign_up_5_small_copy), Language.getLanguageHashMap())
    }

    private fun initAdapter() {
        subscriptionAdapter = SubscriptionAdapter(this@SubscriptionFragment, false)

        rvSubscriptions.apply {
            adapter = subscriptionAdapter
            layoutManager = GridLayoutManager(requireContext(), 2)
        }
    }

    override fun onItemClicked(item: StripePlan) {
        signUpViewModel.selectedSubscriptionLiveData.value = item
    }

    @SuppressLint("InlinedApi")
    private fun initCreditAdapter() {
        val formContentList = mutableListOf<InputFormItem>()
        formContentList.add(
                InputFormItem(
                        valueLiveData = signUpViewModel.cardNumberLiveData,
                        validation = ::validateNotEmpty,
                        label = getString(string.sign_up_5_label_1),
                        hint = getString(string.sign_up_5_placeholder_1),
                        inputType = InputType.TYPE_CLASS_NUMBER,
                        autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                        autoFillHint = View.AUTOFILL_HINT_CREDIT_CARD_NUMBER,
                        type = FormatType.CardNumber,
                        maxLength = resources.getInteger(com.app.coffeeclub.resources.R.integer.credit_card_length)
                )
        )
        formContentList.add(
                InputFormItem(
                        valueLiveData = signUpViewModel.expirationDateLiveData,
                        validation = ::validateNotEmpty,
                        label = getString(string.sign_up_5_label_2),
                        hint = getString(string.sign_up_5_placeholder_2),
                        inputType = InputType.TYPE_CLASS_NUMBER,
                        autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                        autoFillHint = View.AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_DATE,
                        type = FormatType.ExpiryDate,
                        maxLength = resources.getInteger(com.app.coffeeclub.resources.R.integer.expiration_date_length)
                )
        )
        formContentList.add(
                InputFormItem(
                        valueLiveData = signUpViewModel.cvvCodeLiveData,
                        validation = ::validateNotEmpty,
                        label = getString(string.sign_up_5_label_3),
                        hint = getString(string.sign_up_5_placeholder_3),
                        inputType = InputType.TYPE_CLASS_NUMBER,
                        autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                        autoFillHint = View.AUTOFILL_HINT_CREDIT_CARD_SECURITY_CODE,
                        maxLength = resources.getInteger(com.app.coffeeclub.resources.R.integer.cvv_length)
                )
        )


        if (creditCardFormAdapter == null) {
            creditCardFormAdapter = InputFormAdapter()
        }

        rvCreditCard.apply {
            adapter = creditCardFormAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        creditCardFormAdapter?.submitList(formContentList)

    }

    private fun validateNotEmpty(etValue: EditText, tvLabel: TextView, ivStatus: ImageView, hasFocus: Boolean, isNotFinalCheck: Boolean): Boolean {
        return etValue.validate({ text -> text.isNotBlank() },
                getStringData(getString(string.fill_one_field), Language.getLanguageHashMap()),
                tvLabel = tvLabel,
                tvError = tvError,
                ivStatus = ivStatus,
                hasFocus = hasFocus,
                isNotFinalCheck = isNotFinalCheck
        )
    }

    private fun observeSubscriptions(view: View) {
        signUpViewModel.subscriptionsLiveData.observe(viewLifecycleOwner, Observer {
            progressBar.isVisible = it.status == Status.LOADING

            when (it.status) {
                Status.SUCCESS -> {
                    subscriptionAdapter.submitList(updateList(it.data?.get(0)?.plans))
                    val selectedSubscription = signUpViewModel.selectedSubscriptionLiveData.value
                    if (selectedSubscription != null) {
                        subscriptionAdapter.selectItem(selectedSubscription)
                    } else {
                        subscriptionAdapter.setSelected(0)
                    }
                }
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    MessageHelper.showErrorMessageSnackbar(view,
                            topMargin = windowInsetsViewModel.windowInsetTop.value)
                }
            }
        })
    }

    private fun updateList(plans: List<StripePlan>?): List<StripePlan>? {
        plans?.forEachIndexed { index, stripePlan ->
            if (index == 0 && stripePlan.interval == getString(string.year)) {
                Collections.swap(plans, 0, 1)
            }
        }
        return plans
    }

    override fun isStepValid(): Boolean {
        val cardNumber = signUpViewModel.cardNumberLiveData.value
        val expirationDateString = signUpViewModel.expirationDateLiveData.value
        val cvv = signUpViewModel.cvvCodeLiveData.value
        if(signUpViewModel.selectedSubscriptionLiveData.value == null){
            if(signUpViewModel.isSetupSubscriptionsMode){
                view?.let { view ->  MessageHelper.showErrorMessageSnackbar(view,
                       getStringData( getString(string.select_subscription), Language.getLanguageHashMap()),
                        windowInsetsViewModel.windowInsetTop.value)
                }
            } else{
                view?.let { view ->  MessageHelper.showErrorMessageSnackbar(view,
                        getStringData( getString(string.go_directly_app), Language.getLanguageHashMap()),
                        windowInsetsViewModel.windowInsetTop.value)
                }
            }
          return false
        } else if(cardNumber.isNullOrBlank() || expirationDateString.isNullOrBlank() || cvv.isNullOrBlank() || expirationDateString.length < 4) {
            view?.let {
                MessageHelper.showErrorMessageSnackbar(it,
                        getStringData( getString(string.card_not_valid), Language.getLanguageHashMap()),
                        windowInsetsViewModel.windowInsetTop.value)
            }
            return false
        }

        val card = CreditCardHelper.getCreditCard(cardNumber, expirationDateString, cvv)
        val isCardValid = card?.validateCard() ?: false

        if(!isCardValid){
            view?.let {
                MessageHelper.showErrorMessageSnackbar(it,
                        getStringData( getString(string.card_not_valid), Language.getLanguageHashMap()),
                        windowInsetsViewModel.windowInsetTop.value)
            }
            return false
        } else{
            signUpViewModel.enteredValidatedCardInfo = card
        }
        return true
    }
}