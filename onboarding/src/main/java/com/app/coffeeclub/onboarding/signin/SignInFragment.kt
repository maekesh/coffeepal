package com.app.coffeeclub.onboarding.signin

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.app.coffeeclub.actions.Actions
import com.app.coffeeclub.onboarding.BaseOnboardingFragment
import com.app.coffeeclub.onboarding.R
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.getStringData
import com.app.coffeeclub.utilities.extensions.showHintText
import com.app.coffeeclub.utilities.extensions.showText
import com.app.coffeeclub.utilities.managers.smartlock.SmartLockManager
import com.app.coffeeclub.utilities.managers.smartlock.views.SmartLockView
import com.app.coffeeclub.utilities.extensions.validate
import com.app.coffeeclub.utilities.helpers.MessageHelper
import com.app.coffeeclub.utilities.helpers.SharedPreferencesHelper
import com.app.coffeeclub.utilities.helpers.ValidationHelper
import com.app.coffeeclub.utilities.managers.smartlock.models.RequestCodes.Companion.RC_CREDENTIALS_REQUEST
import com.google.android.gms.auth.api.credentials.*
import dk.idealdev.utilities.response.Status
import kotlinx.android.synthetic.main.fragment_sign_in.*
import retrofit2.HttpException
import timber.log.Timber
import java.lang.Exception
import java.net.HttpURLConnection

class SignInFragment(override val layoutResId: Int = R.layout.fragment_sign_in) : BaseOnboardingFragment(),
        SmartLockView {

    private val signInViewModel: SignInViewModel by viewModels()

    override fun onDestroy() {
        super.onDestroy()
        signInViewModel.smartLockManager?.clear()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        showBackAsWhiteArrow()
        initSmartLockManager()
        signInViewModel.smartLockManager?.requestCredentials()

        etEmail.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (etEmail.text.isNotBlank()) {
                validateEmail(hasFocus)
            }
        }

        etPassword.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (etPassword.text.isNotBlank()) {
                validatePassword(hasFocus)
            }
        }

        tvBtnForgotPassword.setOnClickListener {
            findNavController().navigate(SignInFragmentDirections.actionSignInFragmentToForgotPasswordFragment(etEmail.text.toString()))
        }

        tvBtnLogIn.setOnButtonClickListener {
            if (validateEmail(isNotFinalCheck = false) && validatePassword(isNotFinalCheck = false)) {
                signInUser(view)
            }
        }

        tvBtnSignUp.setOnClickListener {
            findNavController().navigate(SignInFragmentDirections.actionSignInFragmentToSignUpFragment())
        }
    }

    private fun initViews() {
        setTitle(getString(string.sign_in_title))
        etEmail.showHintText(getString(string.sign_in_email), Language.getLanguageHashMap())
        etPassword.showHintText(getString(string.sign_in_password), Language.getLanguageHashMap())
        tvBtnForgotPassword.showText(getString(string.sign_in_link_1), Language.getLanguageHashMap())
        val btnText = getStringData(getString(string.sign_in_button), Language.getLanguageHashMap())
        tvBtnLogIn.setText(btnText)
        tvBtnSignUp.showText(getString(string.sign_in_link_2), Language.getLanguageHashMap())
    }

    private fun validateEmail(hasFocus: Boolean = false, isNotFinalCheck: Boolean = true): Boolean {
        return etEmail.validate(
                { email -> ValidationHelper.isEmailValid(email) },
                getStringData(getString(string.invalid_email), Language.getLanguageHashMap()),
                tvError = tvError,
                hasFocus = hasFocus,
                isNotFinalCheck = isNotFinalCheck)
    }

    private fun validatePassword(hasFocus: Boolean = false, isNotFinalCheck: Boolean = true): Boolean {
        return etPassword.validate(
                { password -> ValidationHelper.isPasswordValid(requireContext(), password) },
                getStringData(getString(string.password_length), Language.getLanguageHashMap()),
                tvError = tvError,
                hasFocus = hasFocus,
                isNotFinalCheck = isNotFinalCheck)
    }

    private fun signInUser(view: View) {
        signInViewModel.loginUserLiveData.observe(viewLifecycleOwner, Observer {
            setUiEnabled(it.status != Status.LOADING)

            when (it.status) {
                Status.SUCCESS -> {
                    val data = it.data
                    if (data != null) {
                        SharedPreferencesHelper.saveRefreshToken(data.refreshToken)
                        SharedPreferencesHelper.saveAccessToken(data.token)
                        startHome()
                    } else {
                        MessageHelper.showErrorMessageSnackbar(view,
                                topMargin = windowInsetsViewModel.windowInsetTop.value)
                    }
                }
                Status.ERROR -> {
                    val throwable = it.throwable
                    if (throwable != null && throwable is HttpException) {
                        if (throwable.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            MessageHelper.showErrorMessageSnackbar(view,
                                    getStringData(getString(string.invalid_credentials), Language.getLanguageHashMap()),
                                    windowInsetsViewModel.windowInsetTop.value)
                        } else {
                            MessageHelper.showErrorMessageSnackbar(view,
                                    topMargin = windowInsetsViewModel.windowInsetTop.value)
                        }
                    } else {
                        MessageHelper.showErrorMessageSnackbar(view,
                                topMargin = windowInsetsViewModel.windowInsetTop.value)
                    }
                }
                Status.LOADING -> {
                }
            }
        })

        signInViewModel.signInUser(etEmail.text.toString().trim(), etPassword.text.toString())
    }

    private fun startHome() {
        startActivity(Actions.getHomeIntentNewUser(requireContext()))
        requireActivity().finish()
    }

    private fun setUiEnabled(enable: Boolean) {
        etEmail.isEnabled = enable
        etPassword.isEnabled = enable
        tvBtnSignUp.isEnabled = enable
        tvBtnForgotPassword.isEnabled = enable
        tvBtnLogIn.setProgressState(!enable)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RC_CREDENTIALS_REQUEST -> signInViewModel.smartLockManager?.handleCredentialRequest(resultCode, data)
        }
    }

    private fun initSmartLockManager() {
        signInViewModel.smartLockManager = SmartLockManager.Builder(requireActivity() as AppCompatActivity)
                .withSmartLockCredentialRequest(createCredentialsRequest())
                .withSmartLockView(this)
                .build()
    }

    private fun createCredentialsRequest() =
            CredentialRequest.Builder()
                    .setPasswordLoginSupported(true)
                    .build()

    override fun onCredentialSelected(credential: Credential?) {
        etEmail.setText(credential?.id)
        etPassword.setText(credential?.password)
    }

    override fun credentialRequestFailure(exception: Exception?) {
        Timber.d(exception)
    }
}