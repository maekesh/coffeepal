package com.app.coffeeclub.onboarding.forgot

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.app.coffeeclub.models.EmailDto
import com.app.coffeeclub.onboarding.BaseOnboardingFragment
import com.app.coffeeclub.onboarding.R
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.utilities.extensions.validate
import com.app.coffeeclub.utilities.helpers.MessageHelper
import com.app.coffeeclub.utilities.helpers.ValidationHelper
import dk.idealdev.utilities.KeyboardUtil
import dk.idealdev.utilities.afterTextChanged
import dk.idealdev.utilities.response.Status
import kotlinx.android.synthetic.main.fragment_login_forgot.*

class LoginForgotFragment(override val layoutResId: Int = R.layout.fragment_login_forgot) : BaseOnboardingFragment() {

    private val loginForgotViewModel: LoginForgotViewModel by viewModelProvider()
    private val args: LoginForgotFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle(getString(string.signin2_title_forgot_password))
        showBackAsWhiteArrow()

        etEmail.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if(etEmail.text.isNotBlank()) {
                validateEmail(hasFocus)
            }
        }

        etEmail.afterTextChanged {input ->
            tvBtnRecreatePassword.setButtonEnabled(ValidationHelper.isEmailValid(input))
        }

        initEmailFromArgs()

        tvBtnRecreatePassword.setOnButtonClickListener {
            onSendLink()
        }

        tvBtnResendLink.setOnClickListener {
            onSendLink()
        }

        observeOnUserForgotPost(view)
    }

    private fun initEmailFromArgs() {
        if(args.email.isNotEmpty()) {
            etEmail.setText(args.email)
            etEmail.selectAll()
            KeyboardUtil.showKeyboard(requireActivity())
        }
    }

    private fun onSendLink() {
        if (validateEmail(isNotFinalCheck = false)) {
            val emailDto = EmailDto().apply {
                email = etEmail.text.toString()
            }
            loginForgotViewModel.usersForgotPost(emailDto)
        }
    }

    private fun observeOnUserForgotPost(view: View) {
        loginForgotViewModel.loginForgotLiveData.observe(viewLifecycleOwner, Observer {
            tvBtnRecreatePassword.setProgressState(it.status == Status.LOADING)
            when(it.status) {
                Status.SUCCESS -> {
                    MessageHelper.showSuccessMessageSnackbar(
                            view,
                            getString(string.signin2_button_resend_link_sent),
                            windowInsetsViewModel.windowInsetTop.value
                    )
                    findNavController().navigateUp()
                }
                Status.LOADING -> {

                }
                Status.ERROR -> {
                    MessageHelper.showErrorMessageSnackbar(view,
                        topMargin = windowInsetsViewModel.windowInsetTop.value)
                }
            }
        })
    }

    private fun validateEmail(hasFocus: Boolean = false, isNotFinalCheck: Boolean = true): Boolean {
        return etEmail.validate(
            { email -> ValidationHelper.isEmailValid(email) },
            getString(string.signin_email_validation_error_message),
            tvError = tvError,
            hasFocus = hasFocus,
            isNotFinalCheck = isNotFinalCheck)
    }

}