package com.app.coffeeclub.onboarding.signup.step3

import android.os.Bundle
import android.view.View
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.app.coffeeclub.onboarding.R
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.onboarding.signup.BaseSignUpStepFragment
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.getStringData
import com.app.coffeeclub.utilities.extensions.showHintText
import com.app.coffeeclub.utilities.extensions.showText
import com.app.coffeeclub.utilities.extensions.validate
import com.app.coffeeclub.utilities.helpers.MessageHelper
import com.app.coffeeclub.utilities.helpers.ValidationHelper
import dk.idealdev.utilities.afterTextChanged
import dk.idealdev.utilities.response.Status
import kotlinx.android.synthetic.main.fragment_referral_code.*

class ReferralCodeFragment(override val layoutResId: Int = R.layout.fragment_referral_code) : BaseSignUpStepFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        initExistingReferralCode()
        observeReferralCodeExists(view)

        etReferralCode.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
//            validateReferralCode(hasFocus)
        }

        etReferralCode.afterTextChanged { code ->
            signUpViewModel.referralCodeLiveData.value = code
            ivDelete.isVisible = code.isNotBlank()
            tvBtnActivateCode.isVisible = true
            tvBtnActivateCode.isEnabled = code.isNotBlank()
        }

        tvBtnActivateCode.setOnClickListener {
            if (ValidationHelper.isReferralCodeValid(requireContext(), etReferralCode?.text.toString())) {
                etReferralCode.isEnabled = false
//            ivDelete.isVisible = false
                signUpViewModel.checkReferralCodeExists(etReferralCode.text.toString())
            } else {
                tvBtnActivateCode.isEnabled = false
                MessageHelper.showErrorMessageSnackbar(view,
                        getStringData(getString(string.invalid_referral_code), Language.getLanguageHashMap()),
                        windowInsetsViewModel.windowInsetTop.value)
            }

        }

        tvBtnDeactivateCode.setOnClickListener {
            etReferralCode.text.clear()
            etReferralCode.isEnabled = true
            tvBtnDeactivateCode.isVisible = false
            tvBtnActivateCode.isVisible = true

            MessageHelper.showSuccessMessageSnackbar(view,
                    getString(string.signup3_code_deactivated),
                    windowInsetsViewModel.windowInsetTop.value)
        }

        ivDelete.setOnClickListener {
            etReferralCode.text.clear()
        }
    }

    private fun initViews() {
        tvReferralTitle.showText(getString(string.sign_up_4_paragraph), Language.getLanguageHashMap())
        etReferralCode.showHintText(getString(string.sign_up_4_placeholder), Language.getLanguageHashMap())
        tvBtnActivateCode.showText(getString(string.sign_up_4_button_1), Language.getLanguageHashMap())
        referralTvConditions.showText(getString(string.sign_up_4_small_copy), Language.getLanguageHashMap())
    }

    private fun initExistingReferralCode() {
        val existingReferralCode = signUpViewModel.referralCodeLiveData.value
        if (existingReferralCode != null) {
            etReferralCode.setText(existingReferralCode)
        }
    }

    private fun observeReferralCodeExists(view: View) {
        signUpViewModel.getReferralCodeExistsLiveData.observe(viewLifecycleOwner, Observer {
            pbActivateCode.isVisible = it.status == Status.LOADING
            tvBtnActivateCode.isInvisible = it.status == Status.LOADING

            when (it.status) {
                Status.SUCCESS -> {
                    val exists = it.data?.isExists ?: return@Observer
                    signUpViewModel.doesReferralCodeExistsLiveData.value = exists
                    etReferralCode.isEnabled = true
//                    tvBtnDeactivateCode.isVisible = exists

                    if (exists) {
                        tvBtnActivateCode.isInvisible = exists
                        MessageHelper.showSuccessMessageSnackbar(view,
                                getStringData(getString(string.referral_code_redeemed), Language.getLanguageHashMap()),
                                windowInsetsViewModel.windowInsetTop.value)
                    } else {
                        tvBtnActivateCode.isEnabled = false
                        MessageHelper.showErrorMessageSnackbar(view,
                                getStringData(getString(string.invalid_referral_code), Language.getLanguageHashMap()),
                                windowInsetsViewModel.windowInsetTop.value)
                    }
                }
                Status.ERROR -> {
                    etReferralCode.isEnabled = true
                    tvBtnActivateCode.isEnabled = false
                    MessageHelper.showErrorMessageSnackbar(view,
                            topMargin = windowInsetsViewModel.windowInsetTop.value)
                }
                Status.LOADING -> {
                }
            }
        })
    }

    private fun validateReferralCode(hasFocus: Boolean = false, isNotFinalCheck: Boolean = true): Boolean {
        tvError.isVisible = false
        return etReferralCode.validate(
                { referralCode -> ValidationHelper.isReferralCodeValid(requireContext(), referralCode) },
                getString(string.signup3_code_length_invalid),
                hasFocus = hasFocus,
                isNotFinalCheck = isNotFinalCheck)
    }

    override fun isStepValid(): Boolean {
        return etReferralCode.text.isBlank() || validateReferralCode(isNotFinalCheck = false)
    }
}