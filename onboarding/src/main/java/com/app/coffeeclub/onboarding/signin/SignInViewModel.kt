package com.app.coffeeclub.onboarding.signin

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.coffeeclub.models.AuthResponseDto
import com.app.coffeeclub.models.SignInDto
import com.app.coffeeclub.network.repository.UsersRepository
import com.app.coffeeclub.utilities.managers.smartlock.SmartLockManager
import dk.idealdev.utilities.livedata.SingleLiveEvent
import dk.idealdev.utilities.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SignInViewModel : ViewModel() {

    var smartLockManager: SmartLockManager? = null

    override fun onCleared() {
        super.onCleared()
        smartLockManager?.clear()
    }

    val loginUserLiveData : MutableLiveData<Resource<AuthResponseDto>> = SingleLiveEvent()

    fun signInUser(emailValidated : String, passwordValidated : String){
        val postData = SignInDto().apply {
            email = emailValidated
            password = passwordValidated
        }
        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersSignInPost(loginUserLiveData, postData)
        }
    }
}