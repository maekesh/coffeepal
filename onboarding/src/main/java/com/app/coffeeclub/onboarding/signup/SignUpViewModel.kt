package com.app.coffeeclub.onboarding.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.coffeeclub.models.*
import com.app.coffeeclub.network.repository.ShopsRepository
import com.app.coffeeclub.network.repository.SubscriptionsRepository
import com.app.coffeeclub.network.repository.UsersRepository
import com.app.coffeeclub.utilities.managers.smartlock.HintsManager
import com.stripe.android.model.Card
import dk.idealdev.utilities.livedata.SingleLiveEvent
import dk.idealdev.utilities.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.util.*

class SignUpViewModel: ViewModel() {

    enum class ApiSignUpSteps{
        CreateUser,
        UpdateUser,
        UploadPaymentToken,
        SaveSubscription
    }

    var nextApiSignUpStep : ApiSignUpSteps = ApiSignUpSteps.CreateUser
    var hintsManager: HintsManager? = null

    override fun onCleared() {
        super.onCleared()
        hintsManager?.clear()
    }
    var isSetupSubscriptionsMode = false
    var hasAcceptedNoShopNearby = false
    val subscriptionsLiveData: MutableLiveData<Resource<List<StripeProduct>>> = MutableLiveData()

    // Create User
    val firstNameLiveData: MutableLiveData<String> = MutableLiveData()
    val lastNameLiveData: MutableLiveData<String> = MutableLiveData()
    val emailLiveData: MutableLiveData<String> = MutableLiveData()
    val passwordLiveData: MutableLiveData<String> = MutableLiveData()
    val repeatPasswordLiveData: MutableLiveData<String> = MutableLiveData()

    // Finish User
    val countryLiveData: MutableLiveData<String> = MutableLiveData()
    val cityLiveData: MutableLiveData<String> = MutableLiveData()
    val streetLiveData: MutableLiveData<String> = MutableLiveData()
    val dateOfBirthLiveData: MutableLiveData<Date> = MutableLiveData()
    val sexLiveData: MutableLiveData<UserDto.SexEnum> = MutableLiveData()
    val postalCodeLiveData: MutableLiveData<String> = MutableLiveData()

    // Referral
    val referralCodeLiveData: MutableLiveData<String> = MutableLiveData()
    val getReferralCodeExistsLiveData: MutableLiveData<Resource<ReferralExistsResponse>> = SingleLiveEvent()
    val doesReferralCodeExistsLiveData: MutableLiveData<Boolean> = MutableLiveData()

    // SubscriptionType
    val selectedSubscriptionLiveData: MutableLiveData<StripePlan> = MutableLiveData()
    val postSubscriptionLiveData: MutableLiveData<Resource<Void>> = SingleLiveEvent()

    // Credit Card
    val cardNumberLiveData: MutableLiveData<String> = MutableLiveData()
    val expirationDateLiveData: MutableLiveData<String> = MutableLiveData()
    val cvvCodeLiveData: MutableLiveData<String> = MutableLiveData()
    var enteredValidatedCardInfo: Card? = null

    //Api live data
    val createUserLiveData : MutableLiveData<Resource<AuthResponseDto>> = SingleLiveEvent()
    val updateUserLiveData : MutableLiveData<Resource<User>> = SingleLiveEvent()
    val uploadPaymentTokenLiveData : MutableLiveData<Resource<Any>> = SingleLiveEvent()
    val shopsFromPostalLiveData : MutableLiveData<Resource<ShopsResponse>> = MutableLiveData()

    val _showErrorMessage: MutableLiveData<String> = SingleLiveEvent()
    var showErrorMessage: LiveData<String> = _showErrorMessage

    fun fetchSubscriptions(forceRefresh : Boolean = false){
        viewModelScope.launch(Dispatchers.IO) {
            SubscriptionsRepository.usersMeSubscriptionAvailableGet(subscriptionsLiveData, forceRefresh)
        }
    }

    fun saveSubscriptions(){
        val plan = selectedSubscriptionLiveData.value
        val code = referralCodeLiveData.value

        val postData = PlanDto().apply{
            planId = plan?.id
            referralCode = code
            step = 3
        }

        viewModelScope.launch(Dispatchers.IO) {
            SubscriptionsRepository.usersMeSubscriptionPost(postSubscriptionLiveData, postData)
        }
    }

    fun createUser(){
        val postData = CreateUserDto().apply {
            firstName = firstNameLiveData.value?.trim()
            lastName = lastNameLiveData.value?.trim()
            email = emailLiveData.value
            password = passwordLiveData.value
            step = 1
        }
        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersSignUpPost(createUserLiveData, postData)
        }
    }

    fun updateUser(){
        val postData = UserDto().apply {
            firstName = firstNameLiveData.value?.trim()
            lastName = lastNameLiveData.value?.trim()
            email = emailLiveData.value
            postalCode = postalCodeLiveData.value
            country = countryLiveData.value
            city = cityLiveData.value
            street = streetLiveData.value
            sex = sexLiveData.value
            dateOfBirth = dateOfBirthLiveData.value
            step = 2
        }

        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersMePut(updateUserLiveData, postData)
        }
    }

    fun uploadPaymentToken(token : String){
        val postData = PaymentSourceDto().apply {
            sourceToken = token
        }

        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersMePaymentSourcePost(uploadPaymentTokenLiveData, postData)
        }
    }

    fun getShopsFromPostalCode(){
        viewModelScope.launch(Dispatchers.IO) {
            ShopsRepository.shopsGet(shopsFromPostalLiveData, limit = BigDecimal.valueOf(1), page = BigDecimal.valueOf(1), postalCode = postalCodeLiveData.value, forceRefresh = true)
        }
    }

    fun checkReferralCodeExists(referralCode : String){
        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersReferralCodeGet(getReferralCodeExistsLiveData, referralCode)
        }
    }

    fun setErrorMessageInView(error: String) {
        _showErrorMessage.postValue(error)
    }
}