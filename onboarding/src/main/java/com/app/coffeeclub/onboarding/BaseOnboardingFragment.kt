package com.app.coffeeclub.onboarding

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import com.app.coffeeclub.base.BaseFragment
import com.app.coffeeclub.uicomponents.R
import com.app.coffeeclub.onboarding.R as res
import com.app.coffeeclub.utilities.helpers.InsetHelper
import dk.idealdev.utilities.livedata.observeOnce

/**
 * All OnboardingFragments should observe on window insets
 * To get a better overview; please visit README.md
 */
abstract class BaseOnboardingFragment : BaseFragment() {

    protected val windowInsetsViewModel: WindowInsetsViewModel by viewModelProvider()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeWindowInsetTop(view)
        observeWindowInsetBottom(view)
    }

    private fun observeWindowInsetTop(view: View) {
        windowInsetsViewModel.windowInsetTop.observeOnce(viewLifecycleOwner, Observer { inset ->
            onTopInsetChanged(inset)
            val toolbar = view.findViewById<Toolbar>(R.id.toolbar) ?: return@Observer
            InsetHelper.addTopPadding(toolbar, inset)
        })
    }

    private fun observeWindowInsetBottom(view: View) {
        windowInsetsViewModel.windowInsetBottom.observeOnce(viewLifecycleOwner, Observer {inset ->
            onBottomInsetChanged(inset)
            val container = view.findViewById<ConstraintLayout>(res.id.container) ?: return@Observer
            InsetHelper.addBottomMargin(container, inset)
        })
    }

    open fun onTopInsetChanged(inset: Int) {}
    open fun onBottomInsetChanged(inset: Int) {}

}