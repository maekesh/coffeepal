package com.app.coffeeclub.onboarding.signup.step5

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.coffeeclub.onboarding.R
import com.app.coffeeclub.onboarding.signup.BaseSignUpStepFragment
import kotlinx.android.synthetic.main.fragment_credit_card.*
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.resources.R.integer
import com.app.coffeeclub.uicomponents.forms.FormatType
import com.app.coffeeclub.uicomponents.forms.adapters.InputFormAdapter
import com.app.coffeeclub.uicomponents.forms.items.InputFormItem
import com.app.coffeeclub.utilities.extensions.validate
import com.app.coffeeclub.utilities.helpers.AutofillHelper
import com.app.coffeeclub.utilities.helpers.CreditCardHelper
import com.app.coffeeclub.utilities.helpers.MessageHelper

class CreditCardFragment(override val layoutResId: Int = R.layout.fragment_credit_card) : BaseSignUpStepFragment() {

    private var creditCardFormAdapter: InputFormAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
    }

    @SuppressLint("InlinedApi")
    private fun initAdapter() {
        val formContentList = mutableListOf<InputFormItem>()
        formContentList.add(
            InputFormItem(
                valueLiveData = signUpViewModel.cardNumberLiveData,
                validation = ::validateNotEmpty,
                label = getString(string.signup5_label_credit_card),
                hint = getString(string.signup5_placeholder_credit_card),
                inputType = InputType.TYPE_CLASS_NUMBER,
                autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                autoFillHint = View.AUTOFILL_HINT_CREDIT_CARD_NUMBER,
                type = FormatType.CardNumber,
                maxLength = resources.getInteger(integer.credit_card_length)
            )
        )
        formContentList.add(
            InputFormItem(
                valueLiveData = signUpViewModel.expirationDateLiveData,
                validation = ::validateNotEmpty,
                label = getString(string.signup5_label_expiration_date),
                hint = getString(string.signup5_placeholder_expiration_date),
                inputType = InputType.TYPE_CLASS_NUMBER,
                autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                autoFillHint = View.AUTOFILL_HINT_CREDIT_CARD_EXPIRATION_DATE,
                type = FormatType.ExpiryDate,
                maxLength = resources.getInteger(integer.expiration_date_length)
            )
        )
        formContentList.add(
            InputFormItem(
                valueLiveData = signUpViewModel.cvvCodeLiveData,
                validation = ::validateNotEmpty,
                label = getString(string.signup5_label_cvv),
                hint = getString(string.signup5_placeholder_cvv),
                inputType = InputType.TYPE_CLASS_NUMBER,
                autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                autoFillHint = View.AUTOFILL_HINT_CREDIT_CARD_SECURITY_CODE,
                maxLength = resources.getInteger(integer.cvv_length)
            )
        )


        if (creditCardFormAdapter == null) {
            creditCardFormAdapter = InputFormAdapter()
        }

        rvCreditCard.apply {
            adapter = creditCardFormAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        creditCardFormAdapter?.submitList(formContentList)

    }

    override fun isStepValid(): Boolean {
        val cardNumber = signUpViewModel.cardNumberLiveData.value
        val expirationDateString = signUpViewModel.expirationDateLiveData.value
        val cvv = signUpViewModel.cvvCodeLiveData.value

        if(cardNumber.isNullOrBlank() || expirationDateString.isNullOrBlank() || cvv.isNullOrBlank() || expirationDateString.length < 4) {
            view?.let {
                MessageHelper.showErrorMessageSnackbar(it,
                    getString(string.signup5_error_invalid_card_info),
                    windowInsetsViewModel.windowInsetTop.value)
            }
            return false
        }

        val card = CreditCardHelper.getCreditCard(cardNumber, expirationDateString, cvv)
        val isCardValid = card?.validateCard() ?: false

        if(!isCardValid){
            view?.let {
                MessageHelper.showErrorMessageSnackbar(it,
                    getString(string.signup5_error_invalid_card_info),
                    windowInsetsViewModel.windowInsetTop.value)
            }
        } else{
            signUpViewModel.enteredValidatedCardInfo = card
        }

        return isCardValid
    }

    private fun validateNotEmpty(etValue: EditText, tvLabel: TextView, ivStatus: ImageView, hasFocus: Boolean, isNotFinalCheck: Boolean): Boolean {
        return etValue.validate({ text -> text.isNotBlank() },
            getString(string.signup_info_missing_fields),
            tvLabel = tvLabel,
            tvError = tvError,
            ivStatus = ivStatus,
            hasFocus = hasFocus,
            isNotFinalCheck = isNotFinalCheck
        )
    }
}
