package com.app.coffeeclub.onboarding.forgot

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.coffeeclub.models.EmailDto
import com.app.coffeeclub.network.repository.UsersRepository
import dk.idealdev.utilities.livedata.SingleLiveEvent
import dk.idealdev.utilities.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginForgotViewModel : ViewModel() {

    val loginForgotLiveData: MutableLiveData<Resource<Void>> = SingleLiveEvent()

    fun usersForgotPost(postData: EmailDto) {
        viewModelScope.launch(Dispatchers.IO) {
            UsersRepository.usersForgotPost(loginForgotLiveData, postData)
        }
    }
}