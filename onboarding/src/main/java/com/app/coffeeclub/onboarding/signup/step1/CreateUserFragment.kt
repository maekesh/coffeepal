package com.app.coffeeclub.onboarding.signup.step1

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.coffeeclub.onboarding.R
import com.app.coffeeclub.onboarding.signup.BaseSignUpStepFragment
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.uicomponents.forms.FormHelper
import com.app.coffeeclub.uicomponents.forms.adapters.InputFormAdapter
import com.app.coffeeclub.uicomponents.forms.items.InputFormItem
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.getStringData
import com.app.coffeeclub.utilities.extensions.validate
import com.app.coffeeclub.utilities.helpers.AutofillHelper
import com.app.coffeeclub.utilities.helpers.MessageHelper
import com.app.coffeeclub.utilities.helpers.ValidationHelper
import com.app.coffeeclub.utilities.managers.smartlock.HintsManager
import com.app.coffeeclub.utilities.managers.smartlock.views.HintView
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.credentials.CredentialPickerConfig
import com.google.android.gms.auth.api.credentials.HintRequest
import dk.idealdev.utilities.getDrawable
import kotlinx.android.synthetic.main.fragment_create_user.*

class CreateUserFragment(override val layoutResId: Int = R.layout.fragment_create_user) : BaseSignUpStepFragment(), HintView {

    private var createUserFormAdapter: InputFormAdapter? = null

    override fun onDestroy() {
        super.onDestroy()
        signUpViewModel.hintsManager?.clear()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
        setActionListener()
        initHintManager()
        requestCredentials()
        dataObserver()
    }

    private fun setActionListener() {
        rvCreateUser?.setOnClickListener {
            rvCreateUser?.background = getDrawable(com.app.coffeeclub.resources.R.drawable.form_background)
        }
    }

    private fun dataObserver() {
        signUpViewModel.showErrorMessage.observe(viewLifecycleOwner, Observer {
            if (!TextUtils.isEmpty(it)) {
                rvCreateUser?.background = getDrawable(com.app.coffeeclub.resources.R.drawable.form_background_error)
                MessageHelper.showErrorMessageSnackbar(requireView(), it, 40)
            }
        })

        signUpViewModel.firstNameLiveData.observe(viewLifecycleOwner, Observer {
            rvCreateUser?.background = getDrawable(com.app.coffeeclub.resources.R.drawable.form_background)
        })

        signUpViewModel.lastNameLiveData.observe(viewLifecycleOwner, Observer {
            rvCreateUser?.background = getDrawable(com.app.coffeeclub.resources.R.drawable.form_background)
        })

        signUpViewModel.emailLiveData.observe(viewLifecycleOwner, Observer {
            rvCreateUser?.background = getDrawable(com.app.coffeeclub.resources.R.drawable.form_background)
        })

        signUpViewModel.passwordLiveData.observe(viewLifecycleOwner, Observer {
            rvCreateUser?.background = getDrawable(com.app.coffeeclub.resources.R.drawable.form_background)
        })

    }

    @SuppressLint("InlinedApi")
    private fun initAdapter() {

        val formContentList = mutableListOf<InputFormItem>()
        formContentList.add(
                InputFormItem(
                        valueLiveData = signUpViewModel.firstNameLiveData,
                        validation = ::validateNotEmpty,
                        label = getString(string.sign_up_1_label_1),
                        hint = getString(string.sign_up_1_placeholder_1),
                        inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS,
                        autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                        autoFillHint = View.AUTOFILL_HINT_NAME
                )
        )

        formContentList.add(
                InputFormItem(
                        valueLiveData = signUpViewModel.lastNameLiveData,
                        validation = ::validateNotEmpty,
                        label = getString(string.sign_up_1_label_2),
                        hint = getString(string.sign_up_1_placeholder_2),
                        inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS,
                        autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                        autoFillHint = View.AUTOFILL_HINT_NAME
                )
        )

        formContentList.add(
                InputFormItem(
                        valueLiveData = signUpViewModel.emailLiveData,
                        validation = ::validateEmail,
                        label = getString(string.sign_up_1_label_3),
                        hint = getString(string.sign_up_1_placeholder_3),
                        inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS,
                        autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                        autoFillHint = View.AUTOFILL_HINT_EMAIL_ADDRESS
                )
        )

        formContentList.add(
                InputFormItem(
                        valueLiveData = signUpViewModel.passwordLiveData,
                        validation = ::validatePasswordLength,
                        label = getString(string.sign_up_1_label_4),
                        hint = getString(string.sign_up_1_placeholder_4),
                        inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD,
                        autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                        autoFillHint = View.AUTOFILL_HINT_PASSWORD
                )
        )

        if (createUserFormAdapter == null) {
            createUserFormAdapter = InputFormAdapter()
        }

        rvCreateUser?.apply {
            adapter = createUserFormAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
        createUserFormAdapter?.submitList(formContentList)

    }

    override fun isStepValid(): Boolean {
        val list = createUserFormAdapter?.currentList ?: return false
        val layoutManager = rvCreateUser.layoutManager ?: return false
        return FormHelper.validateForm(requireActivity(), list, layoutManager)
    }

    override fun onNextStep() {
        super.onNextStep()

        val email = signUpViewModel.emailLiveData.value
        val password = signUpViewModel.passwordLiveData.value
        val name = "${signUpViewModel.firstNameLiveData.value} ${signUpViewModel.lastNameLiveData.value}"

        val credential = Credential.Builder(email)
                .setPassword(password)
                .setName(name)
                .build()

        signUpViewModel.hintsManager?.saveCredential(credential)
    }

    private fun validateNotEmpty(etValue: EditText, tvLabel: TextView, ivStatus: ImageView, hasFocus: Boolean, isNotFinalCheck: Boolean): Boolean {
        /*return etValue.validate({ text -> text.isNotBlank() },
                getStringData(getString(string.fill_one_field), Language.getLanguageHashMap()),
                tvLabel = tvLabel,
                tvError = tvError,
                ivStatus = ivStatus,
                hasFocus = hasFocus,
                isNotFinalCheck = isNotFinalCheck)*/
        return true
    }

    private fun validateEmail(etEmail: EditText, tvLabel: TextView, ivStatus: ImageView, hasFocus: Boolean, isNotFinalCheck: Boolean): Boolean {
        val isEmailValid: Boolean = etEmail.validate({ email -> ValidationHelper.isEmailValid(email) },
                getStringData(getString(string.invalid_email), Language.getLanguageHashMap()),
                tvLabel = tvLabel,
                tvError = tvError,
                ivStatus = ivStatus,
                hasFocus = hasFocus,
                isNotFinalCheck = isNotFinalCheck)

        if (!isEmailValid){
            rvCreateUser?.background = getDrawable(com.app.coffeeclub.resources.R.drawable.form_background_error)
        }
        return isEmailValid
    }

    private fun validatePasswordLength(etValue: EditText, tvLabel: TextView, ivStatus: ImageView, hasFocus: Boolean, isNotFinalCheck: Boolean): Boolean {

        val isValidPassowrd = etValue.validate({ password ->
           ValidationHelper.isPasswordValid(requireContext(), password)
       },
               getStringData(getString(string.password_length), Language.getLanguageHashMap()),
               tvLabel = tvLabel,
               tvError = tvError,
               ivStatus = ivStatus,
               hasFocus = hasFocus,
               isNotFinalCheck = isNotFinalCheck)

        if (!isValidPassowrd){
            rvCreateUser?.background = getDrawable(com.app.coffeeclub.resources.R.drawable.form_background_error)
        }

        return isValidPassowrd
    }

    private fun validatePasswordMatch(etValue: EditText, tvLabel: TextView, ivStatus: ImageView, hasFocus: Boolean, isNotFinalCheck: Boolean): Boolean {
        return etValue.validate({ password -> password == signUpViewModel.passwordLiveData.value },
                getString(string.signup_password_validation_match_error_message),
                tvLabel = tvLabel,
                tvError = tvError,
                ivStatus = ivStatus,
                hasFocus = hasFocus,
                isNotFinalCheck = isNotFinalCheck)
    }

    private fun initHintManager() {
        signUpViewModel.hintsManager = HintsManager
                .Builder(requireActivity() as AppCompatActivity)
                .withHintRequest(createHintRequest())
                .withHintsView(this)
                .build()
    }

    private fun createHintRequest() =
            HintRequest
                    .Builder()
                    .setHintPickerConfig(createHintPickerConfig())
                    .setEmailAddressIdentifierSupported(true)
                    .build()

    private fun createHintPickerConfig() =
            CredentialPickerConfig.Builder()
                    .setShowCancelButton(true)
                    .setPrompt(CredentialPickerConfig.Prompt.SIGN_UP)
                    .build()

    private fun requestCredentials() {
        if (signUpViewModel.emailLiveData.value.isNullOrBlank()) {
            signUpViewModel.hintsManager?.requestEmailHints()
        }
    }

    override fun credentialSaveSuccess() {
        super.credentialSaveSuccess()
        view?.let {
            MessageHelper.showSuccessMessageSnackbar(it,
                    getString(string.signup_credentials_saved),
                    windowInsetsViewModel.windowInsetTop.value)
        }
    }

    override fun onCredentialSelected(credential: Credential?) {
        credential ?: return
        signUpViewModel.firstNameLiveData.value = credential.givenName
        signUpViewModel.lastNameLiveData.value = credential.familyName
        signUpViewModel.emailLiveData.value = credential.id
        signUpViewModel.passwordLiveData.value = credential.password
        signUpViewModel.repeatPasswordLiveData.value = credential.password
        createUserFormAdapter?.notifyDataSetChanged()
    }
}
