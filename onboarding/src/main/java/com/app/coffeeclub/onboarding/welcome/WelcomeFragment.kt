package com.app.coffeeclub.onboarding.welcome

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.app.coffeeclub.actions.Actions
import com.app.coffeeclub.onboarding.BaseOnboardingFragment
import com.app.coffeeclub.onboarding.R
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.showText
import com.app.coffeeclub.utilities.helpers.SharedPreferencesHelper
import kotlinx.android.synthetic.main.fragment_welcome.*
import kotlinx.android.synthetic.main.fragment_welcome.tvBtnSignUp

class WelcomeFragment(override val layoutResId: Int = R.layout.fragment_welcome) : BaseOnboardingFragment(), ViewPager.OnPageChangeListener {

    private lateinit var welcomePagerAdapter: WelcomePagerAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        val goToSignup = requireActivity().intent.getBooleanExtra(Actions.ARG_GO_TO_SIGNUP, false)
        val goToSignIn = requireActivity().intent.getBooleanExtra(Actions.ARG_GO_TO_SIGNIN, false)
        val goToSetupSubscription = requireActivity().intent.getBooleanExtra(Actions.ARG_GO_TO_SETUP_SUBSCRIPTION, false)
        val hasCreditCard = requireActivity().intent.getBooleanExtra(Actions.ARG_HAS_CREDIT_CARD, false)

        when {
            goToSignIn -> findNavController().navigate(WelcomeFragmentDirections.actionWelcomeFragmentToSignInFragmentInclusive())
            goToSignup -> findNavController().navigate(WelcomeFragmentDirections.actionWelcomeFragmentToSignUpFragmentInclusive(goToSetupSubscription = goToSetupSubscription, hasCreditCard = hasCreditCard))
            goToSetupSubscription -> findNavController().navigate(WelcomeFragmentDirections.actionWelcomeFragmentToSignUpFragmentInclusive(goToSetupSubscription = true, hasCreditCard = hasCreditCard))
        }

        initViewPager()

        tvBtnSignUp.setOnClickListener {
            findNavController().navigate(WelcomeFragmentDirections.actionWelcomeFragmentToSignUpFragment())
        }

        tvBtnGoToApp.setOnClickListener {
            SharedPreferencesHelper.saveHasSkippedRegistration(true)
            startActivity(Actions.getHomeIntentNewUser(requireContext()))
            requireActivity().finish()
        }

        tvBtnLogIn.setOnClickListener {
            findNavController().navigate(WelcomeFragmentDirections.actionWelcomeFragmentToSignInFragment())
        }
    }

    private fun initViews() {
        tvTrialHint.showText(getString(string.welcome_small_copy), Language.getLanguageHashMap())
        tvBtnSignUp.showText(getString(string.welcome_button_text), Language.getLanguageHashMap())
        tvBtnGoToApp.showText(getString(string.welcome_link_1), Language.getLanguageHashMap())
        tvBtnLogIn.showText(getString(string.welcome_link_2), Language.getLanguageHashMap())
    }

    private fun initViewPager() {
        welcomePagerAdapter = WelcomePagerAdapter(childFragmentManager, requireContext())

        viewpager.apply {
            adapter = welcomePagerAdapter
            addOnPageChangeListener(this@WelcomeFragment)
        }
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        if(welcomePagerAdapter.count > 1) {
            val showWidth = viewpager.height / imageView.height * imageView.width
            val overflow = showWidth - viewpager.width
            val translationX = overflow / (welcomePagerAdapter.count - 1) * (position + positionOffset)
            horizontalScrollView.scrollTo(translationX.toInt(), 0)
        }
    }

    override fun onPageSelected(position: Int) {

    }

}
