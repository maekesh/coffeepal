package com.app.coffeeclub.onboarding.welcome

import android.os.Bundle
import android.view.View
import com.app.coffeeclub.onboarding.BaseOnboardingFragment
import dk.idealdev.utilities.instanceOf
import dk.idealdev.utilities.getArgument
import com.app.coffeeclub.onboarding.R
import kotlinx.android.synthetic.main.fragment_page_welcome.*

class WelcomePageFragment(override val layoutResId: Int = R.layout.fragment_page_welcome) : BaseOnboardingFragment() {

    private val title: String by getArgument(ARG_TITLE)
    private val subTitle: String by getArgument(ARG_SUB_TITLE)

    companion object {
        private const val ARG_TITLE = "ARG_TITLE"
        private const val ARG_SUB_TITLE = "ARG_SUB_TITLE"

        fun newInstance(title: String, subTitle: String) : WelcomePageFragment {
            return instanceOf(ARG_TITLE to title, ARG_SUB_TITLE to subTitle)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        welcomeTitle.text = title
        welcomeSubTitle.text = subTitle
    }
}