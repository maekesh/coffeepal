package com.app.coffeeclub.onboarding

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class WindowInsetsViewModel : ViewModel() {

    val windowInsetTop : MutableLiveData<Int> = MutableLiveData()
    val windowInsetBottom : MutableLiveData<Int> = MutableLiveData()

}