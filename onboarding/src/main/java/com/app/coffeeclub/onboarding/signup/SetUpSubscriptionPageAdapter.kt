package com.app.coffeeclub.onboarding.signup

import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.app.coffeeclub.onboarding.signup.step3.ReferralCodeFragment
import com.app.coffeeclub.onboarding.signup.step4.SubscriptionFragment
import com.app.coffeeclub.onboarding.signup.step5.CreditCardFragment

class SetUpSubscriptionPageAdapter(
    fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    var currentFragment: BaseSignUpStepFragment? = null

    override fun getItem(position: Int): BaseSignUpStepFragment = when (position) {
        SetupSubscriptionStep.ReferralCode.step -> ReferralCodeFragment()
        SetupSubscriptionStep.Subscription.step -> SubscriptionFragment()
        else -> ReferralCodeFragment()
    }

    override fun getCount(): Int = SetupSubscriptionStep.values().size

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {

        if (currentFragment != `object`) {
            currentFragment = `object` as BaseSignUpStepFragment
        }

        super.setPrimaryItem(container, position, `object`)
    }
}

enum class SetupSubscriptionStep(val step: Int) {
    ReferralCode(0),
    Subscription(1)
}