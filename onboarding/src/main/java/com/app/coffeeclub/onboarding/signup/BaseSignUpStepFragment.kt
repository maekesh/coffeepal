package com.app.coffeeclub.onboarding.signup

import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.autofill.AutofillManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.app.coffeeclub.onboarding.BaseOnboardingFragment

abstract class BaseSignUpStepFragment : BaseOnboardingFragment() {

    val signUpViewModel : SignUpViewModel by viewModelProvider()

    var autofillManager: AutofillManager? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            autofillManager = ContextCompat.getSystemService(requireContext(), AutofillManager::class.java)
        }
    }

    abstract fun isStepValid() : Boolean

    open fun onNextStep() {
        //Implement in decedent if needed
    }
}