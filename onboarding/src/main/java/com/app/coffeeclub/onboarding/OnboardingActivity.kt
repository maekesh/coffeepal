package com.app.coffeeclub.onboarding

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.lifecycle.ViewModelProviders
import com.app.coffeeclub.base.BaseActivity
import com.app.coffeeclub.onboarding.signin.SignInViewModel
import com.app.coffeeclub.onboarding.signup.SignUpViewModel
import com.app.coffeeclub.utilities.helpers.InsetHelper
import com.app.coffeeclub.utilities.managers.smartlock.models.RequestCodes
import kotlinx.android.synthetic.main.activity_onboarding.*

class OnboardingActivity : BaseActivity(R.layout.activity_onboarding){

    private val windowInsetsViewModel: WindowInsetsViewModel by lazy{
        ViewModelProviders.of(this).get(WindowInsetsViewModel::class.java)
    }

    private val signUpViewModel by lazy {
        ViewModelProviders.of(this).get(SignUpViewModel::class.java)
    }
    private val signInViewModel by lazy {
        ViewModelProviders.of(this).get(SignInViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        
        window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN)
        window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)

        initWindowInsetListener()
    }

    /**
     * Distributes top and bottom insets to WindowInsetsViewModel
     * To get a better overview; please visit README.md
     */
    private fun initWindowInsetListener() {
        onboardingContainer.setOnApplyWindowInsetsListener { _, insets ->
            if(insets.systemWindowInsetTop > 0) {
                windowInsetsViewModel.windowInsetTop.value = insets.systemWindowInsetTop
            }
            if(insets.systemWindowInsetBottom > 0) {
                windowInsetsViewModel.windowInsetBottom.value = insets.systemWindowInsetBottom
            }
            insets.consumeSystemWindowInsets()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            RequestCodes.RC_HINT_REQUEST -> signUpViewModel.hintsManager?.handleEmailHintRequest(resultCode, data)
            RequestCodes.RC_CREDENTIAL_SAVE -> signUpViewModel.hintsManager?.handleCredentialSave(resultCode)
            RequestCodes.RC_CREDENTIALS_REQUEST -> signInViewModel.smartLockManager?.handleCredentialRequest(resultCode, data)
        }
    }
}


