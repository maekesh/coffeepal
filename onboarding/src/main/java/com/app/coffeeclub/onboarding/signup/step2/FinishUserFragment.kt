package com.app.coffeeclub.onboarding.signup.step2

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.coffeeclub.models.ShopsResponse
import com.app.coffeeclub.onboarding.R
import com.app.coffeeclub.onboarding.signup.BaseSignUpStepFragment
import com.app.coffeeclub.onboarding.signup.SignUpFragmentDirections
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.resources.R.integer
import com.app.coffeeclub.uicomponents.dialogs.showCoffeeClubDialog
import com.app.coffeeclub.uicomponents.forms.FormHelper
import com.app.coffeeclub.uicomponents.forms.adapters.InputFormAdapter
import com.app.coffeeclub.uicomponents.forms.items.InputFormItem
import com.app.coffeeclub.utilities.extensions.validate
import com.app.coffeeclub.utilities.helpers.AutofillHelper
import com.app.coffeeclub.utilities.helpers.DateOfBirthHelper
import com.app.coffeeclub.utilities.helpers.ValidationHelper
import com.app.coffeeclub.utilities.viewmodels.GenderSelectionViewModel
import dk.idealdev.utilities.response.Resource
import kotlinx.android.synthetic.main.fragment_finish_user.*
import kotlinx.android.synthetic.main.fragment_finish_user.tvError
import timber.log.Timber
import java.util.*

class FinishUserFragment(override val layoutResId: Int = R.layout.fragment_finish_user) : BaseSignUpStepFragment() {
    private var finishUserFormAdapter: InputFormAdapter? = null
    private val genderViewModel: GenderSelectionViewModel by viewModelProvider()

    private val shopsFromPostalObserver = ShopsFromPostalObserver()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        observeGender()
        observePostalCode()
    }

    override fun onDestroy() {
        super.onDestroy()
        signUpViewModel.shopsFromPostalLiveData.removeObserver(shopsFromPostalObserver)
    }

    @SuppressLint("InlinedApi")
    private fun initAdapter() {
        val formContentList = mutableListOf<InputFormItem>()

        formContentList.add(
                InputFormItem(
                        valueLiveData = signUpViewModel.countryLiveData,
                        validation = ::validateNotEmpty,
                        label = getString(string.sign_up_3_label_1),
                        hint = getString(string.sign_up_3_placeholder_1),
                        inputType = InputType.TYPE_DATETIME_VARIATION_DATE
                )
        )

        formContentList.add(
                InputFormItem(
                        valueLiveData = signUpViewModel.cityLiveData,
                        validation = ::validateNotEmpty,
                        label = getString(string.sign_up_3_label_2),
                        hint = getString(string.sign_up_3_placeholder_2),
                        inputType = InputType.TYPE_DATETIME_VARIATION_DATE
                )
        )

        formContentList.add(
                InputFormItem(
                        valueLiveData = signUpViewModel.postalCodeLiveData,
                        validation = ::validatePostalCode,
                        label = getString(string.sign_up_3_label_3),
                        hint = getString(string.sign_up_3_placeholder_3),
                        inputType = InputType.TYPE_CLASS_NUMBER,
                        autofillIdBundle = AutofillHelper.getNextAutofillId(autofillManager),
                        autoFillHint = View.AUTOFILL_HINT_POSTAL_CODE,
                        maxLength = resources.getInteger(integer.postal_code_length)
                )
        )

        formContentList.add(
                InputFormItem(
                        valueLiveData = signUpViewModel.streetLiveData,
                        validation = ::validateNotEmpty,
                        label = getString(string.sign_up_3_label_4),
                        hint = getString(string.sign_up_3_placeholder_4),
                        inputType = InputType.TYPE_DATETIME_VARIATION_DATE
                )
        )


        formContentList.add(
                InputFormItem(
                        valueLiveData = signUpViewModel.dateOfBirthLiveData,
                        validation = ::validateNotEmpty,
                        label = getString(string.sign_up_3_label_5),
                        hint = getString(string.sign_up_3_placeholder_5),
                        clickCallback = { position ->
                            onSelectDateOfBirthTapped(position)
                        },
                        inputType = InputType.TYPE_DATETIME_VARIATION_DATE
                )
        )
        formContentList.add(
                InputFormItem(
                        valueLiveData = signUpViewModel.sexLiveData,
                        validation = ::validateNotEmpty,
                        label = getString(string.sign_up_3_label_6),
                        hint = getString(string.sign_up_3_placeholder_6),
                        inputType = InputType.TYPE_TEXT_FLAG_CAP_WORDS,
                        clickCallback = {
                            findNavController().navigate(SignUpFragmentDirections.actionSignUpFragmentToGenderItemListDialogFragment())
                        }
                )
        )


        if (finishUserFormAdapter == null) {
            finishUserFormAdapter = InputFormAdapter()
        }

        rvFinishUser.apply {
            adapter = finishUserFormAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        finishUserFormAdapter?.submitList(formContentList)

        signUpViewModel.sexLiveData.observe(viewLifecycleOwner, Observer {
            finishUserFormAdapter?.notifyItemChanged(5)
        })

    }

    private fun onSelectDateOfBirthTapped(position: Int) {
        var userDobCalendar: Calendar? = null
        val userDayOfBirth = signUpViewModel.dateOfBirthLiveData.value
        if (userDayOfBirth != null) {
            userDobCalendar = Calendar.getInstance()
            userDobCalendar.time = userDayOfBirth
        }


        val datePicker = DateOfBirthHelper.buildDatePicker(requireContext(), userDobCalendar) {
            signUpViewModel.dateOfBirthLiveData.value = it
            finishUserFormAdapter?.notifyItemChanged(position)
        }
        datePicker.show(requireFragmentManager(), "Datepickerdialog")
    }

    private fun observeGender() {
        genderViewModel.selectedGender.observe(viewLifecycleOwner, Observer { gender ->
            signUpViewModel.sexLiveData.value = gender
        })
    }

    private fun observePostalCode() {
        signUpViewModel.postalCodeLiveData.observe(viewLifecycleOwner, Observer {
            if (ValidationHelper.isPostalCodeValid(requireContext(), it)) {
                signUpViewModel.getShopsFromPostalCode()
            }
        })

        signUpViewModel.shopsFromPostalLiveData.observe(viewLifecycleOwner, shopsFromPostalObserver)
    }

    private fun handleNoShopsResult() {
        if (!signUpViewModel.hasAcceptedNoShopNearby) {
            showCoffeeClubDialog(requireContext())
                    .setTitle(string.signup2_1_title)
                    .setMessage(string.signup2_1_message)
                    .positiveButton(string.signup2_1_button_okay) {
                        signUpViewModel.hasAcceptedNoShopNearby = true
                    }
        }
    }

    override fun isStepValid(): Boolean {
        // Since step is optional return true if no information has been filled in
        val birthDateValue = signUpViewModel.dateOfBirthLiveData.value
        val genderValue = signUpViewModel.sexLiveData.value
        val zipValue = signUpViewModel.postalCodeLiveData.value

        if (birthDateValue == null || genderValue == null || zipValue.isNullOrBlank() || zipValue.length == 4) {
            return true
        } else {
            val list = finishUserFormAdapter?.currentList ?: return false
            val layoutManager = rvFinishUser.layoutManager ?: return false
            return FormHelper.validateForm(requireActivity(), list, layoutManager)
        }
    }

    private fun validatePostalCode(etValue: EditText, tvLabel: TextView, ivStatus: ImageView, hasFocus: Boolean, isNotFinalCheck: Boolean): Boolean {
        return etValue.validate(
                { postalCode -> ValidationHelper.isPostalCodeValid(requireContext(), postalCode) },
                getString(string.signup2_error_postal_code),
                tvLabel = tvLabel,
                tvError = tvError,
                ivStatus = ivStatus,
                hasFocus = hasFocus,
                isNotFinalCheck = isNotFinalCheck
        )
    }

    private fun validateNotEmpty(etValue: EditText, tvLabel: TextView, ivStatus: ImageView, hasFocus: Boolean, isNotFinalCheck: Boolean): Boolean {
        return etValue.validate(
                { text -> text.isNotBlank() },
                getString(string.signup_info_missing_fields),
                tvLabel = tvLabel,
                tvError = tvError,
                ivStatus = ivStatus,
                hasFocus = hasFocus,
                isNotFinalCheck = isNotFinalCheck
        )
    }

    private inner class ShopsFromPostalObserver() : Observer<Resource<ShopsResponse>> {
        override fun onChanged(it: Resource<ShopsResponse>?) {
            if (it?.data != null) {
                val itemCount = it.data?.itemCount
                Timber.d("Got $itemCount for postal code ${signUpViewModel.postalCodeLiveData.value}")

                if (itemCount != null && itemCount.toInt() == 0) {
                    handleNoShopsResult()
                }
            }
        }
    }

}
