package com.app.coffeeclub.onboarding.signup.step4

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.View.OnClickListener
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.app.coffeeclub.models.StripePlan
import com.app.coffeeclub.uicomponents.R
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.utilities.helpers.FormatHelper

class SubscriptionAdapter(private val callback: ItemCallback, private val isEdit: Boolean) :
    ListAdapter<StripePlan, SubscriptionAdapter.SubscriptionViewHolder>(StripePlanDiff()) {

    private var selected: Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SubscriptionViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.row_subscription, parent, false)
    )

    override fun onBindViewHolder(holder: SubscriptionViewHolder, position: Int) =
        holder.bind(getItem(position), position)

    fun setSelected(position: Int) {
        selected = position
        notifyDataSetChanged()
        callback.onItemClicked(getItem(position))
    }

    override fun getItemCount(): Int {
        return super.getItemCount()
    }

    inner class SubscriptionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), OnClickListener {

        private var tvSubscriptionName: TextView =
            itemView.findViewById(R.id.tvSubscriptionName)
        private var tvPricePerMonth: TextView =
            itemView.findViewById(R.id.tvPricePerMonth)

        private var planSelectedCb: CheckBox = itemView.findViewById(R.id.planSelectedCb)
        private var saveTv: TextView = itemView.findViewById(R.id.saveTv)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            notifyItemChanged(selected)
            selected = adapterPosition
            notifyItemChanged(selected)

            val clicked = getItem(adapterPosition)
            //We assume we always have one plan
            callback.onItemClicked(clicked)
        }

        fun bind(stripePlan: StripePlan, position: Int) = with(itemView) {
            isSelected = selected == position
            planSelectedCb.isChecked = selected == position
            if(selected != position) {
                planSelectedCb.visibility = View.GONE
            }
            
            if(stripePlan.interval == itemView.context.getString(string.year)) {
                saveTv.visibility = View.VISIBLE
            }
            tvSubscriptionName.text = stripePlan.interval
            tvPricePerMonth.text = FormatHelper.formatPricePerMonth(this.context, stripePlan)

//            tvSubscriptionDescription.text = StripePlan.description
        }
    }

    fun selectItem(plan: StripePlan) {
        val index = currentList.indexOfFirst { it.id == plan.id }
        selected = index
        notifyItemChanged(index)
    }

    interface ItemCallback {
        fun onItemClicked(item: StripePlan)
    }

    private class StripePlanDiff : DiffUtil.ItemCallback<StripePlan>() {
        override fun areItemsTheSame(oldItem: StripePlan, newItem: StripePlan): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: StripePlan, newItem: StripePlan): Boolean {
            return oldItem.id == newItem.id &&
                    oldItem.nickname == newItem.nickname
        }
    }
}