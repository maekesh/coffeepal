package com.app.coffeeclub.onboarding.welcome

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import com.app.coffeeclub.R
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.getStringData

class WelcomePagerAdapter(fragmentManager: FragmentManager, private val context: Context) : FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> {
            WelcomePageFragment.newInstance(
                    getStringData(context.getString(R.string.welcome_headline_1), Language.getLanguageHashMap()),
                    getStringData(context.getString(R.string.welcome_paragraph_1), Language.getLanguageHashMap())
            )
        }
        1 -> {
            WelcomePageFragment.newInstance(
                    getStringData(context.getString(R.string.welcome_headline_2), Language.getLanguageHashMap()),
                    getStringData(context.getString(R.string.welcome_paragraph_2), Language.getLanguageHashMap())
            )
        }
        else -> {
            WelcomePageFragment.newInstance(
                    getStringData(context.getString(R.string.welcome_headline_3), Language.getLanguageHashMap()),
                    getStringData(context.getString(R.string.welcome_paragraph_3), Language.getLanguageHashMap())
            )
        }
    }

    override fun getCount() = 3
}