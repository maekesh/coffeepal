package com.app.coffeeclub.onboarding.signup

import android.app.Activity
import android.os.Bundle
import android.text.SpannableString
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.viewpager.widget.ViewPager
import com.app.coffeeclub.BuildConfig
import com.app.coffeeclub.actions.Actions
import com.app.coffeeclub.models.AuthResponseDto
import com.app.coffeeclub.onboarding.BaseOnboardingFragment
import com.app.coffeeclub.onboarding.R
import com.app.coffeeclub.onboarding.signup.SignUpStep.*
import com.app.coffeeclub.onboarding.signup.step1.CreateUserFragment
import com.app.coffeeclub.onboarding.signup.step2.FinishUserFragment
import com.app.coffeeclub.onboarding.signup.step4.SubscriptionFragment
import com.app.coffeeclub.onboarding.signup.step5.CreditCardFragment
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.getStringData
import com.app.coffeeclub.utilities.extensions.showText
import com.app.coffeeclub.utilities.helpers.*
import com.app.coffeeclub.uicomponents.R as ui
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.stripe.android.model.Token
import dk.idealdev.utilities.KeyboardUtil
import dk.idealdev.utilities.response.Resource
import dk.idealdev.utilities.response.Status
import kotlinx.android.synthetic.main.container_sign_up_bottom_bar.*
import kotlinx.android.synthetic.main.fragment_sign_up.*
import retrofit2.HttpException
import java.lang.Exception
import java.net.HttpURLConnection

class SignUpFragment(override val layoutResId: Int = R.layout.fragment_sign_up) : BaseOnboardingFragment(),
        ViewPager.OnPageChangeListener {

    private val args: SignUpFragmentArgs by navArgs()

    private lateinit var signUpPageAdapter: SignUpPageAdapter
    private lateinit var setUpSubscriptionPageAdapter: SetUpSubscriptionPageAdapter

    private val signUpViewModel: SignUpViewModel by viewModelProvider()

    companion object {
        private const val progressOffset = 1
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showBackAsWhiteArrow()

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            previousStep()
        }
        signUpViewModel.isSetupSubscriptionsMode = args.goToSetupSubscription
        initViews()
        setActionListener()
        observeSignUpLiveData(view)
        initBottomBar()
        initViewPager()
    }

    private fun initViews() {
        tvBtnContinue.showText(getString(string.sign_up_general_button), Language.getLanguageHashMap())
        tvBtnReadSubscriptionTerms.showText(getString(string.sign_up_5_link), Language.getLanguageHashMap())
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            previousStep()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    private fun initBottomBar() {
        if (!args.goToSetupSubscription) {
            initPrivacyPolicy()
        }
    }

    private fun setActionListener() {
        tvBtnContinue.setOnClickListener {
            if (isInputDataValid()) {
                onButtonContinuePressed()
            }
        }

        tvBtnReadSubscriptionTerms.setOnClickListener {
            CustomTabHelper.openCustomTab(requireContext(), getString(string.url_terms_and_conditions))
        }

        tvBtnSkip.setOnClickListener {
            uploadUserData()
        }
    }

    private fun onButtonContinuePressed() {
        val currentFragment: BaseSignUpStepFragment? = if (::signUpPageAdapter.isInitialized) {
            signUpPageAdapter.currentFragment
        } else {
            setUpSubscriptionPageAdapter.currentFragment
        }

        if (currentFragment != null && currentFragment.isStepValid()) {
            when (currentFragment) {
                is CreditCardFragment -> {
                    finishSignUp()
                }
                is SubscriptionFragment -> {
                    finishSignUp()
                }

                is CreateUserFragment -> {
                    createUserInDB()
                }

                is FinishUserFragment -> {
                    updateUserInDB()
                }
                else -> {
                    nextStep(currentFragment)
                }
            }
        }
    }

    private fun updateUserInDB() {
        signUpViewModel.updateUser()
    }

    /**
     * this method triggers the user registration API call with step1 Params added in the Params
     */
    private fun createUserInDB() {
        showLoading(getStringData(getString(string.subscription_create), Language.getLanguageHashMap()))
        signUpViewModel.createUser()
    }

    private fun finishSignUp() {
        //Skip user creation if only setting up subscription
        /*if (args.goToSetupSubscription) {
            finishSetupSubscription()
        } else {
            uploadUserData()
        }*/
        finishSetupSubscription()
    }

    private fun finishSetupSubscription() {
        showLoading(getStringData(getString(string.subscription_create), Language.getLanguageHashMap()))

        val enteredValidatedCardInfo = signUpViewModel.enteredValidatedCardInfo
        if (enteredValidatedCardInfo != null) {
            signUpViewModel.nextApiSignUpStep = SignUpViewModel.ApiSignUpSteps.UploadPaymentToken
            fetchAndUploadStripeToken(enteredValidatedCardInfo)
        } else {
            signUpViewModel.nextApiSignUpStep = SignUpViewModel.ApiSignUpSteps.SaveSubscription
            saveSubscription()
        }
    }

    private fun uploadUserData() {
        showLoading(getString(string.signup_loading))
        when (signUpViewModel.nextApiSignUpStep) {
            SignUpViewModel.ApiSignUpSteps.CreateUser -> signUpViewModel.createUser()
            SignUpViewModel.ApiSignUpSteps.UpdateUser -> signUpViewModel.updateUser()
            SignUpViewModel.ApiSignUpSteps.UploadPaymentToken -> fetchAndUploadStripeToken(signUpViewModel.enteredValidatedCardInfo)
            SignUpViewModel.ApiSignUpSteps.SaveSubscription -> signUpViewModel.saveSubscriptions()
        }
    }

    /**
     * 0. Show progress
     * 1. POST user (email, first name, last name, password)
     * 2. PUT user (dob, gender, zip)
     * 3. set payment info
     * 4. set subscription
     * 5. start home
     */
    private fun observeSignUpLiveData(view: View) {
        signUpViewModel.createUserLiveData.observe(viewLifecycleOwner, Observer {
            hideLoading()
            when (it.status) {
                Status.SUCCESS -> {
                    val data = it.data
                    if (data != null) {
                        SharedPreferencesHelper.saveAccessToken(data.token)
                        SharedPreferencesHelper.saveRefreshToken(data.refreshToken)
                        signUpViewModel.nextApiSignUpStep = SignUpViewModel.ApiSignUpSteps.UpdateUser
                        moveToNextStep()
                    }
                }
                Status.ERROR -> {
                    signUpViewModel.nextApiSignUpStep = SignUpViewModel.ApiSignUpSteps.CreateUser
                    handleCreateUserError(it, view, getStringData(getString(string.couldnot_create_user), Language.getLanguageHashMap()))
                }
                Status.LOADING -> {
                }
            }
        })

        signUpViewModel.updateUserLiveData.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    val data = it.data
                    if (data != null) {
                        moveToNextStep()
//                        finishSetupSubscription()
                    }
                }
                Status.ERROR -> {
                    signUpViewModel.nextApiSignUpStep = SignUpViewModel.ApiSignUpSteps.UpdateUser
                    showCreateUserError(view, getStringData(getString(string.fail_to_update_user), Language.getLanguageHashMap()))
                }
                Status.LOADING -> {
                }
            }
        })
    }

    private fun startHome() {
        startActivity(Actions.getHomeIntentNewUser(requireContext()))
        requireActivity().setResult(Activity.RESULT_OK)
        requireActivity().finish()
    }

    private fun fetchAndUploadStripeToken(cardInfo: Card?) {
        if (cardInfo == null) {
            showCreateUserError(view!!, getStringData(getString(string.fail_save_card), Language.getLanguageHashMap()))
            return
        }

        val stripe = Stripe(
                requireContext(),
                BuildConfig.STRIPE_API_KEY
        )
        stripe.createToken(
                cardInfo, object : TokenCallback {
            override fun onSuccess(result: Token) {
                uploadPaymentToken(result)
            }

            override fun onError(e: Exception) {
                signUpViewModel.nextApiSignUpStep = SignUpViewModel.ApiSignUpSteps.UploadPaymentToken
                showCreateUserError(view!!, getStringData(getString(string.fail_save_card), Language.getLanguageHashMap()))
            }
        }
        )
    }

    private fun uploadPaymentToken(result: Token) {
        signUpViewModel.uploadPaymentTokenLiveData.observe(viewLifecycleOwner, Observer { data ->
            when (data.status) {
                Status.SUCCESS -> {
                    signUpViewModel.nextApiSignUpStep = SignUpViewModel.ApiSignUpSteps.SaveSubscription
                    saveSubscription()
                }
                Status.ERROR -> {
                    signUpViewModel.nextApiSignUpStep = SignUpViewModel.ApiSignUpSteps.UploadPaymentToken
                    showCreateUserError(view!!, getStringData(getString(string.fail_save_card), Language.getLanguageHashMap()))
                }
                Status.LOADING -> {

                }
            }
        })
        signUpViewModel.uploadPaymentToken(result.id)
    }

    private fun saveSubscription() {
        signUpViewModel.postSubscriptionLiveData.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    startHome()
                }
                Status.ERROR -> {
                    signUpViewModel.nextApiSignUpStep = SignUpViewModel.ApiSignUpSteps.SaveSubscription
                    showCreateUserError(view!!, getStringData(getString(string.cant_create_sub), Language.getLanguageHashMap()))
                }
                Status.LOADING -> {
                }
            }
        })
        signUpViewModel.saveSubscriptions()
    }

    private fun handleCreateUserError(it: Resource<AuthResponseDto>, view: View, message: String) {
        val throwable = it.throwable
        if (throwable != null && throwable is HttpException) {

            if (throwable.code() == HttpURLConnection.HTTP_CONFLICT) {
                hideLoading()
                MessageHelper.showErrorMessageSnackbar(view,
                        getStringData(getString(string.email_already_in_use), Language.getLanguageHashMap()),
                        windowInsetsViewModel.windowInsetTop.value)
            } else {
                showCreateUserError(view, message)
            }
        } else {
            showCreateUserError(view, message)
        }
    }

    private fun showLoading(loadingText: String) {
        LoaderHelper.showIsLoadingData(view?.findViewById(ui.id.loaderContainer), view?.findViewById(ui.id.loaderText), loadingText)
    }

    private fun hideLoading() {
        LoaderHelper.hideIsLoadingData(view?.findViewById(ui.id.loaderContainer))
    }

    private fun showCreateUserError(view: View, message: String) {
        hideLoading()
        MessageHelper.showErrorMessageSnackbar(
                view,
                message,
                windowInsetsViewModel.windowInsetTop.value
        )
    }

    private fun initPrivacyPolicy() {
        val totalString = getString(string.sign_up_terms_condition)

        val privacyPolicyKeyword = getString(string.signup_keyword_privacy_policy)
        val privacyPolicyUrl = getString(string.url_privacy_policy)

        val termsAndConditionsKeyword = getString(string.signup_keyword_terms_and_conditions)
        val termsAndConditionsUrl = getString(string.url_terms_and_conditions)

        val spannableString = SpannableString(totalString)

        SpanHelper.addCustomTabToSpannable(
                spannableString,
                totalString,
                termsAndConditionsKeyword,
                termsAndConditionsUrl
        )
        SpanHelper.addCustomTabToSpannable(
                spannableString,
                totalString,
                privacyPolicyKeyword,
                privacyPolicyUrl)

        tvDisclaimer.text = spannableString
        tvDisclaimer.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun initViewPager() {
        if (args.goToSetupSubscription) {
            setUpSubscriptionPageAdapter = SetUpSubscriptionPageAdapter(childFragmentManager)
            setupFromAdapter(setUpSubscriptionPageAdapter)
        } else {
            signUpPageAdapter = SignUpPageAdapter(childFragmentManager)
            setupFromAdapter(signUpPageAdapter)
        }

        updateUIOnPageSelected(viewpager.currentItem)
    }

    private fun setupFromAdapter(pagerAdapter: FragmentStatePagerAdapter) {
        progressBar.max = pagerAdapter.count
        progressBar.progress = progressOffset

        viewpager.apply {
            adapter = pagerAdapter
            addOnPageChangeListener(this@SignUpFragment)

        }
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        updateUIOnPageSelected(position)
    }

    private fun updateUIOnPageSelected(position: Int) {
        updateTitle(position)
        updatePageCount(position)
        updateBottomBar(position)
    }

    private fun updateTitle(position: Int) {
        setTitle(
                getString(
                        if (args.goToSetupSubscription) {
                            when (position) {
                                SetupSubscriptionStep.ReferralCode.step -> string.signup3_title_referral_code
                                SetupSubscriptionStep.Subscription.step -> string.signup4_title_subscription
                                else -> -1
                            }
                        } else {
                            when (position) {
                                CreateUser.step -> string.sign_up_1_title
                                FinishUser.step -> string.sign_up_3_title
                                ReferralCode.step -> string.sign_up_4_title
                                Subscription.step -> string.display_nothing
                                else -> -1
                            }
                        }

                )
        )
    }

    private fun updatePageCount(position: Int) {
        tvStepProgress.text =
                String.format(getString(string.siginup_page_count), position + progressOffset, getPagerAdapter().count)
    }

    private fun updateBottomBar(position: Int) {
        progressBar.progress = position + progressOffset

        resetBottomBar()
        if (args.goToSetupSubscription) {
            when (position) {
                SetupSubscriptionStep.ReferralCode.step -> initReferralCode()
                SetupSubscriptionStep.Subscription.step -> initSubscription()
            }
        } else {
            when (position) {
                CreateUser.step -> initCreateUser()
                ReferralCode.step -> initReferralCode()
                Subscription.step -> initSubscription()
                else -> tvBtnContinue.isEnabled = true
            }
        }
    }

    private fun getPagerAdapter(): FragmentStatePagerAdapter {
        return if (::signUpPageAdapter.isInitialized) {
            signUpPageAdapter
        } else {
            setUpSubscriptionPageAdapter
        }
    }

    private fun resetBottomBar() {
//        tvBtnContinue.text = getString(string.general_button_continue)
        tvDisclaimer.isVisible = false
        tvBtnReadSubscriptionTerms.isVisible = false
    }

    private fun initCreateUser() {
        tvDisclaimer.isVisible = true
//        tvBtnContinue.isEnabled = false

        signUpViewModel.firstNameLiveData.observe(viewLifecycleOwner, Observer {
            setContinueButtonEnabledStateForCreateUserFields()
        })

        signUpViewModel.lastNameLiveData.observe(viewLifecycleOwner, Observer {
            setContinueButtonEnabledStateForCreateUserFields()
        })

        signUpViewModel.emailLiveData.observe(viewLifecycleOwner, Observer {
            setContinueButtonEnabledStateForCreateUserFields()
        })

        signUpViewModel.passwordLiveData.observe(viewLifecycleOwner, Observer {
            setContinueButtonEnabledStateForCreateUserFields()
        })

        signUpViewModel.repeatPasswordLiveData.observe(viewLifecycleOwner, Observer {
            setContinueButtonEnabledStateForCreateUserFields()
        })
    }

    private fun setContinueButtonEnabledStateForCreateUserFields() {
        val firstName = signUpViewModel.firstNameLiveData.value
        val lastName = signUpViewModel.lastNameLiveData.value
        val email = signUpViewModel.emailLiveData.value
        val password = signUpViewModel.passwordLiveData.value
        val repeatPassword = signUpViewModel.repeatPasswordLiveData.value

        val validFirstName = !firstName.isNullOrBlank()
        val validLastName = !lastName.isNullOrBlank()
        val validEmail = ValidationHelper.isEmailValid(email)
        val validPassword = ValidationHelper.isPasswordValid(requireContext(), password)
        val validRepeatPassword = ValidationHelper.isPasswordValid(requireContext(), repeatPassword) && repeatPassword == password
//        tvBtnContinue.isEnabled = validFirstName && validLastName && validEmail && validPassword
    }

    private fun isInputDataValid(): Boolean {
        val firstName = signUpViewModel.firstNameLiveData.value
        val lastName = signUpViewModel.lastNameLiveData.value
        val email = signUpViewModel.emailLiveData.value
        val password = signUpViewModel.passwordLiveData.value

        if (TextUtils.isEmpty(firstName)) {
            val error = getStringData(getString(string.missing_first_name), Language.getLanguageHashMap())
            signUpViewModel.setErrorMessageInView(error)
            return false
        }

        if (TextUtils.isEmpty(lastName)) {
            val error = getStringData(getString(string.missing_last_name), Language.getLanguageHashMap())
            signUpViewModel.setErrorMessageInView(error)
            return false
        }

        if (TextUtils.isEmpty(email)) {
            val error = getStringData(getString(string.invalid_email), Language.getLanguageHashMap())
            signUpViewModel.setErrorMessageInView(error)
            return false
        }

        if (TextUtils.isEmpty(password)) {
            val error = getStringData(getString(string.invalid_password), Language.getLanguageHashMap())
            signUpViewModel.setErrorMessageInView(error)
            return false
        }


        return true
    }

    private fun initReferralCode() {
        tvBtnContinue.isEnabled = true
        tvBtnContinue.showText(getString(string.sign_up_general_button), Language.getLanguageHashMap())

        signUpViewModel.referralCodeLiveData.observe(viewLifecycleOwner, Observer {
            setContinueButtonEnabledStateForReferralField()
        })

        signUpViewModel.doesReferralCodeExistsLiveData.observe(viewLifecycleOwner, Observer {
            setContinueButtonEnabledStateForReferralField()
        })
    }

    private fun setContinueButtonEnabledStateForReferralField() {
        val referralCode = signUpViewModel.referralCodeLiveData.value
        val doesReferralCodeExist = signUpViewModel.doesReferralCodeExistsLiveData.value ?: false

        tvBtnContinue.isEnabled = referralCode.isNullOrBlank() ||
                (ValidationHelper.isReferralCodeValid(requireContext(), referralCode) && doesReferralCodeExist)
    }

    private fun initSubscription() {
        tvBtnContinue.isEnabled = false
        tvBtnReadSubscriptionTerms.isVisible = true

        tvBtnContinue.showText(getString(string.sign_up_5_button), Language.getLanguageHashMap())

        signUpViewModel.selectedSubscriptionLiveData.observe(viewLifecycleOwner, Observer {
            setContinueButtonEnabledStateForSubscriptionFields()
        })
    }

    private fun setContinueButtonEnabledStateForSubscriptionFields() {
        tvBtnContinue.isEnabled = signUpViewModel.selectedSubscriptionLiveData.value != null
    }

    private fun nextStep(currentFragment: BaseSignUpStepFragment) {
        KeyboardUtil.hideKeyboard(requireActivity())
        currentFragment.onNextStep()

        if (viewpager.currentItem != getPagerAdapter().count) {
            viewpager.currentItem = viewpager.currentItem + 1
        }
    }


    private fun moveToNextStep() {
        val currentFragment: BaseSignUpStepFragment? = if (::signUpPageAdapter.isInitialized) {
            signUpPageAdapter.currentFragment
        } else {
            setUpSubscriptionPageAdapter.currentFragment
        }

        KeyboardUtil.hideKeyboard(requireActivity())
        currentFragment?.onNextStep()

        if (viewpager.currentItem != getPagerAdapter().count) {
            viewpager.currentItem = viewpager.currentItem + 1
        }
    }

    private fun previousStep() {
        if (viewpager.currentItem != 0) {
            viewpager.currentItem = viewpager.currentItem - 1
        } else {
            if (!findNavController().popBackStack()) {
                requireActivity().finish()
            }
        }
    }

}
