package com.app.coffeeclub.network.repository

import androidx.lifecycle.MutableLiveData
import com.app.coffeeclub.models.*
import com.app.coffeeclub.network.api.CompaniesApi
import dk.idealdev.utilities.response.Resource
import retrofit2.Response
import java.math.BigDecimal

object CompaniesRepository: BaseRepository() {

    suspend fun companiesCompletePost(liveData : MutableLiveData<Resource<Void>>, body: SignUpCompanyDto) {
        fetchData(liveData, true) { CompaniesApi.Factory.getApi().companiesCompletePost(body).await() }
    }

    suspend fun companiesGet(liveData : MutableLiveData<Resource<List<Company>>>, forceRefresh : Boolean = false) {
        fetchData(liveData, forceRefresh) { CompaniesApi.Factory.getApi().companiesGet().await() }
    }

    suspend fun companiesIdDeletePost(liveData : MutableLiveData<Resource<Void>>, id: BigDecimal) {
        fetchData(liveData, true) { CompaniesApi.Factory.getApi().companiesIdDeletePost(id).await() }
    }

    suspend fun companiesIdEmployeesEmployeeIdDelete(liveData : MutableLiveData<Resource<Void>>, employeeId: BigDecimal, id: BigDecimal) {
        fetchData(liveData, true) { CompaniesApi.Factory.getApi().companiesIdEmployeesEmployeeIdDelete(employeeId, id).await() }
    }

    suspend fun companiesIdEmployeesEmployeeIdPut(liveData : MutableLiveData<Resource<Void>>, employeeId: BigDecimal, id: BigDecimal) {
        fetchData(liveData, true) { CompaniesApi.Factory.getApi().companiesIdEmployeesEmployeeIdPut(employeeId, id).await() }
    }

    suspend fun companiesIdEmployeesEmployeeIdResendInvitePost(liveData : MutableLiveData<Resource<Void>>, employeeId: BigDecimal, id: BigDecimal) {
        fetchData(liveData, true) { CompaniesApi.Factory.getApi().companiesIdEmployeesEmployeeIdResendInvitePost(employeeId, id).await() }
    }

    suspend fun companiesIdEmployeesPost(liveData : MutableLiveData<Resource<Void>>, body: InviteDto, id: BigDecimal) {
        fetchData(liveData, true) { CompaniesApi.Factory.getApi().companiesIdEmployeesPost(body, id).await() }
    }

    suspend fun companiesIdPut(liveData : MutableLiveData<Resource<Void>>, body: CompanyDto, id: BigDecimal) {
        fetchData(liveData, true) { CompaniesApi.Factory.getApi().companiesIdPut(body, id).await() }
    }

    suspend fun companiesIdShopsGet(liveData : MutableLiveData<Resource<Void>>, id: BigDecimal, forceRefresh : Boolean = false) {
        fetchData(liveData, forceRefresh) { CompaniesApi.Factory.getApi().companiesIdShopsGet(id).await() }
    }

    suspend fun companiesIdShopsShopIdGet(liveData : MutableLiveData<Resource<Shop>>, shopId: BigDecimal, id: BigDecimal, forceRefresh : Boolean = false) {
        fetchData(liveData, forceRefresh) { CompaniesApi.Factory.getApi().companiesIdShopsShopIdGet(shopId, id).await() }
    }

    suspend fun companiesPost(liveData : MutableLiveData<Resource<Void>>, body: CreateUserDto) {
        fetchData(liveData, true) { CompaniesApi.Factory.getApi().companiesPost(body).await() }
    }

    suspend fun companiesRequestVerifyPost(liveData : MutableLiveData<Resource<Void>>, body: EmailDto) {
        fetchData(liveData, true) { CompaniesApi.Factory.getApi().companiesRequestVerifyPost(body).await() }
    }
}