package com.app.coffeeclub.network.api;

import com.app.coffeeclub.models.AuthResponseDto;
import com.app.coffeeclub.models.Company;
import com.app.coffeeclub.models.CreateUserDto;
import com.app.coffeeclub.models.DeleteUserRequestDto;
import com.app.coffeeclub.models.EmailDto;
import com.app.coffeeclub.models.EphemeralKeyResponse;
import com.app.coffeeclub.models.LanguageRequest;
import com.app.coffeeclub.models.PaymentSourceDto;
import com.app.coffeeclub.models.ReferralExistsResponse;
import com.app.coffeeclub.models.ResetPasswordDto;
import com.app.coffeeclub.models.SignInDto;
import com.app.coffeeclub.models.SignInWithFbDto;
import com.app.coffeeclub.models.UpdateUserEmailDto;
import com.app.coffeeclub.models.UpdateUserPasswordDto;
import com.app.coffeeclub.models.User;
import com.app.coffeeclub.models.UserDto;
import com.app.coffeeclub.models.UserRatingRequestDto;
import com.app.coffeeclub.network.RetrofitHelper;
import com.google.gson.JsonObject;

import java.util.List;

import kotlinx.coroutines.Deferred;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface UsersApi {
  /**
   * 
   * 
   * @return Call&lt;AuthResponseDto&gt;
   */
  @POST("users/acceptCompanyInvite")
  Deferred<Response<AuthResponseDto>> usersAcceptCompanyInvitePost();
    

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("users/forgot")
  Deferred<Response<Void>> usersForgotPost(
                    @retrofit2.http.Body EmailDto body    
  );

  /**
   * 
   * 
   * @return Call&lt;List&lt;User&gt;&gt;
   */
  @GET("users")
  Deferred<Response<List<User>>> usersGet();
    

  /**
   * 
   * 
   * @return Call&lt;Company&gt;
   */
  @GET("users/me/company")
  Deferred<Response<Company>> usersMeCompanyGet();
    

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("users/me/delete")
  Deferred<Response<Void>> usersMeDelete(
                    @retrofit2.http.Body DeleteUserRequestDto body    
  );

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("users/me/email")
  Deferred<Response<Void>> usersMeEmailPut(
                    @retrofit2.http.Body UpdateUserEmailDto body    
  );

  /**
   * 
   * 
   * @param stripeVersion The stripe version of the client. (required)
   * @return Call&lt;EphemeralKeyResponse&gt;
   */
  @GET("users/me/ephemeral")
  Deferred<Response<EphemeralKeyResponse>> usersMeEphemeralGet(
        @retrofit2.http.Path("stripeVersion") String stripeVersion                
  );

  /**
   * 
   * 
   * @return Call&lt;User&gt;
   */
  @GET("users/me")
  Deferred<Response<User>> usersMeGet();
    

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("users/me/password")
  Deferred<Response<Void>> usersMePasswordPut(
                    @retrofit2.http.Body UpdateUserPasswordDto body    
  );

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;Object&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("users/me/paymentSource")
  Deferred<Response<Object>> usersMePaymentSourcePost(
                    @retrofit2.http.Body PaymentSourceDto body    
  );

  /**
   * 
   * 
   * @param token  (required)
   * @return Call&lt;Void&gt;
   */
  @PUT("users/me/pushToken/{token}")
  Deferred<Response<Void>> usersMePushTokenTokenPut(
            @retrofit2.http.Path("token") String token            
  );

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;User&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("users/me")
  Deferred<Response<User>> usersMePut(
                    @retrofit2.http.Body UserDto body    
  );

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("users/me/rating")
  Deferred<Response<Void>> usersMeRatingPut(
                    @retrofit2.http.Body UserRatingRequestDto body    
  );

  /**
   * 
   * 
   * @param referral Check if referral code exists. (required)
   * @return Call&lt;ReferralExistsResponse&gt;
   */
  @GET("users/referralCode")
  Deferred<Response<ReferralExistsResponse>> usersReferralCodeGet(
        @Query("referral") String referral
  );

  /**
   * 
   * 
   * @return Call&lt;AuthResponseDto&gt;
   */
  @GET("users/refreshToken")
  Deferred<Response<AuthResponseDto>> usersRefreshTokenGet();
    

  /**
   * 
   * 
   * @return Call&lt;Void&gt;
   */
  @GET("users/requestVerify")
  Deferred<Response<Void>> usersRequestVerifyGet();
    

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;AuthResponseDto&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("users/reset")
  Deferred<Response<AuthResponseDto>> usersResetPost(
                    @retrofit2.http.Body ResetPasswordDto body    
  );

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;AuthResponseDto&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("users/signIn")
  Deferred<Response<AuthResponseDto>> usersSignInPost(
                    @retrofit2.http.Body SignInDto body    
  );

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;AuthResponseDto&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("users/signInWithFb")
  Deferred<Response<AuthResponseDto>> usersSignInWithFbPost(
                    @retrofit2.http.Body SignInWithFbDto body    
  );

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;AuthResponseDto&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("users/signUp")
  Deferred<Response<AuthResponseDto>> usersSignUpPost(
                    @retrofit2.http.Body CreateUserDto body    
  );

  /**
   * 
   * 
   * @param token  (required)
   * @return Call&lt;Void&gt;
   */
  @GET("users/verify")
  Deferred<Response<Void>> usersVerifyGet(
        @retrofit2.http.Path("token") String token                
  );

  @POST("language")
  Deferred<Response<JsonObject>> getLanguage(@Body LanguageRequest request);

  class Factory {
    private static UsersApi instance;

    private static void create() {
      instance = RetrofitHelper.INSTANCE.getRetrofitBuilder().create(UsersApi.class);
    }

    public static synchronized UsersApi getApi() {
      if (instance == null) {
        create();
      }
      return instance;
    }
  }


}
