package com.app.coffeeclub.network.repository

import androidx.lifecycle.MutableLiveData
import com.app.coffeeclub.models.Claim
import com.app.coffeeclub.models.ClaimDto
import com.app.coffeeclub.models.ClaimsResponse
import com.app.coffeeclub.network.api.ClaimsApi
import dk.idealdev.utilities.response.Resource
import java.math.BigDecimal

object ClaimsRepository : BaseRepository() {

    suspend fun claimsGet(liveData : MutableLiveData<Resource<ClaimsResponse>>, shopId: BigDecimal, page: BigDecimal, limit: BigDecimal, forceRefresh : Boolean = false) {
        fetchData(liveData, forceRefresh) {
            ClaimsApi.Factory.getApi().claimsGet(shopId, page, limit).await()
        }
    }

    suspend fun claimsIdDelete(liveData : MutableLiveData<Resource<Void>>, id: String) {
        fetchData(liveData, true) {
            ClaimsApi.Factory.getApi().claimsIdDelete(id).await()
        }
    }

    suspend fun claimsPost(liveData : MutableLiveData<Resource<Claim>>, body: ClaimDto) {
        fetchData(liveData, true) {
            ClaimsApi.Factory.getApi().claimsPost(body).await()
        }
    }
}