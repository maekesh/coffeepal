package com.app.coffeeclub.network.api;

import com.app.coffeeclub.models.Company;
import com.app.coffeeclub.models.CompanyDto;
import com.app.coffeeclub.models.CreateUserDto;
import com.app.coffeeclub.models.EmailDto;
import com.app.coffeeclub.models.InviteDto;
import com.app.coffeeclub.models.Shop;
import com.app.coffeeclub.models.SignUpCompanyDto;
import com.app.coffeeclub.network.RetrofitHelper;

import java.math.BigDecimal;
import java.util.List;

import kotlinx.coroutines.Deferred;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface CompaniesApi {
  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("companies/complete")
  Deferred<Response<Void>> companiesCompletePost(
                    @retrofit2.http.Body SignUpCompanyDto body    
  );

  /**
   * 
   * 
   * @return Call&lt;List&lt;Company&gt;&gt;
   */
  @GET("companies")
  Deferred<Response<List<Company>>> companiesGet();
    

  /**
   * 
   * 
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @POST("companies/{id}/delete")
  Deferred<Response<Void>> companiesIdDeletePost(
            @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param employeeId  (required)
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("companies/{id}/employees/{employeeId}")
  Deferred<Response<Void>> companiesIdEmployeesEmployeeIdDelete(
            @retrofit2.http.Path("employeeId") BigDecimal employeeId            ,         @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param employeeId  (required)
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @PUT("companies/{id}/employees/{employeeId}")
  Deferred<Response<Void>> companiesIdEmployeesEmployeeIdPut(
            @retrofit2.http.Path("employeeId") BigDecimal employeeId            ,         @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param employeeId  (required)
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @POST("companies/{id}/employees/{employeeId}/resendInvite")
  Deferred<Response<Void>> companiesIdEmployeesEmployeeIdResendInvitePost(
            @retrofit2.http.Path("employeeId") BigDecimal employeeId            ,         @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param body  (required)
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("companies/{id}/employees")
  Deferred<Response<Void>> companiesIdEmployeesPost(
                    @retrofit2.http.Body InviteDto body    ,         @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param body  (required)
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("companies/{id}")
  Deferred<Response<Void>> companiesIdPut(
                    @retrofit2.http.Body CompanyDto body    ,         @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @GET("companies/{id}/shops")
  Deferred<Response<Void>> companiesIdShopsGet(
            @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param shopId  (required)
   * @param id  (required)
   * @return Call&lt;Shop&gt;
   */
  @GET("companies/{id}/shops/{shopId}")
  Deferred<Response<Shop>> companiesIdShopsShopIdGet(
            @retrofit2.http.Path("shopId") BigDecimal shopId            ,         @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("companies")
  Deferred<Response<Void>> companiesPost(
                    @retrofit2.http.Body CreateUserDto body    
  );

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("companies/requestVerify")
  Deferred<Response<Void>> companiesRequestVerifyPost(
                    @retrofit2.http.Body EmailDto body    
  );

  class Factory {
    private static CompaniesApi instance;

    private static void create() {
      instance = RetrofitHelper.INSTANCE.getRetrofitBuilder().create(CompaniesApi.class);
    }

    public static synchronized CompaniesApi getApi() {
      if (instance == null) {
        create();
      }
      return instance;
    }
  }



}
