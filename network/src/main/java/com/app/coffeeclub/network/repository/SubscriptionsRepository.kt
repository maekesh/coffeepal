package com.app.coffeeclub.network.repository

import androidx.lifecycle.MutableLiveData
import com.app.coffeeclub.models.PlanDto
import com.app.coffeeclub.models.StripeProduct
import com.app.coffeeclub.models.StripeSubscription
import com.app.coffeeclub.network.api.SubscriptionsApi
import dk.idealdev.utilities.response.Resource

object SubscriptionsRepository: BaseRepository() {

    suspend fun usersMeSubscriptionAvailableGet(liveData : MutableLiveData<Resource<List<StripeProduct>>>, forceRefresh : Boolean = false) {
        CompaniesRepository.fetchData(liveData, forceRefresh) {
            SubscriptionsApi.Factory.getApi().usersMeSubscriptionAvailableGet().await()
        }
    }

    suspend fun usersMeSubscriptionDelete(liveData : MutableLiveData<Resource<Void>>) {
        CompaniesRepository.fetchData(liveData, true) {
            SubscriptionsApi.Factory.getApi().usersMeSubscriptionDelete().await()
        }
    }

    suspend fun usersMeSubscriptionGet(liveData : MutableLiveData<Resource<StripeSubscription>>, forceRefresh : Boolean = false) {
        CompaniesRepository.fetchData(liveData, forceRefresh) {
            SubscriptionsApi.Factory.getApi().usersMeSubscriptionGet().await()
        }
    }

    suspend fun usersMeSubscriptionPost(liveData : MutableLiveData<Resource<Void>>, body: PlanDto) {
        CompaniesRepository.fetchData(liveData, true) {
            SubscriptionsApi.Factory.getApi().usersMeSubscriptionPost(body).await()
        }
    }

    suspend fun changeSubscriptionPlan(liveData : MutableLiveData<Resource<Void>>, body: PlanDto) {
        CompaniesRepository.fetchData(liveData, true) {
            SubscriptionsApi.Factory.getApi().usersChangeMeSubscriptionPost(body).await()
        }
    }
}