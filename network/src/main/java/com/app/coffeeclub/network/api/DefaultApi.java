package com.app.coffeeclub.network.api;

import kotlinx.coroutines.Deferred;
import retrofit2.Response;
import retrofit2.http.GET;

public interface DefaultApi {
  /**
   * 
   * 
   * @return Call&lt;Void&gt;
   */
  @GET("")
  Deferred<Response<Void>> rootGet();
    

}
