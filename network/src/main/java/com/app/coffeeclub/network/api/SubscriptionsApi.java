package com.app.coffeeclub.network.api;


import com.app.coffeeclub.models.PlanDto;
import com.app.coffeeclub.models.StripeProduct;
import com.app.coffeeclub.models.StripeSubscription;
import com.app.coffeeclub.network.RetrofitHelper;

import java.util.List;

import kotlinx.coroutines.Deferred;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface SubscriptionsApi {
  /**
   * 
   * 
   * @return Call&lt;List&lt;StripeProduct&gt;&gt;
   */
  @GET("users/me/subscription/available")
  Deferred<Response<List<StripeProduct>>> usersMeSubscriptionAvailableGet();
    

  /**
   * 
   * 
   * @return Call&lt;Void&gt;
   */
  @DELETE("users/me/subscription")
  Deferred<Response<Void>> usersMeSubscriptionDelete();


  /**
   * 
   * 
   * @return Call&lt;StripeSubscription&gt;
   */
  @GET("users/me/subscription")
  Deferred<Response<StripeSubscription>> usersMeSubscriptionGet();
    

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("users/me/subscription")
  Deferred<Response<Void>> usersMeSubscriptionPost(
                    @retrofit2.http.Body PlanDto body    
  );


  @POST("users/me/subscription/changeSubscriptions")
  Deferred<Response<Void>> usersChangeMeSubscriptionPost(
          @retrofit2.http.Body PlanDto body
  );


  class Factory {
    private static SubscriptionsApi instance;

    private static void create() {
      instance = RetrofitHelper.INSTANCE.getRetrofitBuilder().create(SubscriptionsApi.class);
    }

    public static synchronized SubscriptionsApi getApi() {
      if (instance == null) {
        create();
      }
      return instance;
    }
  }


}
