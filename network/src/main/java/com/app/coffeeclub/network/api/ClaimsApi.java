package com.app.coffeeclub.network.api;

import com.app.coffeeclub.models.Claim;
import com.app.coffeeclub.models.ClaimDto;
import com.app.coffeeclub.models.ClaimsResponse;
import com.app.coffeeclub.network.RetrofitHelper;

import java.math.BigDecimal;

import kotlinx.coroutines.Deferred;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ClaimsApi {
  /**
   * 
   * 
   * @param shopId Filter by shopId (optional)
   * @param page The page to return. Defaults to 1. (optional)
   * @param limit The amount of items in each response to return. Defaults to 10. (optional)
   * @return Call&lt;ClaimsResponse&gt;
   */
  @GET("claims")
  Deferred<Response<ClaimsResponse>> claimsGet(
        @retrofit2.http.Path("shopId") BigDecimal shopId                ,     @retrofit2.http.Path("page") BigDecimal page                ,     @retrofit2.http.Path("limit") BigDecimal limit                
  );

  /**
   * 
   * 
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("claims/{id}")
  Deferred<Response<Void>> claimsIdDelete(
            @retrofit2.http.Path("id") String id            
  );

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("claims")
  Deferred<Response<Claim>> claimsPost(
                    @retrofit2.http.Body ClaimDto body    
  );

  class Factory {
    private static ClaimsApi instance;

    private static void create() {
      instance = RetrofitHelper.INSTANCE.getRetrofitBuilder().create(ClaimsApi.class);
    }

    public static synchronized ClaimsApi getApi() {
      if (instance == null) {
        create();
      }
      return instance;
    }
  }
}
