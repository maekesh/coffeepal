package com.app.coffeeclub.network.repository

import androidx.lifecycle.MutableLiveData
import com.app.coffeeclub.models.*
import com.app.coffeeclub.network.api.ShopsApi
import dk.idealdev.utilities.response.Resource
import okhttp3.RequestBody
import java.math.BigDecimal

object ShopsRepository: BaseRepository() {

    suspend fun shopsAdminAnyGet(liveData : MutableLiveData<Resource<Void>>, forceRefresh : Boolean = false) {
        CompaniesRepository.fetchData(liveData, forceRefresh) {
            ShopsApi.Factory.getApi().shopsAdminAnyGet().await()
        }
    }

    suspend fun shopsGet(liveData : MutableLiveData<Resource<ShopsResponse>>, postalCode: String? = null, lng: BigDecimal? = null, lat: BigDecimal? = null, query: String? = null,  limit: BigDecimal, page: BigDecimal, forceRefresh : Boolean = false) {
        CompaniesRepository.fetchData(liveData, forceRefresh) {
            ShopsApi.Factory.getApi().shopsGet(postalCode, lng, lat, query, limit, page).await()
        }
    }

    suspend fun shopsIdApprovePost(liveData : MutableLiveData<Resource<Void>>, id: BigDecimal) {
        CompaniesRepository.fetchData(liveData, true) {
            ShopsApi.Factory.getApi().shopsIdActivatePost(id).await()
        }
    }

    suspend fun shopsIdDelete(liveData : MutableLiveData<Resource<Void>>, id: BigDecimal, forceRefresh : Boolean = false) {
        CompaniesRepository.fetchData(liveData, forceRefresh) {
            ShopsApi.Factory.getApi().shopsIdDelete(id).await()
        }
    }

    suspend fun shopsIdGet(liveData : MutableLiveData<Resource<Shop>>, id: BigDecimal, forceRefresh : Boolean = false) {
        CompaniesRepository.fetchData(liveData, forceRefresh) {
            ShopsApi.Factory.getApi().shopsIdGet(id).await()
        }
    }

    suspend fun shopsIdImageImageIdDelete(liveData : MutableLiveData<Resource<Void>>, imageId: String, id: BigDecimal, forceRefresh : Boolean = false) {
        CompaniesRepository.fetchData(liveData, forceRefresh) {
            ShopsApi.Factory.getApi().shopsIdImageImageIdDelete(imageId, id).await()
        }
    }

    suspend fun shopsIdImagePost(liveData : MutableLiveData<Resource<ImageFile>>, image: RequestBody, id: BigDecimal) {
        CompaniesRepository.fetchData(liveData, true) {
            ShopsApi.Factory.getApi().shopsIdImagePost(image, id).await()
        }
    }

    suspend fun shopsIdPaymentSetupIntentGet(liveData : MutableLiveData<Resource<Void>>, id: BigDecimal, forceRefresh : Boolean = false) {
        CompaniesRepository.fetchData(liveData, forceRefresh) {
            ShopsApi.Factory.getApi().shopsIdPaymentSetupIntentGet(id).await()
        }
    }

    suspend fun shopsIdPaymentSourcePut(liveData : MutableLiveData<Resource<Void>>, body: PaymentSourceDto, id: BigDecimal) {
        CompaniesRepository.fetchData(liveData, true) {
            ShopsApi.Factory.getApi().shopsIdPaymentSourcePut(body, id).await()
        }
    }

    suspend fun shopsIdPersonelGet(liveData : MutableLiveData<Resource<Void>>, id: BigDecimal, forceRefresh : Boolean = false) {
        CompaniesRepository.fetchData(liveData, forceRefresh) {
            ShopsApi.Factory.getApi().shopsIdPersonelGet(id).await()
        }
    }

    suspend fun shopsIdPersonelPersonelIdDelete(liveData : MutableLiveData<Resource<Void>>, personelId: BigDecimal, id: BigDecimal) {
        CompaniesRepository.fetchData(liveData, true) {
            ShopsApi.Factory.getApi().shopsIdPersonelPersonelIdDelete(personelId, id).await()
        }
    }

    suspend fun shopsIdPersonelPost(liveData : MutableLiveData<Resource<Void>>, body: ShopPersonelDto, id: BigDecimal) {
        CompaniesRepository.fetchData(liveData, true) {
            ShopsApi.Factory.getApi().shopsIdPersonelPost(body, id).await()
        }
    }

    suspend fun shopsIdPut(liveData : MutableLiveData<Resource<Shop>>, body: ShopDto, id: BigDecimal) {
        CompaniesRepository.fetchData(liveData, true) {
            ShopsApi.Factory.getApi().shopsIdPut(body, id).await()
        }
    }

    suspend fun shopsIdStatusPost(liveData : MutableLiveData<Resource<Void>>, id: BigDecimal) {
        CompaniesRepository.fetchData(liveData, true) {
            ShopsApi.Factory.getApi().shopsIdStatusPost(id).await()
        }
    }

    suspend fun shopsPost(liveData : MutableLiveData<Resource<Shop>>, body: ShopDto) {
        CompaniesRepository.fetchData(liveData, true) {
            ShopsApi.Factory.getApi().shopsPost(body).await()
        }
    }

    suspend fun shopsRegistrationFeeGet(liveData : MutableLiveData<Resource<Void>>, forceRefresh : Boolean = false) {
        CompaniesRepository.fetchData(liveData, forceRefresh) {
            ShopsApi.Factory.getApi().shopsRegistrationFeeGet().await()
        }
    }
}