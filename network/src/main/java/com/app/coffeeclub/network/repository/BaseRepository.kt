package com.app.coffeeclub.network.repository

import androidx.lifecycle.MutableLiveData
import com.app.coffeeclub.utilities.helpers.SharedPreferencesHelper
import dk.idealdev.utilities.livedata.LiveDataResourceHelper
import dk.idealdev.utilities.response.Resource
import retrofit2.HttpException
import retrofit2.Response

open class BaseRepository{

    suspend fun <T : Any> fetchData(livedata : MutableLiveData<Resource<T>>, forceRefresh : Boolean = false, call: suspend ()-> Response<T>)  {
        if(LiveDataResourceHelper.useCacheIfAvailable(livedata, forceRefresh)){
            return
        }

        livedata.postValue(Resource.loading())

        val result = safeApiResult(call = call)

        livedata.postValue(result)
    }

    suspend fun <T: Any> safeApiResult(call: suspend ()-> Response<T>) : Resource<T>{
        try {
            val response = call.invoke()
            if(response.isSuccessful) {

                val body = response.body()
                val bodyString = body.toString()

                //Handle errorCode from backend
                if(!bodyString.isBlank() && bodyString.contains("statusCode")){
                    if(!bodyString.contains("statusCode: 0")){
                        return Resource.error("body.statusCode != 0", HttpException(response))
                    }
                }

                return Resource.success(body)
            }

            return Resource.error(response.message(), HttpException(response))
        } catch (ex : Exception){
            return Resource.error(ex.message, ex)
        }
    }
}