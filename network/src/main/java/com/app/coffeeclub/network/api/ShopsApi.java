package com.app.coffeeclub.network.api;

import com.app.coffeeclub.models.ImageFile;
import com.app.coffeeclub.models.PaymentSourceDto;
import com.app.coffeeclub.models.Shop;
import com.app.coffeeclub.models.ShopDto;
import com.app.coffeeclub.models.ShopPersonelDto;
import com.app.coffeeclub.models.ShopsResponse;
import com.app.coffeeclub.network.RetrofitHelper;

import java.math.BigDecimal;

import kotlinx.coroutines.Deferred;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ShopsApi {
  /**
   * 
   * 
   * @return Call&lt;Void&gt;
   */
  @GET("shops/admin/any")
  Deferred<Response<Void>> shopsAdminAnyGet();
    

  /**
   * 
   * 
   * @param postalCode A postalcode to filter upon. Finds all shops with this postal code or within a reasonabledistance. This filter is singular and cannot be applied with lat, long or query filters. (optional)
   * @param lng Longitude coordinate to filter shops. Must have lat in query as well to take effect. (optional)
   * @param lat Latitude coordinate to filter shops. Must have lng in query as well to take effect. (optional)
   * @param query A search query to filter results on. (optional)
   * @param limit The amount of items in each response to return. Defaults to 10. (optional)
   * @param page The page to return. Defaults to 1. (optional)
   * @return Call&lt;ShopsResponse&gt;
   */
  @GET("shops")
  Deferred<Response<ShopsResponse>> shopsGet(
          @Query("postalCode") String postalCode,
          @Query("lng") BigDecimal lng,
          @Query("lat") BigDecimal lat,
          @Query("query") String query,
          @Query("limit") BigDecimal limit, 
          @Query("page") BigDecimal page
  );

  /**
   * 
   * 
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @POST("shops/{id}/activate")
  Deferred<Response<Void>> shopsIdActivatePost(
            @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("shops/{id}")
  Deferred<Response<Void>> shopsIdDelete(
            @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param id  (required)
   * @return Call&lt;Shop&gt;
   */
  @GET("shops/{id}")
  Deferred<Response<Shop>> shopsIdGet(
            @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param imageId  (required)
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("shops/{id}/image/{imageId}")
  Deferred<Response<Void>> shopsIdImageImageIdDelete(
            @retrofit2.http.Path("imageId") String imageId            ,         @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param image  (required)
   * @param id  (required)
   * @return Call&lt;ImageFile&gt;
   */
  @retrofit2.http.Multipart
  @POST("shops/{id}/image")
  Deferred<Response<ImageFile>> shopsIdImagePost(
                        @retrofit2.http.Part("image\"; filename=\"image") RequestBody image,         @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @GET("shops/{id}/paymentSetupIntent")
  Deferred<Response<Void>> shopsIdPaymentSetupIntentGet(
            @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param body  (required)
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("shops/{id}/paymentSource")
  Deferred<Response<Void>> shopsIdPaymentSourcePut(
                    @retrofit2.http.Body PaymentSourceDto body    ,         @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @GET("shops/{id}/personel")
  Deferred<Response<Void>> shopsIdPersonelGet(
            @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param personelId  (required)
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("shops/{id}/personel/{personelId}")
  Deferred<Response<Void>> shopsIdPersonelPersonelIdDelete(
            @retrofit2.http.Path("personelId") BigDecimal personelId            ,         @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param body  (required)
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("shops/{id}/personel")
  Deferred<Response<Void>> shopsIdPersonelPost(
                    @retrofit2.http.Body ShopPersonelDto body    ,         @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param body  (required)
   * @param id  (required)
   * @return Call&lt;Shop&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("shops/{id}")
  Deferred<Response<Shop>> shopsIdPut(
                    @retrofit2.http.Body ShopDto body    ,         @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param id  (required)
   * @return Call&lt;Void&gt;
   */
  @POST("shops/{id}/status")
  Deferred<Response<Void>> shopsIdStatusPost(
            @retrofit2.http.Path("id") BigDecimal id            
  );

  /**
   * 
   * 
   * @param body  (required)
   * @return Call&lt;Shop&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("shops")
  Deferred<Response<Shop>> shopsPost(
                    @retrofit2.http.Body ShopDto body    
  );

  /**
   * 
   * 
   * @return Call&lt;Void&gt;
   */
  @GET("shops/registrationFee")
  Deferred<Response<Void>> shopsRegistrationFeeGet();

  class Factory {
    private static ShopsApi instance;

    private static void create() {
      instance = RetrofitHelper.INSTANCE.getRetrofitBuilder().create(ShopsApi.class);
    }

    public static synchronized ShopsApi getApi() {
      if (instance == null) {
        create();
      }
      return instance;
    }
  }


}
