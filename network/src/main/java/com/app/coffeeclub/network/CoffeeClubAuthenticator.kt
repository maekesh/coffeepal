package com.app.coffeeclub.network

import android.content.Context
import com.app.coffeeclub.models.AuthResponseDto
import com.app.coffeeclub.network.api.UsersApi
import com.app.coffeeclub.utilities.helpers.AppResetHelper
import com.app.coffeeclub.utilities.helpers.SharedPreferencesHelper
import kotlinx.coroutines.*
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import timber.log.Timber

class CoffeeClubAuthenticator(val context: Context) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        if(response.request().header("Authorization") == null){
            return null
        }

        // Check if "the failed request Authorization key" is different from new authorization key
        // to prevent looping the request for new key
        if (response.request().header("Authorization") != null && response.request().header("Authorization") != "Bearer ${SharedPreferencesHelper.loadAccessToken().toString()}") {
            Timber.d("Skipping auth, new auth: %s", response.request().header("Authorization") )
            return null
        }

        Timber.d("Authenticating for response: %s", response)
        Timber.d("Challenges: %s", response.challenges())

        return runBlocking(Dispatchers.IO) {
            refreshData(response)
        }
    }

    private suspend fun refreshData(response: Response): Request? {
        val accessToken: String?
        val refreshCall = UsersApi.Factory.getApi().usersRefreshTokenGet()

        try {
            val responseCall = refreshCall.await()
            val responseRequest = responseCall.body() as AuthResponseDto
            val refreshToken = responseRequest.refreshToken

            if (refreshToken != null) {
                accessToken = responseRequest.token.toString()
                SharedPreferencesHelper.saveRefreshToken(refreshToken)
                SharedPreferencesHelper.saveAccessToken(responseRequest.token)
            } else {
                return handleRefreshFailed()
            }

        } catch (ex: Exception) {
            return handleRefreshFailed()
        }

        // retry the failed 401 request with new access token
        return response.request().newBuilder()
            .header("Authorization", "Bearer $accessToken") // use the new access token
            .build()
    }

    /**
     * Logs in the user in case of failed refresh call
     */
    private fun handleRefreshFailed() : Request? {
        Timber.e("handleRefreshFailed")
        AppResetHelper.resetApplication(context)
        return null
    }
}
