package com.app.coffeeclub.network

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.app.coffeeclub.utilities.helpers.GsonHelper
import com.app.coffeeclub.utilities.helpers.SharedPreferencesHelper
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit

import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Cache
import timber.log.Timber
import java.util.*

object RetrofitHelper {

    private const val timeout: Long = 30
    lateinit var context: Application

    fun init(application: Application) {
        this.context = application
    }

    fun getRetrofitBuilder(): Retrofit {
        val httpClient = OkHttpClient.Builder()

        setupTimeout(httpClient)
        setupCaching(httpClient)
        setupAuthorization(httpClient)
        setupLogging(httpClient)

        httpClient.authenticator(CoffeeClubAuthenticator(context.applicationContext))

        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(httpClient.build())
            .addConverterFactory(GsonConverterFactory.create(GsonHelper.newInstance()))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }

    private fun setupTimeout(httpClient: OkHttpClient.Builder) {
        httpClient.readTimeout(timeout, TimeUnit.SECONDS)
        httpClient.connectTimeout(timeout, TimeUnit.SECONDS)
        httpClient.writeTimeout(timeout, TimeUnit.SECONDS)
    }

    private fun setupLogging(httpClient: OkHttpClient.Builder) {
        val loggingInterceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        } else {
            loggingInterceptor.level = HttpLoggingInterceptor.Level.NONE
        }

        httpClient.addInterceptor(loggingInterceptor)
    }

    /**
     * Source: https://medium.com/mindorks/caching-with-retrofit-store-responses-offline-71439ed32fda
     */
    private fun setupCaching(httpClient: OkHttpClient.Builder) {
        val cacheSize = (5 * 1024 * 1024).toLong() //5 MB cache
        val myCache = Cache(context.applicationContext.cacheDir, cacheSize)
        httpClient.cache(myCache)

        httpClient.addInterceptor { chain ->
            var request = chain.request()

            // Add Cache Control only for GET methods
            if (request.method() == ("GET")) {
                /*
            *  Leveraging the advantage of using Kotlin,
            *  we initialize the request and change its header depending on whether
            *  the device is connected to Internet or not.
            */
                request = if (hasNetwork(context)!!) {
                    /*
            *  If there is Internet, get the cache that was stored 5 seconds ago.
            *  If the cache is older than 5 seconds, then discard it,
            *  and indicate an error in fetching the response.
            *  The 'max-age' attribute is responsible for this behavior.
            */
                    request.newBuilder().header("Cache-Control", "public, max-age=" + 5).build()
                } else {
                    /*
            *  If there is no Internet, get the cache that was stored 7 days ago.
            *  If the cache is older than 7 days, then discard it,
            *  and indicate an error in fetching the response.
            *  The 'max-stale' attribute is responsible for this behavior.
            *  The 'only-if-cached' attribute indicates to not retrieve new data; fetch the cache only instead.
            */
                    request.newBuilder().header(
                        "Cache-Control",
                        "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7
                    ).build()
                }
            }

            // Add the modified request to the chain.
            chain.proceed(request)
        }
    }

    private fun setupAuthorization(httpClient: OkHttpClient.Builder) {
        httpClient.addInterceptor { chain ->
            val original = chain.request()

            val requestBuilder = original.newBuilder()

            val sessionToken = SharedPreferencesHelper.loadAccessToken()

            val encodedPath = chain.request().url().encodedPath()

            if (encodedPath == "/users/refreshToken") {
                requestBuilder.header("Authorization", "Bearer ${SharedPreferencesHelper.loadRefreshToken()}")
            } else if (!sessionToken.isNullOrBlank()) {
                requestBuilder.header("Authorization", "Bearer $sessionToken")
                Timber.d("Performing request with Authorization header :)\n%s", original.url().toString())
            } else {
                Timber.e("Performing request without Authorization header! \n%s,", original.url().toString())
            }

            requestBuilder.header("Accept-Language", Locale.getDefault().language)

            val request = requestBuilder.build()
            chain.proceed(request)
        }
    }

    private fun hasNetwork(context: Context): Boolean? {
        var isConnected: Boolean? = false
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        if (activeNetwork != null && activeNetwork.isConnected) {
            isConnected = true
        }
        return isConnected
    }
}