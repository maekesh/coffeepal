package com.app.coffeeclub.network.repository

import androidx.lifecycle.MutableLiveData
import com.app.coffeeclub.models.*
import com.app.coffeeclub.network.api.UsersApi
import com.google.gson.JsonObject
import dk.idealdev.utilities.response.Resource
import okhttp3.ResponseBody

object UsersRepository: BaseRepository() {

    suspend fun usersAcceptCompanyInvitePost(liveData : MutableLiveData<Resource<AuthResponseDto>>) {
        fetchData(liveData, true) {
            UsersApi.Factory.getApi().usersAcceptCompanyInvitePost().await()
        }
    }

    suspend fun usersForgotPost(liveData : MutableLiveData<Resource<Void>>, body: EmailDto) {
        fetchData(liveData, true) {
            UsersApi.Factory.getApi().usersForgotPost(body).await()
        }
    }

    suspend fun usersGet(liveData : MutableLiveData<Resource<List<User>>>, forceRefresh : Boolean = false) {
        fetchData(liveData, forceRefresh) {
            UsersApi.Factory.getApi().usersGet().await()
        }
    }

    suspend fun usersMeCompanyGet(liveData : MutableLiveData<Resource<Company>>, forceRefresh : Boolean = false) {
        fetchData(liveData, forceRefresh) {
            UsersApi.Factory.getApi().usersMeCompanyGet().await()
        }
    }

    suspend fun usersMeGet(liveData : MutableLiveData<Resource<User>>, forceRefresh : Boolean = false) {
        fetchData(liveData, forceRefresh) {
            UsersApi.Factory.getApi().usersMeGet().await()
        }
    }

    suspend fun usersMePasswordPut(liveData : MutableLiveData<Resource<Void>>, body: UpdateUserPasswordDto) {
        fetchData(liveData, true) {
            UsersApi.Factory.getApi().usersMePasswordPut(body).await()
        }
    }

    suspend fun usersMePaymentSourcePost(liveData : MutableLiveData<Resource<Any>>, body: PaymentSourceDto) {
        fetchData(liveData, true) {
            UsersApi.Factory.getApi().usersMePaymentSourcePost(body).await()
        }
    }

    suspend fun usersMePushTokenTokenPut(liveData : MutableLiveData<Resource<Void>>, token: String) {
        fetchData(liveData, true) {
            UsersApi.Factory.getApi().usersMePushTokenTokenPut(token).await()
        }
    }

    suspend fun usersMePut(liveData : MutableLiveData<Resource<User>>, body: UserDto) {
        fetchData(liveData, true) {
            UsersApi.Factory.getApi().usersMePut(body).await()
        }
    }

    suspend fun usersMeRatingPut(liveData : MutableLiveData<Resource<Void>>, body: UserRatingRequestDto) {
        fetchData(liveData, true) {
            UsersApi.Factory.getApi().usersMeRatingPut(body).await()
        }
    }

    suspend fun usersMeDelete(liveData : MutableLiveData<Resource<Void>>, body: DeleteUserRequestDto) {
        fetchData(liveData, true) {
            UsersApi.Factory.getApi().usersMeDelete(body).await()
        }
    }

    suspend fun usersMeEmailPut(liveData : MutableLiveData<Resource<Void>>, body: UpdateUserEmailDto) {
        fetchData(liveData, true) {
            UsersApi.Factory.getApi().usersMeEmailPut(body).await()
        }
    }

    suspend fun usersMeEphemeralGet(liveData : MutableLiveData<Resource<EphemeralKeyResponse>>, stripeVersion: String, forceRefresh : Boolean = false) {
        fetchData(liveData, forceRefresh) {
            UsersApi.Factory.getApi().usersMeEphemeralGet(stripeVersion).await()
        }
    }

    suspend fun usersRefreshTokenGet(liveData : MutableLiveData<Resource<AuthResponseDto>>, forceRefresh : Boolean = false) {
        fetchData(liveData, forceRefresh) {
            UsersApi.Factory.getApi().usersRefreshTokenGet().await()
        }
    }

    suspend fun usersRequestVerifyGet(liveData : MutableLiveData<Resource<Void>>, forceRefresh : Boolean = false) {
        fetchData(liveData, forceRefresh) {
            UsersApi.Factory.getApi().usersRequestVerifyGet().await()
        }
    }

    suspend fun usersResetPost(liveData : MutableLiveData<Resource<AuthResponseDto>>, body: ResetPasswordDto) {
        fetchData(liveData, true) {
            UsersApi.Factory.getApi().usersResetPost(body).await()
        }
    }

    suspend fun usersSignInPost(liveData : MutableLiveData<Resource<AuthResponseDto>>, body: SignInDto) {
        fetchData(liveData, true) {
            UsersApi.Factory.getApi().usersSignInPost(body).await()
        }
    }

    suspend fun usersSignInWithFbPost(liveData : MutableLiveData<Resource<AuthResponseDto>>, body: SignInWithFbDto) {
        fetchData(liveData, true) {
            UsersApi.Factory.getApi().usersSignInWithFbPost(body).await()
        }
    }

    suspend fun usersSignUpPost(liveData : MutableLiveData<Resource<AuthResponseDto>>, body: CreateUserDto) {
        fetchData(liveData, true) {
            UsersApi.Factory.getApi().usersSignUpPost(body).await()
        }
    }

    suspend fun usersVerifyGet(liveData : MutableLiveData<Resource<Void>>, token: String, forceRefresh : Boolean = false) {
        fetchData(liveData, forceRefresh) {
            UsersApi.Factory.getApi().usersVerifyGet(token).await()
        }
    }

    suspend fun usersReferralCodeGet(liveData : MutableLiveData<Resource<ReferralExistsResponse>>, referralCode: String){
        fetchData(liveData, true) {
            UsersApi.Factory.getApi().usersReferralCodeGet(referralCode).await()
        }
    }

    suspend fun getLanguageFromServer(languageLiveData: MutableLiveData<Resource<JsonObject>>, language: LanguageRequest ) {
        fetchData(languageLiveData, true) {
            UsersApi.Factory.getApi().getLanguage(language).await()
        }
    }

}