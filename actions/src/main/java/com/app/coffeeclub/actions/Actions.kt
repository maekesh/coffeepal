package com.app.coffeeclub.actions

import android.content.Context
import android.content.Intent

object Actions {

    const val ARG_REQUEST_PERMISSIONS = "ARG_REQUEST_PERMISSIONS"
    const val ARG_GO_TO_SIGNUP = "ARG_GO_TO_SIGNUP"
    const val ARG_GO_TO_SIGNIN = "ARG_GO_TO_SIGNIN"
    const val ARG_GO_TO_SETUP_SUBSCRIPTION = "ARG_GO_TO_SETUP_SUBSCRIPTION"
    const val ARG_HAS_CREDIT_CARD = "ARG_HAS_CREDIT_CARD"

    fun getHomeIntentNewUser(context: Context): Intent {
        return internalIntent(context, "com.app.coffeeclub.home.open")
                .putExtra(ARG_REQUEST_PERMISSIONS, true)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
    }
    fun getHomeIntent(context: Context): Intent = internalIntent(context, "com.app.coffeeclub.home.open")
    fun getOnboardingIntent(context: Context, goToSignup : Boolean = false, goToSignIn : Boolean = false, goToSetupSubscription : Boolean = false, hasCreditCard : Boolean): Intent = internalIntent(context, "com.app.coffeeclub.onboarding.OnboardingActivity")
            .putExtra(ARG_GO_TO_SIGNUP, goToSignup)
            .putExtra(ARG_GO_TO_SIGNIN, goToSignIn)
            .putExtra(ARG_GO_TO_SETUP_SUBSCRIPTION, goToSetupSubscription)
            .putExtra(ARG_HAS_CREDIT_CARD, hasCreditCard)

    private fun internalIntent(context: Context, action: String) = Intent(action).setPackage(context.packageName)
}