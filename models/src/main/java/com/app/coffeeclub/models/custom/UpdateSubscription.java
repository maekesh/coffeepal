package com.app.coffeeclub.models.custom;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class UpdateSubscription {

    @SerializedName("id")
    @Expose
    private int id = 0;

    @SerializedName("subscriptionId")
    @Expose
    private String subscriptionId;

    @SerializedName("newStartDate")
    @Expose
    private Date newStartDate;

    @SerializedName("plan")
    @Expose
    private String plan;

    @SerializedName("duration")
    @Expose
    private String duration;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("stripeCustomerId")
    @Expose
    private String stripeCustomerId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public Date getNewStartDate() {
        return newStartDate;
    }

    public void setNewStartDate(Date newStartDate) {
        this.newStartDate = newStartDate;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStripeCustomerId() {
        return stripeCustomerId;
    }

    public void setStripeCustomerId(String stripeCustomerId) {
        this.stripeCustomerId = stripeCustomerId;
    }
}
