/*
 * Coffee Club API
 * API documentation for the Coffee Club API
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.app.coffeeclub.models;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import android.os.Parcelable;
import android.os.Parcel;

/**
 * CreateUserDto
 */

public class CreateUserDto {

  @SerializedName("email")
  private String email = null;

  @SerializedName("firstName")
  private String firstName = null;

  @SerializedName("lastName")
  private String lastName = null;

  @SerializedName("phone")
  private String phone = null;

  @SerializedName("password")
  private String password = null;
  public CreateUserDto email(String email) {
    this.email = email;
    return this;
  }

  @SerializedName("step")
  private int step;

  public int getStep() {
    return step;
  }

  public void setStep(int step) {
    this.step = step;
  }

  /**
  * Get email
  * @return email
  **/
  @Schema(required = true, description = "")
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public CreateUserDto firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  

  /**
  * Get firstName
  * @return firstName
  **/
  @Schema(required = true, description = "")
  public String getFirstName() {
    return firstName;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  public CreateUserDto lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  

  /**
  * Get lastName
  * @return lastName
  **/
  @Schema(required = true, description = "")
  public String getLastName() {
    return lastName;
  }
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  public CreateUserDto phone(String phone) {
    this.phone = phone;
    return this;
  }

  

  /**
  * Get phone
  * @return phone
  **/
  @Schema(required = true, description = "")
  public String getPhone() {
    return phone;
  }
  public void setPhone(String phone) {
    this.phone = phone;
  }
  public CreateUserDto password(String password) {
    this.password = password;
    return this;
  }

  

  /**
  * Get password
  * @return password
  **/
  @Schema(required = true, description = "")
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateUserDto createUserDto = (CreateUserDto) o;
    return Objects.equals(this.email, createUserDto.email) &&
        Objects.equals(this.firstName, createUserDto.firstName) &&
        Objects.equals(this.lastName, createUserDto.lastName) &&
        Objects.equals(this.phone, createUserDto.phone) &&
        Objects.equals(this.password, createUserDto.password);
  }

  @Override
  public int hashCode() {
    return Objects.hash(email, firstName, lastName, phone, password);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateUserDto {\n");
    
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public void writeToParcel(Parcel out, int flags) {
    
    out.writeValue(email);
    out.writeValue(firstName);
    out.writeValue(lastName);
    out.writeValue(phone);
    out.writeValue(password);
  }

  public CreateUserDto() {
    super();
  }

  CreateUserDto(Parcel in) {
    
    email = (String)in.readValue(null);
    
    firstName = (String)in.readValue(null);
    
    lastName = (String)in.readValue(null);
    
    phone = (String)in.readValue(null);
    
    password = (String)in.readValue(null);
    
  }

  public int describeContents() {
    return 0;
  }

  public static final Parcelable.Creator<CreateUserDto> CREATOR = new Parcelable.Creator<CreateUserDto>() {
    public CreateUserDto createFromParcel(Parcel in) {
      return new CreateUserDto(in);
    }
    public CreateUserDto[] newArray(int size) {
      return new CreateUserDto[size];
    }
  };
}
