package com.app.coffeeclub.models

import com.google.gson.annotations.SerializedName
import org.json.JSONObject

data class LanguageRequest(
        @SerializedName("language")
        val language: String
)