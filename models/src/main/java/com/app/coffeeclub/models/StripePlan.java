/*
 * Coffee Club API
 * API documentation for the Coffee Club API
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package com.app.coffeeclub.models;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import java.math.BigDecimal;
import android.os.Parcelable;
import android.os.Parcel;

/**
 * StripePlan
 */

public class StripePlan {

  @SerializedName("id")
  private String id = null;

  @SerializedName("amount")
  private BigDecimal amount = null;

  @SerializedName("currency")
  private String currency = null;

  @SerializedName("nickname")
  private String nickname = null;

  @SerializedName("interval")
  private String interval = null;

  public String getInterval() {
    return interval;
  }

  public void setInterval(String interval) {
    this.interval = interval;
  }

  public StripePlan id(String id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public static Parcelable.Creator<StripePlan> getCREATOR() {
    return CREATOR;
  }

  @SerializedName("name")
  private String name = null;

  

  /**
  * Get id
  * @return id
  **/
  @Schema(required = true, description = "")
  public String getId() {
    return id;
  }
  public void setId(String id) {
    this.id = id;
  }
  public StripePlan amount(BigDecimal amount) {
    this.amount = amount;
    return this;
  }

  

  /**
  * price in cents
  * @return amount
  **/
  @Schema(example = "9900.0", required = true, description = "price in cents")
  public BigDecimal getAmount() {
    return amount;
  }
  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }
  public StripePlan currency(String currency) {
    this.currency = currency;
    return this;
  }

  

  /**
  * Currency 
  * @return currency
  **/
  @Schema(example = "dkk", required = true, description = "Currency ")
  public String getCurrency() {
    return currency;
  }
  public void setCurrency(String currency) {
    this.currency = currency;
  }
  public StripePlan nickname(String nickname) {
    this.nickname = nickname;
    return this;
  }

  

  /**
  * Get nickname
  * @return nickname
  **/
  @Schema(required = true, description = "")
  public String getNickname() {
    return nickname;
  }
  public void setNickname(String nickname) {
    this.nickname = nickname;
  }
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StripePlan stripePlan = (StripePlan) o;
    return Objects.equals(this.id, stripePlan.id) &&
        Objects.equals(this.amount, stripePlan.amount) &&
        Objects.equals(this.currency, stripePlan.currency) &&
        Objects.equals(this.nickname, stripePlan.nickname);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, amount, currency, nickname);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StripePlan {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    nickname: ").append(toIndentedString(nickname)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  public void writeToParcel(Parcel out, int flags) {
    
    out.writeValue(id);
    out.writeValue(amount);
    out.writeValue(currency);
    out.writeValue(nickname);
  }

  public StripePlan() {
    super();
  }

  StripePlan(Parcel in) {
    
    id = (String)in.readValue(null);
    
    
    amount = (BigDecimal)in.readValue(BigDecimal.class.getClassLoader());
    currency = (String)in.readValue(null);
    
    nickname = (String)in.readValue(null);
    
  }

  public int describeContents() {
    return 0;
  }

  public static final Parcelable.Creator<StripePlan> CREATOR = new Parcelable.Creator<StripePlan>() {
    public StripePlan createFromParcel(Parcel in) {
      return new StripePlan(in);
    }
    public StripePlan[] newArray(int size) {
      return new StripePlan[size];
    }
  };
}
