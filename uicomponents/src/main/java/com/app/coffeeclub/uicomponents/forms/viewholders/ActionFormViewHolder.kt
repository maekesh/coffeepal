package com.app.coffeeclub.uicomponents.forms.viewholders

import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import com.app.coffeeclub.uicomponents.forms.items.ActionFormItem
import com.app.coffeeclub.uicomponents.forms.items.FormItem
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.getStringData

class ActionFormViewHolder(itemView: View): FormViewHolder(itemView) {

    override fun bind(item: FormItem, itemCount: Int) {
        super.bind(item, itemCount)

    }

    override fun setFieldReadOnly(item: FormItem) {
        super.setFieldReadOnly(item)
        if(item is ActionFormItem && item.isBtn) {
            setBtn(item)
            ivArrowRight.isVisible = !item.isBtn
        } else  {
            ivArrowRight.isVisible = true
        }

        if (item.hint.isNotBlank()) {
            tvValue.isVisible = true
            tvValue.isEnabled = false
        } 
    }

    private fun setBtn(item: ActionFormItem) {
        tvValue.isGone = item.isBtn
        tvLabel.isGone = item.isBtn
        tvBtn.isVisible = item.isBtn

//        tvBtn.text = item.label
        tvBtn.text = getStringData(item.label, Language.getLanguageHashMap())

        itemView.setOnClickListener(null)

        tvBtn.setOnClickListener {
            item.clickCallback?.invoke(adapterPosition)
        }

        item.btnTextColor?.let {
            tvBtn.setTextColor(it)
        }
    }
}