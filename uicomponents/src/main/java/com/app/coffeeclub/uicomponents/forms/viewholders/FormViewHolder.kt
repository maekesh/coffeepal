package com.app.coffeeclub.uicomponents.forms.viewholders

import android.text.InputFilter
import android.text.InputType
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.app.coffeeclub.models.UserDto
import com.app.coffeeclub.uicomponents.R
import com.app.coffeeclub.uicomponents.forms.FormatType
import com.app.coffeeclub.uicomponents.forms.items.FormItem
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.getLocalizationText
import com.app.coffeeclub.utilities.extensions.getStringData
import com.app.coffeeclub.utilities.extensions.showHintText
import com.app.coffeeclub.utilities.extensions.showText
import com.app.coffeeclub.utilities.helpers.CreditCardHelper
import com.app.coffeeclub.utilities.helpers.DateHelper
import com.app.coffeeclub.utilities.textwatchers.CreditCardExpiryTextWatcher
import com.app.coffeeclub.utilities.textwatchers.CreditCardFormattingTextWatcher
import dk.idealdev.utilities.afterTextChanged
import java.util.*

open class FormViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    protected lateinit var tvBtn: TextView
    protected lateinit var tvLabel: TextView
    protected lateinit var tvValue: TextView
    protected lateinit var etValue: EditText
    protected lateinit var ivStatus: ImageView
    protected lateinit var ivArrowRight: ImageView

    open fun bind(item: FormItem, itemCount: Int) {
        tvBtn = itemView.findViewById(R.id.tvBtn)
        tvLabel = itemView.findViewById(R.id.tvLabel)
        tvValue = itemView.findViewById(R.id.tvValue)
        etValue = itemView.findViewById(R.id.etValue)
        ivStatus = itemView.findViewById(R.id.ivStatus)
        ivArrowRight = itemView.findViewById(R.id.ivArrowRight)

//        tvLabel.text = item.label
        tvLabel.showText(item.label, Language.getLanguageHashMap())
        etValue.showHintText(item.hint, Language.getLanguageHashMap())
//        etValue.hint = item.hint

        val isReadOnly = item.clickCallback != null || item.isReadOnly

        setMaxLength(item)
        setFocusChangeListener(item)
        setFormatting(item, isReadOnly)
        setTextContent(item)
        setImeOptions(itemCount, item)
        setInputType(item)

        if (isReadOnly) {
            setFieldReadOnly(item)
        } else {
            setFieldEditable(item)
        }
    }

    private fun setMaxLength(item: FormItem) {
        val maxLength = item.maxLength
        if (maxLength != null) {
            etValue.filters = arrayOf(InputFilter.LengthFilter(maxLength))
        }
    }

    private fun setFocusChangeListener(item: FormItem) {
        etValue.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            ivStatus.visibility = if (hasFocus) View.VISIBLE else View.INVISIBLE
            tvLabel.setTextColor(ContextCompat.getColor(itemView.context,
                    if (hasFocus) {
                        R.color.purple_light
                    } else {
                        R.color.gray100
                    }))

            onFocusChange(item, hasFocus)
        }
        ivStatus.visibility = View.INVISIBLE
    }

    open fun onFocusChange(item: FormItem, hasFocus: Boolean) {}

    private fun setFormatting(item: FormItem, isReadOnly: Boolean) {
        if (isReadOnly) {
            setFormattingTextView(item)
        } else {
            setFormattingEditText(item)
        }
    }

    private fun setFormattingTextView(item: FormItem) {
        val value = item.valueLiveData?.value as? String ?: return
        when (item.type) {
            FormatType.Standard -> {
            }
            FormatType.ExpiryDate ->
                CreditCardHelper.formatExpiryString(value, textView = tvValue)

            FormatType.CardNumber ->
                CreditCardHelper.formatCreditCard(value, textView = tvValue)
        }
    }

    private fun setFormattingEditText(item: FormItem) {
        when (item.type) {
            FormatType.Standard -> {
            }
            FormatType.ExpiryDate ->
                etValue.addTextChangedListener(CreditCardExpiryTextWatcher(etValue))

            FormatType.CardNumber ->
                etValue.addTextChangedListener(CreditCardFormattingTextWatcher(etValue))
        }
    }

    private fun setTextContent(item: FormItem) {
        val value = item.valueLiveData?.value ?: return
        when {
            value is String && !value.isBlank() -> etValue.setText(value)
            value is Date -> etValue.setText(DateHelper.formatDate(value))
            value is UserDto.SexEnum -> {
                var genderData = ""
                when (value) {
                    UserDto.SexEnum.MALE -> {
                        genderData = getLocalizationText(itemView.context.getString(R.string.sign_up_3_gender_option_2), Language.getLanguageHashMap())
                    }
                    UserDto.SexEnum.FEMALE -> {
                        genderData = getLocalizationText(itemView.context.getString(R.string.sign_up_3_gender_option_1), Language.getLanguageHashMap())
                    }
                    UserDto.SexEnum.OTHER -> {
                        genderData = getLocalizationText(itemView.context.getString(R.string.sign_up_3_gender_option_3), Language.getLanguageHashMap())
                    }
                }
                etValue.setText(genderData)
            }
        }
    }

    private fun setImeOptions(itemCount: Int, item: FormItem) {
        if (adapterPosition == itemCount - 1) {
            etValue.imeOptions = EditorInfo.IME_ACTION_DONE
        } else if ( getStringData(item.label, Language.getLanguageHashMap())== getStringData(itemView.context.getString(R.string.sign_up_3_label_4), Language.getLanguageHashMap())) {
            etValue.imeOptions = EditorInfo.IME_ACTION_DONE
        } else {
            etValue.imeOptions = EditorInfo.IME_ACTION_NEXT
        }
    }

    private fun setInputType(item: FormItem) {
        val inputType = item.inputType

        if (inputType != null) {
            etValue.inputType = InputType.TYPE_CLASS_TEXT or inputType
            //EditTexts with password getSubscriptionType has another font, so override that
            if (inputType == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
                etValue.typeface = ResourcesCompat.getFont(itemView.context, R.font.open_sans_regular)
            }
        } else {
            etValue.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_WORDS
        }
    }

    open fun setFieldReadOnly(item: FormItem) {
        etValue.isVisible = false
        tvValue.isVisible = true


        if (!etValue.text.isNullOrBlank()) {
            tvValue.setTextColor(ContextCompat.getColor(itemView.context, R.color.gray100))
            tvValue.text = etValue.text
        } else {
            tvValue.setTextColor(ContextCompat.getColor(itemView.context, R.color.gray500))
            tvValue.text = etValue.hint
            if (etValue.hint == "null") {
                tvValue.isVisible = false
            }

        }

        itemView.setOnClickListener {
            item.clickCallback?.invoke(adapterPosition)
        }
    }

    private fun setFieldEditable(item: FormItem) {
        etValue.afterTextChanged {
            item.valueLiveData?.value = it
        }

        etValue.isVisible = true
        tvValue.isVisible = false

        itemView.setOnClickListener {
            etValue.requestFocus()
        }
    }
}