package com.app.coffeeclub.uicomponents.forms.items

import androidx.lifecycle.MutableLiveData
import com.app.coffeeclub.uicomponents.forms.FormatType

open class FormItem(
    val valueLiveData : MutableLiveData<out Any>? = null,
    val label : String,
    val hint : String = "",
    val inputType: Int? = null,
    val maxLength : Int? = null,
    val type : FormatType = FormatType.Standard,
    val clickCallback : ((position : Int) -> Unit)? = null,
    val isReadOnly : Boolean = true) {

    override fun equals(other: Any?): Boolean {
        val otherItem = other as FormItem
        return label == otherItem.label && hint == otherItem.hint && type == otherItem.type && valueLiveData?.value == otherItem.valueLiveData?.value && clickCallback == otherItem.clickCallback
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}