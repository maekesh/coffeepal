package com.app.coffeeclub.uicomponents.forms.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.app.coffeeclub.uicomponents.forms.items.FormItem
import com.app.coffeeclub.uicomponents.R
import com.app.coffeeclub.uicomponents.forms.viewholders.FormViewHolder

class FormAdapter :
    ListAdapter<FormItem, FormViewHolder>(MutableLiveDataDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = FormViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.row_form, parent, false)
    )

    override fun onBindViewHolder(holder: FormViewHolder, position: Int) = holder.bind(getItem(position), itemCount)

    private class MutableLiveDataDiff : DiffUtil.ItemCallback<FormItem>() {
        override fun areItemsTheSame(oldItem: FormItem, newItem: FormItem): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: FormItem, newItem: FormItem): Boolean {
            return oldItem == newItem
        }
    }
}
