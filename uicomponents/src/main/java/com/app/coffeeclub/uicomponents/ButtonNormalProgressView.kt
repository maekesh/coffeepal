package com.app.coffeeclub.uicomponents

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.isGone
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.button_large_progress.view.*

class ButtonNormalProgressView(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
    FrameLayout(context, attrs, defStyleAttr) {

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    init {
        LayoutInflater.from(context).inflate(R.layout.button_large_progress, this, true)

        if (attrs != null && !isInEditMode) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.ButtonNormalProgressView)
            if (a.hasValue(R.styleable.ButtonNormalProgressView_text)) {
                val buttonText = a.getString(R.styleable.ButtonNormalProgressView_text)
                tvBtn.text = buttonText
            }
            if (a.hasValue(R.styleable.ButtonNormalProgressView_buttonEnabled)) {
                val buttonEnabled = a.getBoolean(R.styleable.ButtonNormalProgressView_buttonEnabled, true)
                tvBtn.isEnabled = buttonEnabled
            }
            a.recycle()
        }
    }

    fun setText(string: String) {
        tvBtn.text = string
    }

    fun setButtonEnabled(enabled : Boolean){
        tvBtn.isEnabled = enabled
    }

    fun setOnButtonClickListener(clickListener: () -> Unit) {
        tvBtn.setOnClickListener { clickListener.invoke() }
    }

    fun setProgressState(showProgress: Boolean) {
        tvBtn.isEnabled = !showProgress
        if (showProgress) {
            showProgress()
        } else {
            hideProgress()
        }
    }

    private fun showProgress() {
        pbButton.isVisible = true
    }

    private fun hideProgress() {
        pbButton.isGone = true
    }
}