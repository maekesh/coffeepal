package com.app.coffeeclub.uicomponents.forms.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.app.coffeeclub.uicomponents.R
import com.app.coffeeclub.uicomponents.forms.items.ActionFormItem
import com.app.coffeeclub.uicomponents.forms.viewholders.ActionFormViewHolder
import com.app.coffeeclub.uicomponents.forms.viewholders.FormViewHolder

class ActionFormAdapter :
    ListAdapter<ActionFormItem, ActionFormViewHolder>(MutableLiveDataDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ActionFormViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.row_form, parent, false)
    )

    override fun onBindViewHolder(holder: ActionFormViewHolder, position: Int) = holder.bind(getItem(position), itemCount)

    private class MutableLiveDataDiff : DiffUtil.ItemCallback<ActionFormItem>() {
        override fun areItemsTheSame(oldItem: ActionFormItem, newItem: ActionFormItem): Boolean {
            return oldItem.valueLiveData?.value == newItem.valueLiveData?.value
        }

        override fun areContentsTheSame(oldItem: ActionFormItem, newItem: ActionFormItem): Boolean {
            return oldItem == newItem
        }
    }
}
