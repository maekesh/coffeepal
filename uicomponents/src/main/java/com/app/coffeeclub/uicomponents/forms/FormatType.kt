package com.app.coffeeclub.uicomponents.forms

enum class FormatType{
    Standard,
    ExpiryDate,
    CardNumber
}