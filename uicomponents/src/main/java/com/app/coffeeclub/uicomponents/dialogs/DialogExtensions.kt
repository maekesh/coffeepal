package com.app.coffeeclub.uicomponents.dialogs

import android.app.AlertDialog
import android.content.Context

fun showCoffeeClubDialog(context: Context): CoffeeClubDialog {
    val dialog = CoffeeClubDialog(context)
    dialog.showDialog()
    return dialog
}

fun showAlertDialog(context: Context, dialogBuilder: AlertDialog.Builder.() -> Unit) {
    val builder = AlertDialog.Builder(context)
    builder.dialogBuilder()
    val dialog = builder.create()
    dialog.show()
}

fun AlertDialog.Builder.positiveButton(resourceId: Int, handleClick: (which: Int) -> Unit = {}) {
    this.setPositiveButton(context.getString(resourceId)) { _, which -> handleClick(which) }
}

fun AlertDialog.Builder.negativeButton(resourceId: Int, handleClick: (which: Int) -> Unit = {}) {
    this.setNegativeButton(context.getString(resourceId)) { _, which -> handleClick(which) }
}
