package com.app.coffeeclub.uicomponents.forms.items

import android.os.Bundle
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import com.app.coffeeclub.uicomponents.forms.FormatType

class InputFormItem(valueLiveData : MutableLiveData<out Any>,
                    label : String,
                    hint : String,
                    inputType: Int? = null,
                    maxLength : Int? = null,
                    type : FormatType = FormatType.Standard,
                    clickCallback : ((position : Int) -> Unit)? = null,
                    val validation: (etValue: EditText, tvLabel: TextView, ivStatus: ImageView, hasFocus: Boolean, isNotFinalCheck: Boolean) -> Boolean,
                    val autofillIdBundle: Bundle? = null,
                    val autoFillHint: String? = null)
    : FormItem(valueLiveData, label, hint, inputType, maxLength, type, clickCallback, false)