package com.app.coffeeclub.uicomponents.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.coffeeclub.models.UserDto
import com.app.coffeeclub.resources.R.color
import com.app.coffeeclub.resources.R.string
import com.app.coffeeclub.uicomponents.R
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.getLocalizationText
import com.app.coffeeclub.utilities.extensions.showText
import com.app.coffeeclub.utilities.viewmodels.GenderSelectionViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_genderitem_list_dialog.*
import kotlinx.android.synthetic.main.fragment_genderitem_list_dialog_item.view.*


class GenderItemListDialogFragment : BottomSheetDialogFragment() {

    val genderViewModel: GenderSelectionViewModel by lazy {
        ViewModelProviders.of(requireActivity()).get(GenderSelectionViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_genderitem_list_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        genderTitleTv.showText(getString(string.sign_up_3_placeholder_6), Language.getLanguageHashMap())
        rvGenderList.layoutManager = LinearLayoutManager(context)
        rvGenderList.adapter = GenderItemAdapter(3, genderViewModel.selectedGender.value?.ordinal)
    }

    private inner class ViewHolder internal constructor(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.fragment_genderitem_list_dialog_item, parent, false)) {

        init {
            itemView.setOnClickListener {
                genderViewModel.selectedGender.value = when (adapterPosition) {
                    0 -> UserDto.SexEnum.MALE
                    1 -> UserDto.SexEnum.FEMALE
                    2 -> UserDto.SexEnum.OTHER
                    else -> UserDto.SexEnum.OTHER
                }
                dismiss()
            }
        }
    }

    private inner class GenderItemAdapter internal constructor(private val itemCount: Int, val selectedIndex : Int? = null) :
        RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            if(selectedIndex != null && selectedIndex == position){
                holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, color.gray600))
            } else{
                holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, color.gray700))
            }


            holder.itemView.tvGenderIcon.text = when (position) {
                0 -> getString(string.signup2_sheet_gender_male_icon)
                1 -> getString(string.signup2_sheet_gender_female_icon)
                2 -> getString(string.signup2_sheet_gender_other_icon)
                else -> ""
            }

            holder.itemView.tvGender.text = when (position) {
                0 -> getLocalizationText(getString(string.sign_up_3_gender_option_2), Language.getLanguageHashMap())
                1 -> getLocalizationText(getString(string.sign_up_3_gender_option_1), Language.getLanguageHashMap())
                2 -> getLocalizationText(getString(string.sign_up_3_gender_option_3), Language.getLanguageHashMap())
                else -> ""
            }

        }

        override fun getItemCount(): Int {
            return itemCount
        }
    }
}