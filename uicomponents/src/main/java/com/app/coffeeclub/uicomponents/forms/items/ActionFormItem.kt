package com.app.coffeeclub.uicomponents.forms.items

import androidx.annotation.ColorInt
import androidx.lifecycle.MutableLiveData
import com.app.coffeeclub.uicomponents.forms.FormatType

class ActionFormItem(valueLiveData : MutableLiveData<out Any>? = null,
                     label : String,
                     hint : String = "",
                     inputType: Int? = null,
                     maxLength : Int? = null,
                     type : FormatType = FormatType.Standard,
                     clickCallback : ((position : Int) -> Unit),
                     val isBtn : Boolean = false,
                     @ColorInt val btnTextColor: Int? = null)
    : FormItem(valueLiveData, label, hint, inputType, maxLength, type, clickCallback, true)  {

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}