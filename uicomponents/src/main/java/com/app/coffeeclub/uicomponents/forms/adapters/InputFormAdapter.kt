package com.app.coffeeclub.uicomponents.forms.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.app.coffeeclub.uicomponents.R
import com.app.coffeeclub.uicomponents.forms.items.InputFormItem
import com.app.coffeeclub.uicomponents.forms.viewholders.InputFormViewHolder

class InputFormAdapter :
    ListAdapter<InputFormItem, InputFormViewHolder>(MutableLiveDataDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InputFormViewHolder {
        val view =  LayoutInflater.from(parent.context)
                .inflate(R.layout.row_form, parent, false)
        return InputFormViewHolder(view)
    }

    override fun onBindViewHolder(holder: InputFormViewHolder, position: Int) = holder.bind(getItem(position), itemCount)

    private class MutableLiveDataDiff : DiffUtil.ItemCallback<InputFormItem>() {
        override fun areItemsTheSame(oldItem: InputFormItem, newItem: InputFormItem): Boolean {
            return oldItem.valueLiveData?.value == newItem.valueLiveData?.value
        }

        override fun areContentsTheSame(oldItem: InputFormItem, newItem: InputFormItem): Boolean {
            return oldItem == newItem
        }
    }
}
