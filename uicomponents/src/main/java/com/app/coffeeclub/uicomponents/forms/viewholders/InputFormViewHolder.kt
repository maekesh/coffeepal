package com.app.coffeeclub.uicomponents.forms.viewholders

import android.os.Build
import android.view.View
import androidx.annotation.RequiresApi
import com.app.coffeeclub.uicomponents.forms.items.FormItem
import com.app.coffeeclub.uicomponents.forms.items.InputFormItem
import com.app.coffeeclub.utilities.helpers.AutofillHelper

class InputFormViewHolder(itemView: View) : FormViewHolder(itemView) {

    override fun bind(item: FormItem, itemCount: Int) {
        super.bind(item, itemCount)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P && item is InputFormItem) {
            setAutoFill(item)
        }
    }

    override fun onFocusChange(item: FormItem, hasFocus: Boolean) {
        super.onFocusChange(item, hasFocus)
        if(item is InputFormItem) {
            item.validation(etValue, tvLabel, ivStatus, hasFocus, true)
        }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun setAutoFill(item: InputFormItem) {
        val hasAutofillId = item.autofillIdBundle?.containsKey(AutofillHelper.AUTOFILL_ID_ARG) ?: false
        if(hasAutofillId && !item.autoFillHint.isNullOrBlank()) {
            etValue.autofillId = item.autofillIdBundle?.getParcelable(AutofillHelper.AUTOFILL_ID_ARG)
            etValue.importantForAutofill = View.IMPORTANT_FOR_AUTOFILL_YES
            etValue.setAutofillHints(item.autoFillHint)
        }
    }

}