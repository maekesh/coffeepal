package com.app.coffeeclub.uicomponents.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.view.Gravity
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.core.view.isVisible
import com.app.coffeeclub.uicomponents.R
import com.app.coffeeclub.utilities.classes.Language
import com.app.coffeeclub.utilities.extensions.showText

class CoffeeClubDialog(val context: Context) {

    private val dialog: Dialog = Dialog(context)

    fun showDialog() {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.coffee_club_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window?.setGravity(Gravity.BOTTOM)
        dialog.show()
    }

    fun setTitle(resourceId: Int): CoffeeClubDialog {
        val tvAlertTitle = dialog.findViewById<TextView>(R.id.tvAlertTitle)
        tvAlertTitle.showText(context.getString(resourceId), Language.getLanguageHashMap())
        return this
    }

    fun setMessage(resourceId: Int): CoffeeClubDialog {
        val tvAlertMessage = dialog.findViewById<TextView>(R.id.tvAlertMessage)
        tvAlertMessage.isVisible = true
        tvAlertMessage.showText(context.getString(resourceId), Language.getLanguageHashMap())
        return this
    }

    fun setMessage(text: String): CoffeeClubDialog {
        val tvAlertMessage = dialog.findViewById<TextView>(R.id.tvAlertMessage)
        tvAlertMessage.isVisible = true
        tvAlertMessage.text = text
        return this
    }

    fun secondaryButton(resourceId: Int, handleClick:() -> Unit = {}) : CoffeeClubDialog {
        val tvBtnSecondary = dialog.findViewById<TextView>(R.id.tvBtnSecondary)
        tvBtnSecondary.isVisible = true
        tvBtnSecondary.showText(context.getString(resourceId), Language.getLanguageHashMap())
        tvBtnSecondary.setOnClickListener {
            handleClick()
        }
        return this
    }

    fun setCaption(resourceId: Int): CoffeeClubDialog {
        val tvCaption = dialog.findViewById<TextView>(R.id.tvCaption)
        tvCaption.isVisible = true
        tvCaption.text = context.getString(resourceId)
        return this
    }

    fun positiveButton(resourceId: Int, isDestructive: Boolean = false, handleClick:() -> Unit = {}): CoffeeClubDialog {
        return positiveButton(context.getString(resourceId), isDestructive, handleClick)
    }

    fun positiveButton(str: String, isDestructive: Boolean = false, handleClick: () -> Unit = {}): CoffeeClubDialog {
        val tvBtnAccept = dialog.findViewById<TextView>(if(isDestructive) {
            R.id.tvBtnAcceptDestructive
        } else {
            R.id.tvBtnAccept
        })

        tvBtnAccept.isVisible = true
        tvBtnAccept.showText(str, Language.getLanguageHashMap())
        tvBtnAccept.setOnClickListener {
            handleClick()
            dialog.dismiss()
        }
        return this
    }

    fun negativeButton(resourceId: Int, handleClick:() -> Unit = {}) : CoffeeClubDialog {
        val tvBtnDecline = dialog.findViewById<TextView>(R.id.tvBtnDecline)
        tvBtnDecline.isVisible = true
        tvBtnDecline.showText(context.getString(resourceId), Language.getLanguageHashMap())
        tvBtnDecline.setOnClickListener {
            handleClick()
            dialog.dismiss()
        }
        return this
    }
}