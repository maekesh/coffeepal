package com.app.coffeeclub.uicomponents.forms

import android.app.Activity
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.coffeeclub.uicomponents.R
import com.app.coffeeclub.uicomponents.forms.items.FormItem
import com.app.coffeeclub.uicomponents.forms.items.InputFormItem
import dk.idealdev.utilities.KeyboardUtil

object FormHelper {

    fun validateForm(activity: Activity, list: List<InputFormItem>, layoutManager: RecyclerView.LayoutManager) : Boolean {
        var areInputsValid = true

        for((index, formItem) in list.withIndex()) {
            val itemView = layoutManager.findViewByPosition(index) ?: return false
            val etValue = itemView.findViewById<EditText>(R.id.etValue)
            val tvValue = itemView.findViewById<TextView>(R.id.tvValue)
            val ivStatus = itemView.findViewById<ImageView>(R.id.ivStatus)
            val isItemValid = formItem.validation(etValue, tvValue, ivStatus,false, false)
            if(areInputsValid && !isItemValid) {
                etValue.requestFocus()
                formItem.validation
                etValue.selectAll()
                KeyboardUtil.showKeyboard(activity)
                areInputsValid = isItemValid
                break
            }
        }

        return areInputsValid
    }
}